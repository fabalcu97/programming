import datetime

from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible  # only if you need to support Python 2
class User(models.Model):
	name_text = models.CharField(max_length=200, 'Name')
	user_text = models.CharField(min_length = 4, max_length = 10, 'User')
	email_text = models.EmailField()
	password = models.

	def __str__(self):
		return self.name_text
