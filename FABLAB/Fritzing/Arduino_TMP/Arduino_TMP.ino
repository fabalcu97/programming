#include <SoftwareSerial.h>


#define ledPin 8 // pin 13 LED

#define txPin 1 // definimos txpin en que pin de arduino se encuentra
#define rxPin 0 // definimos rxpin en que pin de arduino se encuentra

bool state;
bool flag=0;

SoftwareSerial bluetooth(rxPin, txPin);

void setup() {
    // sets the pins as outputs:
    Serial.begin(9600); // velocidad de la comunicacion entre arduino y ordenador
    Serial.println("Estoy preparado"); //mensaje de comprobacion
    
    pinMode(ledPin, OUTPUT);
    
    pinMode(rxPin, INPUT);     // Configuramos los pines del bluetooth
    pinMode(txPin, OUTPUT);
    
    bluetooth.begin(9600);     // Iniciamos la comunicación Bluetooth
}

void loop() {

  if(bluetooth.available()){
    state = bluetooth.read();
  }
    
    Serial.print(state);
    Serial.write("-");
    
    if (state) {
        digitalWrite(ledPin, HIGH); // Enciende el LED
    }
    else{
        digitalWrite(ledPin, LOW); // Apaga el LED
    }
}