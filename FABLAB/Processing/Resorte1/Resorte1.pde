
float x_axis = 0;
float y_axis = 0;
float x_0 = 0;
float y_0 = 0;

float speed = 0;   // m/s
float rotacion = 0;
float ang = radians(rotacion);  // 85°
float G = 9.8;
float time = 0;
float speed_y = 0;
float speed_x = 0;

volatile boolean flag = true;
volatile boolean speedflag = true;


// Spring drawing constants for top bar
int springHeight = 32;  // Height
int left;               // Left position
int right;              // Right position
int max = 200;          // Maximum Y value
int min = 100;          // Minimum Y value
boolean over = false;   // If mouse over
boolean move = false;   // If mouse down and over

// Spring simulation constants
float M = 0.8;   // Mass
float K = 1;   // Spring constant
float D = 0.92;  // Damping
float R = 150;   // Rest position

// Spring simulation variables
float ps = R;    // Position
float vs = 0.0;  // Velocity
float as = 0;    // Acceleration
float f = 0;     // Force


void setup() {
  //size(640, 360);
  fullScreen();
  rectMode(CORNERS);
  noStroke();
  //left = width/2 - 100;
  //right = width/2 + 100;
  left = -100;
  right = 100;
}

void draw() {
  background(102);
  translate(width/2, height/2);
  rotate(radians(rotacion));
  ang = radians(90-rotacion);
  
  println("Angulo: "+ang+"°");
  updateSpring();
  drawSpring();
  drawBall();
}

void drawBall() {
  
  fill(255, 0, 0);

  //ellipse(width/2 + x_axis, height/2 -50- y_axis, springHeight+2, springHeight+2); //<>//
  ellipse(x_axis, 130-y_axis, springHeight+2, springHeight+2);
  if(speed != 0){
    lanzamiento();
    println("X axis: "+ x_axis);
    println("Y axis: "+ y_axis);
  }
}

void drawSpring() {
  
  // Draw base
  fill(0.2);
  float baseWidth = 0.5 * ps + -8;
  if (keyPressed) {
   if ( key == CODED ) {
     if ( keyCode == UP && rotacion > 0) {
       rotacion -= 1;
     }
     if ( keyCode == DOWN && rotacion < 90) {
       rotacion += 1;
     }
   }
  }
  //rect(width/2 - baseWidth, ps, width/2 + baseWidth, height/2);
  rect(-baseWidth, ps, baseWidth, 300);

  // Set color and draw top bar
  if(over || move) { 
   fill(255);
  }
  else { 
   fill(204);
  }
  rect(left, ps, right, ps + springHeight);
}


void updateSpring() {
  // Update the spring position
  if(!move) {
    f = -K * (ps - R);    // f=-ky
    as = f / M;           // Set the acceleration, f=ma == a=f/m
    vs = D * (vs + as);   // Set the velocity
    ps = ps + vs;         // Updated position
  }
  if(abs(vs) < 0.1) {
    vs = 0.0;
  }
  if(speed < vs){
    speed = vs;
  }
  println("velocidad: "+speed);
  // Test if mouse is over the top bar
  if((mouseX > left && mouseX < right && mouseY > ps && mouseY < ps + springHeight) || keyPressed && key == ' ') {
    over = true;
  }
  else{
    over = false;
  }
  
  // Set and constrain the position of top bar
  if(move) {
    ps = mouseY - springHeight/2;
    ps = constrain(ps, min, max);
  }
}

void mousePressed() {
  if(over) {
    move = true;
  }
}

void mouseReleased() {
  move = false;
}




void lanzamiento() {
  /*if(x_axis > width/2 || y_axis > height/2){
     return; 
  }*/
  
  x_axis = x((speed*(cos(ang))), time);
  y_axis = y(ang, x_axis, speed);

  time += 0.1;
  speed_y = y_speed(speed*sin(ang), time);
}

float x(float vel, float time) {
  return x_0 + vel * time;
}

float y(float ang, float pos_x, float speed) {
  return (tan(ang)) * pos_x - ( G * sq(pos_x) ) / ( 2 * sq(speed * (cos(ang)) ) );
}

float y_speed(float vel, float time) {
  return vel - G*time;
}