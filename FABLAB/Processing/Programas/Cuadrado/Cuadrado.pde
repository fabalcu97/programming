float y = 0;
float gravity = 1;
float x = 125;
float speed = 5;

void setup() {
  size(250, 250);
}

void draw() {
  background(25, 62, 130); //<>//
  speed += gravity;
  y += speed;
  ellipse(x, y, 50, 50);
  fill(255, 255, 255);
  if (y > height){
    y = height;
    speed *= -1;
  }
  if (y < 0){
    speed *= -1;
  }
}
