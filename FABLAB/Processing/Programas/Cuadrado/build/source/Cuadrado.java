import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Cuadrado extends PApplet {

float y = 0;
float gravity = 1;
float x = 125;
float speed = 5;

public void setup() {
  
}

public void draw() {
  background(25, 62, 130); //<>//
  speed += gravity;
  y += speed;
  ellipse(x, y, 50, 50);
  fill(255, 255, 255);
  if (y > height){
    y = height;
    speed *= -1;
  }
  if (y < 0){
    speed *= -1;
  }
}
  public void settings() {  size(250, 250); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Cuadrado" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
