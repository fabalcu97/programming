//<>// //<>//
void settings() {

  fullScreen();
}

void draw() {
  background(0, 0, 0);

  line(20, 300, displayWidth-50, 300);
  stroke(255);
  line(20, 20, 20, 300);
  stroke(255);
  fill(255, 0, 0);
  ellipse(x_axis + 20, 300 - y_axis, 10, 10);
  stroke(255);

  if ( key == 'r' && end_flag ) {
    end_flag = false;
    x_vector = new float[1];
    y_vector = new float[1];
    t_vector = new float[1];
    it = 0;
  }

  if ( keyPressed && !end_flag ) {

    if ( key == 't' ) {

      ang_flag = false;
      vel_flag = false;

      ang = radians(rotacion);
      speed_x = x_speed();
      Tv = t_vuelo();
      Tv_usuario = Tv;
      max_x = max_X();
      max_y = max_Y();
      time = 0;
      lanz_flag = true;
      lng = int(Tv/t_tasa);
      x_vector = new float[lng];
      y_vector = new float[lng];
      t_vector = new float[lng];

      t_flag = true;
    }
    if ( key == '+'  && vel_flag) {
      speed += 1;
      delay(100);
    }
    if ( key == '-'  && vel_flag) {
      speed -= 1;
      delay(100);
    }
    
    if ( key == CODED ) {
      if ( keyCode == UP && rotacion < 90  && ang_flag) {
        rotacion += 1;
      }
      if ( keyCode == DOWN /* && rotacion > 0*/  && ang_flag) {
        rotacion -= 1;
      }
      if ( keyCode == LEFT && Tv_usuario > 0 && t_flag) {
        Tv_usuario -= t_tasa;
      }
      if ( keyCode == RIGHT && Tv_usuario < Tv  && t_flag ) {
        Tv_usuario += t_tasa;
      }
      delay(100);
    }
    if ( key == ' ' && lanz_flag ) {
      t_flag = false;
      time += t_tasa;
      lanzamiento();
      /*println("T: "+time);
       println("Vx: "+speed_x);
       println("Rx: "+x_axis);
       println("Vy: "+speed_y);
       println("Ry: "+y_axis);
       println("------");*/
    }
  }  
  text("Modificar angulo con flecha arriba y flecha abajo. Modificar velocidad con '+' y '-'. ", 100, 15);
  text("Presionar 't' para modificar el tiempo de vuelo con flecha izquierda o flecha derecha.", 100, 30);  
  text("Presionar espacio para ejecutar el movimiento.", 100, 45);
  recorridoX();
  recorridoY();
  variables();
}

void recorridoX() {

  x = 20;

  translate(0, height/2);
  textSize(15);
  fill(255, 255, 255);  
  text("Tiempo: "+time+"s.", 20, -20);
  text("Recorrido en X respecto del tiempo: "+x_axis+"m.", 20, 0);
  text("X", 0, 150);
  text("Tiempo", 150, 320);
  line(20, 300, 300, 300);
  stroke(255, 255, 255);
  line(20, 20, 20, 300);
  stroke(255);
  fill(255, 255, 255);
  //point(t, 320-x);
  for (int i = 0; i < it; ++i) {
    t = map(t_vector[i], 0, Tv, 18, 300);
    if ( x_axis != 0 ) {
      x = map(x_vector[i], 0, max_x, 20, 300);
    }
    ellipse(t+3, 320-x, 1, 1);
  }
}

void recorridoY() {

  y = 20;

  translate(width/3, 0);
  textSize(15);
  fill(255, 255, 255);
  text("Y", 0, 150);
  text("Tiempo", 150, 320);
  text("Tiempo: "+time+"s.", 20, -20);
  text("Recorrido en Y respecto del tiempo: "+y_axis+"m.", 20, 0);
  line(20, 300, 300, 300);
  stroke(255, 255, 255);
  line(20, 20, 20, 300);
  stroke(255);
  fill(255, 255, 255);
  //point(t, 320-y);
  for (int i = 0; i < it; ++i) {
    t = map(t_vector[i], 0, Tv, 18, 300);
    y = map(y_vector[i], 0, max_y, 20, 300);
    ellipse(t+3, 320-y, 1, 1);
  }
}

void variables() {
  translate(width/3, 0);
  textSize(15);
  fill(255, 255, 255);
  text("Tiempo. : "+time+" s.", 0, 0);
  text("Tiempo V. : "+Tv+" s.", 0, 30);
  text("Ang. : "+rotacion+" °.", 0, 60);
  text("Rec. X: "+x_axis+" m.", 0, 90);
  text("Rec. Y: "+y_axis+" m.", 0, 120);
  text("Max. X: "+max_x+" m.", 0, 150);
  text("Max. Y: "+max_y+" m.", 0, 180);
  text("Vel. Total: "+speed+" m/s.", 0, 210);
  text("Vel. X: "+speed_x+" m/s.", 0, 240);
  text("Vel. Y: "+speed_y+" m/s.", 0, 270);
  text("Tiempo V. usuario: "+Tv_usuario+" s.", 0, 310);
  translate(0, 0);
}
void lanzamiento() {

  if ( time > Tv_usuario ) {
    if ( time > Tv ) {
      x_axis = max_x;
      y_axis = 0;
    }
    time -= t_tasa;
    end_flag = true;
    lanz_flag = false;
    return;
  }

  x_axis = x();
  y_axis = y();
  x_vector[it] = x_axis;
  y_vector[it] = y_axis;
  t_vector[it] = time;
  ++it;

  speed_y = y_speed(speed*sin(ang), time);
}