import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Fisica extends PApplet {

//<>//
public void settings() {

  fullScreen();
}

public void draw() {
  background(0, 0, 0);

  line(20, 300, displayWidth-50, 300);
  stroke(255);
  line(20, 20, 20, 300);
  stroke(255);
  fill(255, 0, 0);
  ellipse(x_axis + 20, 300 - y_axis, 10, 10);
  stroke(255);

  if ( key == 'r' && end_flag ) {
    end_flag = false;
    x_vector = new float[1];
    y_vector = new float[1];
    t_vector = new float[1];
    it = 0;
  }

  if ( keyPressed && !end_flag ) {

    if ( key == 't' ) {

      ang_flag = false;
      vel_flag = false;

      ang = radians(rotacion);
      speed_x = x_speed();
      Tv = t_vuelo();
      Tv_usuario = Tv;
      max_x = max_X();
      max_y = max_Y();
      time = 0;
      lanz_flag = true;
      lng = PApplet.parseInt(Tv/t_tasa);
      x_vector = new float[lng];
      y_vector = new float[lng];
      t_vector = new float[lng];

      t_flag = true;
    }
    if ( key == '+'  && vel_flag) {
      speed += 1;
      delay(100);
    }
    if ( key == '-'  && vel_flag) {
      speed -= 1;
      delay(100);
    }
    
    if ( key == CODED ) {
      if ( keyCode == UP && rotacion < 90  && ang_flag) {
        rotacion += 1;
      }
      if ( keyCode == DOWN /* && rotacion > 0*/  && ang_flag) {
        rotacion -= 1;
      }
      if ( keyCode == LEFT && Tv_usuario > 0 && t_flag) {
        Tv_usuario -= t_tasa;
      }
      if ( keyCode == RIGHT && Tv_usuario < Tv  && t_flag ) {
        Tv_usuario += t_tasa;
      }
      delay(100);
    }
    if ( key == ' ' && lanz_flag ) {
      t_flag = false;
      time += t_tasa;
      lanzamiento();
      /*println("T: "+time);
       println("Vx: "+speed_x);
       println("Rx: "+x_axis);
       println("Vy: "+speed_y);
       println("Ry: "+y_axis);
       println("------");*/
    }
  }  
  text("Modificar angulo con flecha arriba y flecha abajo. Modificar velocidad con '+' y '-'. ", 100, 15);
  text("Presionar 't' para modificar el tiempo de vuelo con flecha izquierda o flecha derecha.", 100, 30);  
  text("Presionar espacio para ejecutar el movimiento.", 100, 45);
  recorridoX();
  recorridoY();
  variables();
}

public void recorridoX() {

  x = 20;

  translate(0, height/2);
  textSize(15);
  fill(255, 255, 255);  
  text("Tiempo: "+time+"s.", 20, -20);
  text("Recorrido en X respecto del tiempo: "+x_axis+"m.", 20, 0);
  text("X", 0, 150);
  text("Tiempo", 150, 320);
  line(20, 300, 300, 300);
  stroke(255, 255, 255);
  line(20, 20, 20, 300);
  stroke(255);
  fill(255, 255, 255);
  //point(t, 320-x);
  for (int i = 0; i < it; ++i) {
    t = map(t_vector[i], 0, Tv, 18, 300);
    if ( x_axis != 0 ) {
      x = map(x_vector[i], 0, max_x, 20, 300);
    }
    ellipse(t+3, 320-x, 1, 1);
  }
}

public void recorridoY() {

  y = 20;

  translate(width/3, 0);
  textSize(15);
  fill(255, 255, 255);
  text("Y", 0, 150);
  text("Tiempo", 150, 320);
  text("Tiempo: "+time+"s.", 20, -20);
  text("Recorrido en Y respecto del tiempo: "+y_axis+"m.", 20, 0);
  line(20, 300, 300, 300);
  stroke(255, 255, 255);
  line(20, 20, 20, 300);
  stroke(255);
  fill(255, 255, 255);
  //point(t, 320-y);
  for (int i = 0; i < it; ++i) {
    t = map(t_vector[i], 0, Tv, 18, 300);
    y = map(y_vector[i], 0, max_y, 20, 300);
    ellipse(t+3, 320-y, 1, 1);
  }
}

public void variables() {
  translate(width/3, 0);
  textSize(15);
  fill(255, 255, 255);
  text("Tiempo. : "+time+" s.", 0, 0);
  text("Tiempo V. : "+Tv+" s.", 0, 30);
  text("Ang. : "+rotacion+" \u00b0.", 0, 60);
  text("Rec. X: "+x_axis+" m.", 0, 90);
  text("Rec. Y: "+y_axis+" m.", 0, 120);
  text("Max. X: "+max_x+" m.", 0, 150);
  text("Max. Y: "+max_y+" m.", 0, 180);
  text("Vel. Total: "+speed+" m/s.", 0, 210);
  text("Vel. X: "+speed_x+" m/s.", 0, 240);
  text("Vel. Y: "+speed_y+" m/s.", 0, 270);
  text("Tiempo V. usuario: "+Tv_usuario+" s.", 0, 310);
  translate(0, 0);
}
public void lanzamiento() {

  if ( time > Tv_usuario ) {
    if ( time > Tv ) {
      x_axis = max_x;
      y_axis = 0;
    }
    time -= t_tasa;
    end_flag = true;
    lanz_flag = false;
    return;
  }

  x_axis = x();
  y_axis = y();
  x_vector[it] = x_axis;
  y_vector[it] = y_axis;
  t_vector[it] = time;
  ++it;

  speed_y = y_speed(speed*sin(ang), time);
}

float x_axis = 0;
float y_axis = 0;
float x_0 = 0;
float y_0 = 0;

float speed = 50;   // m/s
float rotacion = 45;
float ang = 0;  // 85\u00b0
float G = 9.8f;
float time = 0;
float t_tasa = 0.1f;
float speed_y = 0;

volatile boolean flag = true;
volatile boolean end_flag = false;
volatile boolean ang_flag = true;
volatile boolean t_flag = false;
volatile boolean vel_flag = true;
volatile boolean lanz_flag = false;
float t = 20;
float x = 0;
float y = 0;

float speed_x = 0;
float Tv = 0;
float Tv_usuario = 0;
float max_x = 0;
float max_y = 0;

float x_vector[];
float y_vector[];
float t_vector[];
int lng = 0;
int it = 0;

public float x() {
  if (cos(ang) < 0) {
    return 0;
  }
  return speed * cos(ang) * time;
}

public float x_speed() {
  if (cos(ang) < 0) {
    return 0;
  }
  return speed * cos(ang);
}

public float max_X(){
  return Tv * speed_x;
}

public float y() {
  return y_0 + speed * sin(ang) * time - ( G * sq(time) / 2);
}

public float y1(float pos_x) {
  return (tan(ang)) * pos_x - ( G * sq(pos_x) / ( 2 * sq((speed* (cos(ang)) ))));
}

public float y_speed(float vel, float time) {
  return vel - G*time;
}

public float t_vuelo() {
  return (2 * speed * sin(ang))/G;
}

public float max_Y() {
  return (sq( speed * sin(ang) ))/(G);
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "Fisica" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
