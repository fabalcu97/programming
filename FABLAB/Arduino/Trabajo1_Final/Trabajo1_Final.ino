#include <Arduino_FreeRTOS.h>
#include <semphr.h>

#define SD_CS_PIN SS
#include <SPI.h>
#include <SdFat.h>


// Configurar el orden de los pines dependiendo a como los hayas conectado.
#define Matriz_0_0 22
#define Matriz_0_1 23
#define Matriz_0_2 24            //                        Malla de LEDS
#define Matriz_0_3 25             //  Matriz_0_0 | Matriz_0_1 | Matriz_0_2 | Matriz_0_3 | Matriz_0_4
#define Matriz_0_4 26             //  Matriz_1_0 | Matriz_1_1 | Matriz_1_2 | Matriz_1_3 | Matriz_1_4
#define Matriz_1_0 28             //  Matriz_2_0 | Matriz_2_1 | Matriz_2_2 | Matriz_2_3 | Matriz_2_4
#define Matriz_1_1 29             //  Matriz_3_0 | Matriz_3_1 | Matriz_3_2 | Matriz_3_3 | Matriz_3_4
#define Matriz_1_2 30             //  Matriz_4_0 | Matriz_4_1 | Matriz_4_2 | Matriz_4_3 | Matriz_4_4
#define Matriz_1_3 31
#define Matriz_1_4 32
#define Matriz_2_0 34
#define Matriz_2_1 35
#define Matriz_2_2 36
#define Matriz_2_3 37
#define Matriz_2_4 38
#define Matriz_3_0 40
#define Matriz_3_1 41
#define Matriz_3_2 42
#define Matriz_3_3 43
#define Matriz_3_4 44
#define Matriz_4_0 7
#define Matriz_4_1 8
#define Matriz_4_2 9
#define Matriz_4_3 10
#define Matriz_4_4 11
//Los numero de LEDS
#define NumeroDeLEDS 25

//La variable Matriz matriz servir� para inicializar los pines
uint8_t Matriz[NumeroDeLEDS] = {Matriz_0_0, Matriz_0_1, Matriz_0_2, Matriz_0_3, Matriz_0_4, Matriz_1_0, Matriz_1_1, Matriz_1_2, Matriz_1_3, Matriz_1_4, Matriz_2_0, Matriz_2_1, Matriz_2_2, Matriz_2_3, Matriz_2_4, Matriz_3_0, Matriz_3_1, Matriz_3_2, Matriz_3_3, Matriz_3_4, Matriz_4_0, Matriz_4_1, Matriz_4_2, Matriz_4_3, Matriz_4_4};

#define BTSerial Serial1
#define BT Serial2
SdFile myFile;
SdFat SD;
/*
  * * * * *
  *       *
  * * * * *
  *       *
  *       *
*/
void MostrarA() {
  //Serial.println("A");
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_4, HIGH);



}
/*
  * * * *
  *       *
  * * * *
  *       *
  * * * *
*/
void MostrarB() {
  // Serial.println("B");
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);


}
/*
  * * * * *



  * * * * *
*/
void MostrarC() {
  // Serial.println("C");
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
/*
  * * * *
  *       *
  *       *
  *       *
  * * * *
*/
void MostrarD() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
}
/*
  * * * * *

  * * * * *

  * * * * *
*/
void MostrarE() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
/*
  * * * * *

  * * * * *


*/
void MostrarF() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_4_0, HIGH);

}
/*
  * * * * *

  *   * * *
  *       *
  * * * * *
*/
void MostrarG() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
/*
  *       *
  *       *
  * * * * *
  *       *
  *       *
*/
void MostrarH() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
/*
  * * * * *



  * * * * *
*/
void MostrarI() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_2, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_3_2, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}

/*
  * * * * *


  *   *
  * * *
*/
void MostrarJ() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_2, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_2, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);

}
/*
  *     *
  *   *
  * *
  *   *
  *     *
*/
void MostrarK() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_2, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_2, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
}
/*




  * * * * *
*/
void MostrarL() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
/*
  *       *
  * *   * *
  *   *   *
  *       *
  *       *
*/

void MostrarM() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_1, HIGH);
  digitalWrite(Matriz_1_3, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_4, HIGH);

}
/*
  *       *
  * *     *
  *   *   *
  *     * *
  *       *
*/

void MostrarN() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_1, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_3, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_4, HIGH);

}


/*
  * * * * *
  *       *
  *       *
  *       *
  * * * * *
*/

void MostrarO() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);

}
/*
  * * * *
  *     *
  * * * *


*/

void MostrarP() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_3, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_4_0, HIGH);

}

/*
  * * * *
  *     *
  *   * *
  * * * *


*/

void MostrarQ() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_3, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_1, HIGH);
  digitalWrite(Matriz_3_2, HIGH);
  digitalWrite(Matriz_3_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);

}

/*
  * * *
  *     *
  * * *
  *   *
  *     *

*/

void MostrarR() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_3, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_2, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_3, HIGH);

}


/*
  * * * * *

  * * * * *

  * * * * *

*/
void MostrarS() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_1, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_3, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}

/*
  * * * * *





*/
void MostrarT() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_2, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_3_2, HIGH);
  digitalWrite(Matriz_4_2, HIGH);

}
/*
  *       *
  *       *
  *       *
  *       *
  * * * * *

*/
void MostrarU() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
/*
   *       *
   *       *
   *       *
     *   *


*/
void MostrarV() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_1, HIGH);
  digitalWrite(Matriz_3_3, HIGH);
  digitalWrite(Matriz_4_2, HIGH);

}
/*
   *       *
   *       *
   *   *   *
   * *   * *
   *       *
*/
void MostrarW() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_0, HIGH);
  digitalWrite(Matriz_1_4, HIGH);
  digitalWrite(Matriz_2_0, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_2_4, HIGH);
  digitalWrite(Matriz_3_0, HIGH);
  digitalWrite(Matriz_3_1, HIGH);
  digitalWrite(Matriz_3_3, HIGH);
  digitalWrite(Matriz_3_4, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_4, HIGH);

}
/*
   *       *
     *   *

     *   *
   *       *

*/
void MostrarX() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_1, HIGH);
  digitalWrite(Matriz_1_3, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_3_1, HIGH);
  digitalWrite(Matriz_3_3, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
/*
   *       *
     *   *




*/
void MostrarY() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_1, HIGH);
  digitalWrite(Matriz_1_3, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_3_2, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
}
/*
   * * * * *



   * * * * *

*/
void MostrarZ() {
  digitalWrite(Matriz_0_0, HIGH);
  digitalWrite(Matriz_0_1, HIGH);
  digitalWrite(Matriz_0_2, HIGH);
  digitalWrite(Matriz_0_3, HIGH);
  digitalWrite(Matriz_0_4, HIGH);
  digitalWrite(Matriz_1_3, HIGH);
  digitalWrite(Matriz_2_2, HIGH);
  digitalWrite(Matriz_3_1, HIGH);
  digitalWrite(Matriz_4_0, HIGH);
  digitalWrite(Matriz_4_1, HIGH);
  digitalWrite(Matriz_4_2, HIGH);
  digitalWrite(Matriz_4_3, HIGH);
  digitalWrite(Matriz_4_4, HIGH);
}
void MostrarEspacio() {
  digitalWrite(Matriz_0_0, LOW);
  digitalWrite(Matriz_0_1, LOW);
  digitalWrite(Matriz_0_2, LOW);
  digitalWrite(Matriz_0_3, LOW);
  digitalWrite(Matriz_0_4, LOW);
  digitalWrite(Matriz_1_0, LOW);
  digitalWrite(Matriz_1_1, LOW);
  digitalWrite(Matriz_1_2, LOW);
  digitalWrite(Matriz_1_3, LOW);
  digitalWrite(Matriz_1_4, LOW);
  digitalWrite(Matriz_2_0, LOW);
  digitalWrite(Matriz_2_1, LOW);
  digitalWrite(Matriz_2_2, LOW);
  digitalWrite(Matriz_2_3, LOW);
  digitalWrite(Matriz_2_4, LOW);
  digitalWrite(Matriz_3_0, LOW);
  digitalWrite(Matriz_3_1, LOW);
  digitalWrite(Matriz_3_2, LOW);
  digitalWrite(Matriz_3_3, LOW);
  digitalWrite(Matriz_3_4, LOW);
  digitalWrite(Matriz_4_0, LOW);
  digitalWrite(Matriz_4_1, LOW);
  digitalWrite(Matriz_4_2, LOW);
  digitalWrite(Matriz_4_3, LOW);
  digitalWrite(Matriz_4_4, LOW);
}
void Mostrar(char letra) {
  switch (letra) {
    case 'A': MostrarA();
      break;
    case 'B': MostrarB();
      break;
    case 'C': MostrarC();
      break;
    case 'D': MostrarD();
      break;
    case 'E': MostrarE();
      break;
    case 'F': MostrarF();
      break;
    case 'G': MostrarG();
      break;
    case 'H': MostrarH();
      break;
    case 'I': MostrarI();
      break;
    case 'J': MostrarJ();
      break;
    case 'K': MostrarK();
      break;
    case 'L': MostrarL();
      break;
    case 'M': MostrarM();
      break;
    case 'N': MostrarN();
      break;
    case 'O': MostrarO();
      break;
    case 'P': MostrarP();
      break;
    case 'Q': MostrarQ();
      break;
    case 'R': MostrarR();
      break;
    case 'S': MostrarS();
      break;
    case 'T': MostrarT();
      break;
    case 'U': MostrarU();
      break;
    case 'V': MostrarV();
      break;
    case 'W': MostrarW();
      break;
    case 'X': MostrarX();
      break;
    case 'Y': MostrarY();
      break;
    case 'Z': MostrarZ();
      break;
    default:
      MostrarEspacio();

      //   printf("Esa letra no vale XD\n");
      break;
  }

}





#define mainDELAY_LOOP_COUNT  400000 // ( 0xffffff )


#define MAX_COLA 200
/* The task functions. */

char letras[] = "abcdefghijklmnoprstu#vwxyz";
struct queue
{
  char arr[MAX_COLA];
  int front; // delete from this position
  int rear;  // insert from this position:
  int tam;
  queue()
  {
    front = -1;
    rear = -1;
    tam = 0;
    for (int i = 0; i < MAX_COLA; i++)
      arr[i] = '\0';
  }
  char getfront()
  {
    return arr[front];
  }
  void insert(char x)
  {
    arr[rear + 1] = x;
    if (front == -1)
    {
      front = 0;
    }
    rear = rear + 1;
    tam = tam + 1;
  }
  void delete_element()
  {
    arr[front] = 0;
    front = front + 1;
    if (front > rear)
    {
      front = -1;
      rear = -1;
      tam = 0;
    }
    else
      tam--;
  }
  void delete_cola()
  {
    if (front != -1 && rear != -1)
    {
      for (int i = front; i <= rear + 2; i++)
        delete_element();
      //  tam=0;
    }

  }
  void display()
  {
    if (front != -1 && rear != -1)
    {
      for (int i = front; i <= rear; i++)
      {
        Serial.print("Queue[");
        Serial.print(i);
        Serial.print("] :");
        Serial.print(arr[i]);
        Serial.println("\t");
      }
    }
    else
    {
      Serial.println("Queue is empty !!!");
    }
  }
  void to_String(/*char  aux[MAX_COLA]*/)//convierto la cola en string
  {
    if (front != -1 && rear != -1)
    {
      char aux[MAX_COLA];// = (char*)malloc(len + 1 + 1 );

      for (int i = front; i <= rear; i++)
      {
        Serial.print("Queue[");
        Serial.print(i);
        Serial.print("] :");
        Serial.print(arr[i]);
        Serial.println("\t");
        aux[i] = arr[i];

      }
      Serial.println("Tamanho de la cola:");
      //            vPrintString("TAMA�O DE LA COLA\n");
      Serial.println(tam);
      aux[tam] = '\0';
      Serial.println("Cola concatenada en un string que se almacenara en el archivo txt:");
      Serial.println(aux);
      if (!myFile.open("arduino.txt", O_RDWR | O_CREAT | O_AT_END)) {
        SD.errorHalt("opening test.txt for write failed");
      }
      Serial.print("Writing to test.txt...");
      myFile.println(aux);

      // close the file:
      myFile.close();
      Serial.println("done.");
      //            vPrintString("Cola concatenada en un string que se almacenara en el archivo txt\n");
      //            vPrintString(aux);//aca debe mandarse ese aux a un archivo cola para ser atendido
      //            vPrintString("\n");
    }
    else
    {
      Serial.println("Queue is empty !!!");
    }


  }
};






SemaphoreHandle_t sem = 0;
SemaphoreHandle_t sem2 = 0;
SemaphoreHandle_t sem3 = 0;
SemaphoreHandle_t sem4 = 0;
SemaphoreHandle_t xCountingSemaphore = 0;

queue q[5];//ARRAY DE ARDUINOS ARDUINO 0 1 2 3 4
int arduino;
/*queue q1;//ARDUINO 1
  queue q2;//ARDUINO 2
  queue q3;//ARDUINO 3
  queue q4;//ARDUINO 4
  queue q5;//ARDUINO 5*/
int count = 0;
int count2 = 0;
char lista[50];
char mensaje[50];
int estado = 50;
int posicion;
bool disponible = 1;
char id;
void setup() {
  for (int i = 0; i < 25; i++) {
    pinMode(Matriz[i], OUTPUT);
  }
  //    Mostrar('A');

  Serial.begin(9600);
  BT.begin(9600);
  BTSerial.begin(9600);

  digitalWrite(53, HIGH);
  if (!SD.begin(53, SPI_HALF_SPEED)) {
    SD.initErrorHalt();
  }
  if (!myFile.open("arduino.txt", O_RDWR | O_CREAT | O_AT_END)) {
    SD.errorHalt("opening test.txt for write failed");
  }
  
  myFile.remove();
  myFile.close();

  vSemaphoreCreateBinary(sem);
  vSemaphoreCreateBinary(sem2);
  vSemaphoreCreateBinary(sem3);
  vSemaphoreCreateBinary(sem4);
  xCountingSemaphore = xSemaphoreCreateCounting( 40, 0 );
  xSemaphoreTake(sem, 0);
  xSemaphoreTake(sem2, 0);
  xSemaphoreTake(sem3, 0);
  xSemaphoreTake(sem4, 0);
  xTaskCreate(vProtocolo, (const portCHAR*)"Protocolo", 500, NULL, 3, NULL);
  xTaskCreate(vBluetooth, (const portCHAR*)"Bluetooth", 1500, NULL, 2, NULL);
  xTaskCreate(vMostrarMensaje,  (const portCHAR*)"Mostrar", 500, NULL, 4, NULL);
  xTaskCreate(vOpciones, (const portCHAR*)"Opciones", 500, NULL, 2, NULL);
  //xTaskCreate(vEnviar,(const portCHAR*)"Enviar",500,NULL,2,NULL);
  vTaskStartScheduler();
  for ( ;; );
  //  }
}
void vBluetooth(void *pvParameters) {
  for (;;) {
    if (BT.available()) {
      while (BT.available()) {
        char leer;
        //Serial.write(BT.read());
        leer = BT.read();
        lista[count] = leer;
        count++;
        delay(20);
        if (leer == 35){
          break;
        }
      }
      xSemaphoreGive(sem);
    }
  }

}
void vProtocolo(void *pvParameters) {
  for (;;) {
    if (xSemaphoreTake(sem, portMAX_DELAY) == pdTRUE) {
      int j = 3;
      id = lista[0];
      estado = lista[1];
      char t = lista[2];
      //char pos=lista[2];
      //posicion = pos - '0';

      int arduino = id - '0';
      Serial.println("ID:");
      Serial.println(arduino);
      Serial.println("Semaforo:");
      Serial.println(estado);
      Serial.println("letra:");
      Serial.println(t);
      Serial.println("Enviando del celular:");
      /*while(j<count){
          Serial.write(lista[j]);
          mensaje[j-3]=lista[j];
        //  BTSerial.write(lista[j]);
         //count++;
         j++;
        }*/

      //mensaje[j-4]='\0';
      count = 0;

      q[arduino].insert(t);

      if (t == '#')
      {
        Serial.println("Ahora mando a imprimir lo que haya en la cola nmro:");
        //        vPrintString("AHORA MANDO A IMPRIMIR LO QUE HAYA EN LA COLA nmro ");
        Serial.println(arduino);
        q[arduino].to_String();
        Serial.println("Dejo limpia la cola:");
        //        vPrintString("DEJO LIMPIA LA COLA\n");
        q[arduino].delete_cola();
        xSemaphoreGive(xCountingSemaphore);

      }

      xSemaphoreGive(sem4);
    }
    //    BTSerial.end();
  }

}
char enviar_a[4];

void vMostrarMensaje(void *pvParameters) {
  int posicion_actual = 0;
  for (;;) {
    if (xSemaphoreTake(xCountingSemaphore, portMAX_DELAY) == pdTRUE && xSemaphoreTake(sem3, portMAX_DELAY) == pdTRUE) {
      Serial.println("El mensaje es:");
      if (!myFile.open("arduino.txt", O_READ)) {
        SD.errorHalt("opening test.txt for read failed");
      }
      Serial.println("arduino.txt:");
      myFile.seekCur(posicion_actual);
      int data;   enviar_a[0] = '1';
      enviar_a[1] = '1';
      data = myFile.read();
      Mostrar(data);
      while ((data = myFile.read()) >= 0) {
        posicion_actual++;
        delay(2000);
        BT.write(enviar_a);
        delay(500);
        MostrarEspacio();
        Mostrar(data);
        enviar_a[2] = data;
        enviar_a[3] = '\0';

        if (data == 35)
          break;
        else
          Serial.write(data);
        Serial.print('%');
      }
      // close the file:
      myFile.close();
      posicion_actual += 2;
      // Serial.write(mensaje[posicion]);
      // MostrarEspacio();
      // Mostrar(data);
    }
  }

}
void vOpciones(void *pvParameters ) {
  for (;;) {
    if (estado == 48) {
      Serial.println("Estoy apagado");
      MostrarEspacio();
    }

    if (estado == 49) {
      xSemaphoreGive(sem3);
      // Serial.println("Estoy imprimiendo");

    }

    if (estado == 50) {
      //Serial.println("Despauseame primero");
    }

    if (estado == 51) {
      Serial.println("Manos a la obra");
      estado = 49;
    }
  }

}
void vEnviar(void *pvParameters) {
  for (;;) {
    if (xSemaphoreTake(sem4, portMAX_DELAY) == pdTRUE) {
      char enviarmensaje[10];
      enviarmensaje[0] = id;
      enviarmensaje[1] = estado;
      enviarmensaje[2] = 48;
      for (int i = 3; lista[i] != '\0'; i++) {
        enviarmensaje[3] = lista[i];


        BTSerial.write(enviarmensaje);
        Serial.println("Acutalizacion:");
        delay(3000);
        MostrarEspacio();
        Mostrar(mensaje[i - 2]);
        Serial.println(mensaje[i - 3]);
        Serial.println("Envio a Lucho:");
        Serial.write(enviarmensaje);
        delay(2000);
        Serial.println();

      }
    }


  }
}

void loop() {}

