#include <TinyGPS++.h>

#include <SoftwareSerial.h>

long lat, lon;  // Variables auxiliares para lectura de longitud y latitud
double LAT, LON; // Variables auxiliares para latitud y longitud con punto decimal

SoftwareSerial ss(2, 3); // Pines usados para comunicacion con el GPS (RXD = 2, TXD = 3)
TinyGPSPlus GPS; // Declaracion de cabecera para la funcion GPS



void lectura_GPS();
bool feedgps();
void informacion_gps(TinyGPSPlus&);
void lectura_serial();

void setup() {
  ss.begin(9600);     // Velocidad de comunicacion del GPS (Segun hoja de datos)
  Serial.begin(9600); // Velocidad de comunicacion con la computadora
}

void loop() {
  lectura_serial(); // Lectura de la informacion del GPS
  //  delay(3000);
}

void lectura_GPS() {
  bool newdata = false;               // Variable auxiliar para ingreso de datos
  unsigned long start = millis();     // Variable auxiliar para refresh y update datos

  while (millis() - start < 3000) {   // Update de lectura cada segundo
    if (feedgps()) {
      newdata = true;  // Validacion de datos en la pila
    }
  }
  if (newdata) {
    informacion_gps(GPS);   // Ingreso de datos
  }
}

bool feedgps() {
  while (ss.available()) {     // Mientras exista informacion en la pila de datos
    if (GPS.encode(ss.read())) // Validacion de existencia de datos en la pila de datos
      return true;
  }
  return false;
}

void informacion_gps(TinyGPSPlus &GPS)
{
  //GPS.get_position(&lat, &lon); // Byte de datos de latitud y longitud
  lat = GPS.location.lat();
  lon = GPS.location.lng();
  LAT = lat; // Adquicion de datos en decimales
  LON = lon; // Adquicion de datos en decimales
  {
    feedgps(); // Sin esta funcion, puede causarse un error de suma
  }
}

void lectura_serial() {
  
  lat = GPS.location.lat();
  lon = GPS.location.lng();
  lectura_GPS();                   // Funcion de rutina de lectura del GPS
  Serial.print("P");
  
  Serial.print(LAT, 4);    // Latitud en grados con diez decimales
  Serial.print(":");
  
  Serial.print(LON, 4);  // Longitud en grados con diez decimales
  Serial.println();
  delay(500);
}



