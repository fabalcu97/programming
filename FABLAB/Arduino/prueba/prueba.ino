#include <RRobin.h>

RRobin RR;

void loop2(void* args)
{
    digitalWrite(12, HIGH);
    delay(500);
    digitalWrite(12, LOW);
    delay(500);
}

void loop3(void* args)
{
    digitalWrite(2, HIGH);
    delay(150);
    digitalWrite(2, LOW);
    delay(150);
}


void setup() {
  pinMode(12, OUTPUT);
  pinMode(2, OUTPUT);

  Serial.begin(9600);
  RR.TaskCreate(&loop3, 0, "1");
  RR.TaskCreate(&loop2, 1, "2");
  RR.Execute();
  
}

void loop() {}
