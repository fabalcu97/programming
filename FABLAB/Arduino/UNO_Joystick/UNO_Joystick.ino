#include <Arduino.h>

#define up 12
#define down 13
#define right 11
#define left 10

//  Variables


const int X = 0;
const int Y =  1;
const int Z = 7;
int axis_x = 0;
int axis_y = 0;
bool axis_z = 0;

void setup()
{
  pinMode(Z, INPUT);
  Serial.begin(9600);
}

void loop()
{
  axis_x = analogRead(X);
  axis_y = analogRead(Y);
  axis_z = digitalRead(Z);

  int movements[3] = {  axis_x,
                        axis_y,
                        axis_z};

  Serial.print("x");
  Serial.print(movements[0]);
  Serial.print("y");
  Serial.print(movements[1]);
  Serial.print("z");
  Serial.print(movements[2]);

  /*Serial.write("Eje X: ");*/
  /*Serial.println(axis_x);*/
  /*Serial.write("Eje Y: ");*/
  /*Serial.println(axis_y);*/
  /*Serial.write("Eje Z: ");*/
  /*Serial.println(axis_z);*/
  /*delay(1000);*/
}
