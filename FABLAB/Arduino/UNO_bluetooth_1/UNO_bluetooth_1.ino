int led = 13;

void setup() {
    pinMode(led, OUTPUT);
    Serial.begin(9600);
}
void loop() {
    digitalWrite(led, HIGH);
    command("AT",2);
    command("AT+VERSION",12);
    command("AT+NAMEArduino",9);
    command("AT+BAUD4",8);
    while(1);
}
void command(const char* cmd, int num_bytes_response) {
    delay(1000);
    Serial.print(cmd);
    delay(1500);
    for (int i=0;i<num_bytes_response;i++)
        Serial.write(Serial.read());
}
