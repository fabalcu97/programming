/*
   PROGRAMA DE PRUEBA: SkyNav SKM53 GPS
   CONEXION:
             RXD: Arduino Pin 3
             TXD: Arduino Pin 2
             GND: Arduino GND
             VCC: Arduino 5V
             NC y RST: Dejar sin conexion
             
   Para correr el programa se necesitan las librerias TinyGPS++ y New Software Serial Library.
   
   Autor: Renato H.
   http://beetlecraft.blogspot.pe/
   
   El siguiente programa es de uso publico, cualquier modificacion o mal uso del mismo que pudiera 
   ocasionar el mal funcionamiento de la plataforma de uso de la misma no es responsabilidad del autor
*/

#include <TinyGPS++.h>      // Libreria de manejo del GPS
#include <SoftwareSerial.h> // Libreria de manejo de comunicacion serial alterna

float LAT, LON; // Variables auxiliares para latitud y longitud con punto decimal 

SoftwareSerial ss(2,3); // Pines usados para comunicacion con el GPS (RXD = 2, TXD = 3)
TinyGPSPlus GPS;        // Declaracion de cabecera para la funcion GPS

void setup(){
  ss.begin(9600);     // Velocidad de comunicacion del GPS (Segun hoja de datos)
  Serial.begin(9600); // Velocidad de comunicacion con la computadora
}

void loop(){
  main_gps(); // Lectura de la informacion del GPS
}

void main_gps(){
  while (ss.available() > 0) {
    if (GPS.encode(ss.read())) {
      lectura();
    }
  }
  
  if (millis() > 5000 && GPS.charsProcessed() < 10){
    Serial.println("No hay GPS...");
    //uwhile(true);
  }
}

void lectura(){
  //if (GPS.location.isValid()){
    LAT = GPS.location.lat()*1000000;
    LON = GPS.location.lng()*1000000;
    //Serial.print("Latitud : ");
    Serial.print("P");
    Serial.print(":");
    Serial.print(LAT);    // Latitud en grados con diez decimales
    //Serial.print(" :: Longitud : "); 
    Serial.print(":");
    Serial.print(LON);  // Longitud en grados con diez decimales
    Serial.print(":");
    Serial.println();
  /*}
  else{
    Serial.println(F("Dato invalido"));
  }*/
}
