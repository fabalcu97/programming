#include <Arduino_FreeRTOS.h>
#include <SoftwareSerial.h>

#define SD_CS_PIN SS
#include <SPI.h>
#include <SdFat.h>
SdFat SD;

File myFile;

#define ledPin 10
volatile bool state = false;
volatile bool BT_flag = false;
int it = 0;

SoftwareSerial bluetooth(8, 9); //Rx - Tx

// define two tasks for Blink & AnalogRead
void TaskBlink( void *pvParameters );
void TaskSDCard( void *pvParameters );

// the setup function runs once when you press reset or power the board
void setup() {

  // Now set up two tasks to run independently.
  xTaskCreate( TaskBlink, (const portCHAR *)"Blink", 128, NULL, 2, NULL );
  xTaskCreate( TaskSDCard,  (const portCHAR *) "SDCard", 128, NULL, 2, NULL );

  ////////////////////////////////////// SETUP Bluetooth

  pinMode(ledPin, OUTPUT);
  //pinMode(8, INPUT);
  //pinMode(9, OUTPUT);

  //bluetooth.begin(38400);
  Serial1.begin(9600);

  ////////////////////////////////////// SETUP SD-Card

  Serial.begin(9600);

  Serial.print("Initializing SD card...");

  pinMode(53, OUTPUT);

  if (!SD.begin(SD_CS_PIN)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  //////////////////////////////////////

  // Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
  // Empty. Things are done in Tasks.
}

void TaskSDCard(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  for (;;) // A Task shall never return or exit.
  {
    if (BT_flag) {
      myFile = SD.open("TEST.txt", FILE_WRITE); //(name, tipe)

      if (myFile) {
        Serial.print("Writing to test.txt...");
        myFile.println("holi - " + it);
        Serial.println("holi - " + it);
        myFile.close();
        Serial.println("done.");
        BT_flag = false;
        delay(500);
        /*if(Serial.available()){
          char c = Serial.read();
          if(c == 'q'){
            return;
          }
        }*/
      }
      else{
        Serial.println("error opening test.txt");
      }
    }
  }
}

void TaskBlink(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  for (;;)
  {
    if ( Serial1.available() && !BT_flag) {
      state = Serial1.read();
      it++;
      BT_flag = true;
    }

    if (state) {
      digitalWrite(ledPin, HIGH);
    }
    else {
      digitalWrite(ledPin, LOW);
    }
  }
}
