/*
   PROGRAMA DE PRUEBA DE SERVOS: SERVOS ANALOGICOS
 
   CONEXION:
             TRIGGER: Pin 12 del Arduino UNO
             ECHO: Pin 11 del Arduino UNO
             GND: Conectar los dos GND
             VCC: Conectar los dos 5V
    
   En el siguiente programa probaremos el sensor HC - SR04 
    
   NOTA: Para el siguiente programa necesitamos la libreria "NewPing" se puede encontrar en el
   siguiente link https://www.dropbox.com/sh/2k5m76rxm6ew4sz/AAC4-ui2LQL9LZC30_bARyXja?dl=0
    
   Autor: Renato H.
   http://beetlecraft.blogspot.pe/
 
   El siguiente programa es de uso publico, cualquier modificacion o mal uso del mismo que pudiera 
   ocasionar el mal funcionamiento de la plataforma de uso de la misma no es responsabilidad del 
   autor
*/
 
#include <NewPing.h>
 
#define TRIGGER_PIN  12  // Pin designado para el pin Trigger en el Arduino UNO
#define ECHO_PIN     11  // Pin designado para el pin Echo en el Arduino UNO
#define MAX_DISTANCE 200 // Distancia máxima en cm
 
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // Configuracion de la libreria NewPing
 
const int ajuste = 0; // Medida de ajuste a la medicion que depende de cada sensor
int cm; // Dato de almacenamiento de la medicion 
 
void setup() {
  Serial.begin(115200); // Configuracion del puerto serial
}
 
void loop() {
  delay(50);            // Tiempo de espera para la medicion
  cm = sonar.ping_cm(); // Medicion del sensor
  cm = cm + ajuste;     // Ajuste a la medicion hecha 
  Serial.print("Distancia: ");
  Serial.print(cm); 
  Serial.println("cm");
}
