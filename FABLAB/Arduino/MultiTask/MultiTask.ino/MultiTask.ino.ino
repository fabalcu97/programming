#include <Arduino_FreeRTOS.h>
#include <SoftwareSerial.h>

// define two tasks for Blink & AnalogRead
void TaskBlink( void *pvParameters );
void TaskBluetooth( void *pvParameters );

SoftwareSerial bluetooth(0, 1);

// the setup function runs once when you press reset or power the board
void setup() {

  // Now set up two tasks to run independently.
  xTaskCreate( TaskBlink, (const portCHAR *)"Malla", 128, NULL, 2, NULL );
  xTaskCreate( TaskBluetooth,  (const portCHAR *) "Bluetooth", 128, NULL, 2, NULL );


  // Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
  // Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

void TaskBlink(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  int A[3][3]= {  {30, 32, 34},
                  {38, 40, 42},
                  {46, 48, 50}  };
  
  int B[3][3]= {  {31, 33, 35},
                  {39, 41, 43},
                  {47, 49, 51}  };
  
  int x_axis = 0;
  int y_axis = 0;
  int pot = 0;
  volatile bool flag = 0;
  
  #define MotionPin 13
  #define POT A0
  
  for(int i = 0; i < 3; ++i){
    for(int j = 0; j < 3; ++j){
      pinMode(A[i][j], OUTPUT);
      pinMode(B[i][j], OUTPUT);
    }
  }
  pinMode(MotionPin, INPUT);
  pinMode(POT, INPUT);
  Serial.begin(9600);
  for(int i = 0; i < 3; ++i){
    for(int j = 0; j < 3; ++j){
      digitalWrite(A[i][j], HIGH);
    }
  }

  for (;;) // A Task shall never return or exit.
  { 
    flag = digitalRead(MotionPin);

    if ( flag ){
      digitalWrite(A[x_axis][y_axis], HIGH);
      digitalWrite(B[x_axis][y_axis], LOW);
      x_axis = (x_axis + 1) % 3;    
      digitalWrite(A[x_axis][y_axis], LOW);
      digitalWrite(B[x_axis][y_axis], HIGH);
    }
    pot = map(analogRead(POT), 0, 1023, 0, 1);
    Serial.println(flag);
    if ( pot == 1 ){
      digitalWrite(A[x_axis][y_axis], HIGH);
      digitalWrite(B[x_axis][y_axis], LOW);
      y_axis = (y_axis + 1) % 3;
      digitalWrite(A[x_axis][y_axis], LOW);
      digitalWrite(B[x_axis][y_axis], HIGH);
    }
    delay(500);
  }
}

void TaskBluetooth(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  #define ledPin 21
  volatile bool state = false;
  pinMode(ledPin, OUTPUT);
  pinMode(0, INPUT);
  pinMode(1, OUTPUT);
  bluetooth.begin(9600);
  
  for (;;)
  {
    if( bluetooth.available() ){
      state = bluetooth.read();
    }
    
    if (state) {
        digitalWrite(ledPin, HIGH);
    }
    else{
        digitalWrite(ledPin, LOW);
    }
  }
}
