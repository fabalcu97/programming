/*
  Sparkle.h - Library for flashing led.
  Created by Renato Hurtado, November 28, 2015.
  http://beetlecraft.blogspot.pe/
  
  Released into the public domain.
*/

#ifndef Sparkle_h
#define Sparkle_h

#include "Arduino.h"

/* ************************************************************************************************
Main function : Sparkle name(pin for led, miliseconds)
Initialization function : name.begin()
Main routine : name.spark()
************************************************************************************************ */ 

class Sparkle{
  public:                                   
    Sparkle(int pin_led, int milisegundos); 
    void spark();                           
    void begin();                           
  private:                                  
    int _pin_led;                           
    int _milisegundos;                      
};

#endif
