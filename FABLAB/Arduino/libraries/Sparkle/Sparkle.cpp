/*
  Sparkle.cpp - Library for flashing led.
  Created by Renato Hurtado, November 28, 2015.
  http://beetlecraft.blogspot.pe/
  
  Released into the public domain.
*/

#include "Arduino.h"
#include "Sparkle.h" 

Sparkle::Sparkle(int pin_led, int milisegundos){ 
  _pin_led = pin_led;
  _milisegundos = milisegundos;
}

void Sparkle::begin(){
  pinMode(_pin_led, OUTPUT);
}

void Sparkle::spark(){
  digitalWrite(_pin_led, HIGH); 
  delay(_milisegundos);
  digitalWrite(_pin_led, LOW);
  delay(_milisegundos);  
}
