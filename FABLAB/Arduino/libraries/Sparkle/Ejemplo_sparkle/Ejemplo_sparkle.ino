/*
   Ejemplo del uso de la libreria Sparkle.h
   
   Autor: Renato H.
   http://beetlecraft.blogspot.pe/

   El siguiente programa es de uso publico, cualquier modificacion o mal uso del mismo que pudiera 
   ocasionar el mal funcionamiento de la plataforma de uso de la misma no es responsabilidad del 
   autor.
*/

#include <Sparkle.h> // Libreria Sparkle

const int led = 13;    // Pin asginado al led
const int dtime = 500; // Tiempo de retraso de 500 milisegundos

Sparkle LED(led, dtime);     // Funcion Sparkle asignado al pin 13 con delay de 500 milisegundos
void setup(){ LED.begin(); } // Inicializacion de la funcion Sparkle
void loop(){ LED.spark(); }  // Rutina principal de la funcion Sparkle
