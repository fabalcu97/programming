#include <Arduino.h>
#include "RRobin.h"

List Prioridades[5];
Task* pTask = Prioridades[0].t_head;

ISR(TIMER1_COMPA_vect){
	if (Prioridades[0].t_tail == pTask) {
		pTask = Prioridades[0].t_head;
	}
	else{
		pTask = Prioridades[0].t_tail;
	}
	/*pTask = pTask->t_next;*/
	Serial.print(pTask->t_priority);
}

RRobin::RRobin(){

}

void RRobin::Execute()
{
	cli();          // disable global interrupts
	TCCR1A = 0;     // set entire TCCR1A register to 0
	TCCR1B = 0;     // same for TCCR1B

	// set compare match register to desired timer count:
	OCR1A = 15624;
	// turn on CTC mode:
	TCCR1B |= (1 << WGM12);
	// Set CS10 and CS12 bits for 1024 prescaler:
	TCCR1B |= (1 << CS10);
	TCCR1B |= (1 << CS12);
	// enable timer compare interrupt:
	TIMSK1 |= (1 << OCIE1A);
	sei();          // enable global interrupts
	for(;;)
	{
		pTask->t_function(nullptr);
	}

};

void RRobin::TaskCreate(pFunction foo, int priority, char* name){

	Task* t = new Task(foo, priority, name);
	Prioridades[0].Insert(t);

};
