#include "task.h"

class List{


	public:
		Task* t_head;
		Task* t_tail;
		int t_size;
		List()
		{
			t_head = nullptr;
			t_tail = nullptr;
			t_size = 0;
		};
		void Insert(Task* nTask)
		{
			if ( t_head == nullptr )
			{
				t_head = t_tail = nTask;
				nTask->def_next(nTask);
			}
			else
			{
				Task* tmP = t_tail;
				tmP->t_next = nTask;
				t_tail = nTask;
				t_tail->def_next(t_head);
			}
			t_size ++;
			return;
		};
		void Remove(Task*);
		void Watch()
		{
			Task** tmP = &t_head;
			do {
				/*Serial.print((*tmP)->t_name);
				Serial.print("-");*/
				tmP = &(*tmP)->t_next;
			} while( *tmP != t_tail);
			/*Serial.print((*tmP)->t_name);
			Serial.print("-\n");*/
		};
		int Size()
		{
			return t_size;
		};


};
