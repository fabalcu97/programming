#define BLK 0
#define RNN 1
#define	WTN 2

typedef void(*pFunction)(void *);

struct Task{

	char* t_name;
	int t_priority;
	int t_state;
	pFunction t_function;
	Task* t_next;
	int t_timer;
	//Task* prev;

	Task(pFunction foo, int priority, char* name)
	{
		t_timer = 0;
		t_function = foo;
		t_name = name;
		t_priority = priority;
		t_state = RNN;
	};
	void executionTask(){
		t_function(nullptr);

	}
	void def_next(Task* next){
		t_next = next;
	};
};
