#include <Arduino.h>

#define up 12
#define down 13
#define right 11
#define left 10

//	Variables


const int X = 0;
const int Y =  1;
const int Z = 7;
int axis_x = 0;
int axis_y = 0;
bool axis_z = 0;

void setup()
{
	pinMode(Z, INPUT);
	Serial.begin(9600);
}

void loop()
{
	/*axis_x = map(analogRead(X), 0, 1023, -10, 10);
	axis_y = map(analogRead(Y), 0, 1023, -10, 10);*/

	axis_x = analogRead(X);
	axis_y = analogRead(Y);
	axis_z = digitalRead(Z);

	Serial.write("Eje X: ");
	Serial.println(axis_x);
	Serial.write("Eje Y: ");
	Serial.println(axis_y);
	Serial.write("Eje Z: ");
	Serial.println(axis_z);
	Serial.write("\n");

	if(axis_x == 0 && axis_y == 0){
		digitalWrite(up, LOW);
		digitalWrite(down, LOW);
		digitalWrite(right, LOW);
		digitalWrite(left, LOW);
	}

	if (axis_x < 0)
	{
		digitalWrite(left, HIGH);
	}
	if (axis_x > 0)
	{
		digitalWrite(right, HIGH);
	}
	if (axis_y < 0)
	{
		digitalWrite(down, HIGH);
	}
	if (axis_y > 0)
	{
		digitalWrite(up, HIGH);
	}
	if (axis_z)
	{
		digitalWrite(up, HIGH);
		digitalWrite(down, HIGH);
		digitalWrite(right, HIGH);
		digitalWrite(left, HIGH);
		delay(1000);
		digitalWrite(up, LOW);
		digitalWrite(down, LOW);
		digitalWrite(right, LOW);
		digitalWrite(left, LOW);
		delay(1000);
	}

}
