int A[4][4]= {  {22, 24, 26, 28},
                {30, 32, 34, 36},
                {38, 40, 42, 44},
                {46, 48, 50, 52}  };

int B[4][4]= {  {23, 25, 27, 29},
                {31, 33, 35, 37},
                {39, 41, 43, 45},
                {47, 49, 51, 53}  };

int x = 0;
volatile bool flag = 0;

void setup() {
  for(int i = 0; i < 4; ++i){
    for(int j = 0; j < 4; ++j){
      pinMode(A[i][j], OUTPUT);
      pinMode(B[i][j], OUTPUT);
    }
  }

  Serial.begin(9600);
}

void loop() {
  x = random(3);
  Serial.println(x);
  if (x == 0){
    figura();
  }
  if (x == 1){
    equis();
  }
  if (x == 2){
    seguimiento();
  }
}

void figura(){
  flag = !flag;
  digitalWrite(B[0][0], flag);
  digitalWrite(B[0][1], !flag);
  digitalWrite(B[0][2], !flag);
  digitalWrite(B[0][3], flag);
  digitalWrite(B[1][0], !flag);
  digitalWrite(B[1][1], flag);
  digitalWrite(B[1][2], flag);
  digitalWrite(B[1][3], !flag);
  digitalWrite(B[2][0], !flag);
  digitalWrite(B[2][1], flag);
  digitalWrite(B[2][2], flag);
  digitalWrite(B[2][3], !flag);
  digitalWrite(B[3][0], flag);
  digitalWrite(B[3][1], !flag);
  digitalWrite(B[3][2], !flag);
  digitalWrite(B[3][3], flag);
    
  digitalWrite(A[0][0], !flag);
  digitalWrite(A[0][1], flag);
  digitalWrite(A[0][2], flag);
  digitalWrite(A[0][3], !flag);
  digitalWrite(A[1][0], flag);
  digitalWrite(A[1][1], !flag);
  digitalWrite(A[1][2], !flag);
  digitalWrite(A[1][3], flag);
  digitalWrite(A[2][0], flag);
  digitalWrite(A[2][1], !flag);
  digitalWrite(A[2][2], !flag);
  digitalWrite(A[2][3], flag);
  digitalWrite(A[3][0], !flag);
  digitalWrite(A[3][1], flag);
  digitalWrite(A[3][2], flag);
  digitalWrite(A[3][3], !flag);
  
  delay(500);
}

void seguimiento(){
  for(int i = 0; i < 4; ++i){
    for(int j = 0; j < 4; ++j){
      digitalWrite(B[i][j], LOW);
      digitalWrite(A[i][j], HIGH);
      delay(150);
    }
  }
  for(int i = 0; i < 4; ++i){
    for(int j = 0; j < 4; ++j){
      digitalWrite(A[i][j], LOW);
      digitalWrite(B[i][j], HIGH);
      delay(150);
    }
  }
}

void equis(){
  flag = !flag;
  digitalWrite(B[0][0], !flag);
  digitalWrite(B[0][1], flag);
  digitalWrite(B[0][2], flag);
  digitalWrite(B[0][3], !flag);
  digitalWrite(B[1][0], flag);
  digitalWrite(B[1][1], !flag);
  digitalWrite(B[1][2], !flag);
  digitalWrite(B[1][3], flag);
  digitalWrite(B[2][0], flag);
  digitalWrite(B[2][1], !flag);
  digitalWrite(B[2][2], !flag);
  digitalWrite(B[2][3], flag);
  digitalWrite(B[3][0], !flag);
  digitalWrite(B[3][1], flag);
  digitalWrite(B[3][2], flag);
  digitalWrite(B[3][3], !flag);
    
  digitalWrite(A[0][0], flag);
  digitalWrite(A[0][1], !flag);
  digitalWrite(A[0][2], !flag);
  digitalWrite(A[0][3], flag);
  digitalWrite(A[1][0], !flag);
  digitalWrite(A[1][1], flag);
  digitalWrite(A[1][2], flag);
  digitalWrite(A[1][3], !flag);
  digitalWrite(A[2][0], !flag);
  digitalWrite(A[2][1], flag);
  digitalWrite(A[2][2], flag);
  digitalWrite(A[2][3], !flag);
  digitalWrite(A[3][0], flag);
  digitalWrite(A[3][1], !flag);
  digitalWrite(A[3][2], !flag);
  digitalWrite(A[3][3], flag);
  
  delay(500);

}
