/*
   PROGRAMA DE PRUEBA DE SERVOS: SERVOS ANALOGICOS
 
   CONEXION:
             PWM: Pin 11 del Arduino UNO
             GND: Conectar los dos GND
             VCC: Conectar los dos 5V
    
   En el siguiente programa probaremos la posicon angular de un servo por medio de escritura de su 
   angulo por monitor serial. El angulo inicial es 0 grados.
    
   NOTA: Para el siguiente programa necesitamos la libreria "Servo.h"
    
   Autor: Renato H.
   http://beetlecraft.blogspot.pe/
 
   El siguiente programa es de uso publico, cualquier modificacion o mal uso del mismo que pudiera 
   ocasionar el mal funcionamiento de la plataforma de uso de la misma no es responsabilidad del autor
*/
 
#include <Servo.h> // Libreria de manejo de servos
 
Servo servo_prueba1;        // Nombre asignado al servo
Servo servo_prueba2;        // Nombre asignado al servo
const int salida_pwm1 = 11; // Variable para configuracion del pin de salida de senal PWM
const int salida_pwm2 = 10; // Variable para configuracion del pin de salida de senal PWM
String movimientos = "";

int angulo1 = 0,
    angulo2 = 0;
String  temp1 ="",
        temp2 = "";
bool coma = 0;
 
void setup() {
  Serial.begin(9600);                   // Configuracion del puerto serial de comunicacion con la PC
  servo_prueba1.attach(salida_pwm1);      // Configuracion del pin de salida de la senal PWM
  servo_prueba2.attach(salida_pwm2);      // Configuracion del pin de salida de la senal PWM
  servo_prueba1.writeMicroseconds(2000); // Posicion angular inicial de 1000 us
  servo_prueba2.writeMicroseconds(2000); // Posicion angular inicial de 1000 us
  pinMode(13, OUTPUT);
  Serial.println("Escribir la posicion angular en Grados separados unicamente por coma: ");
  
}
 
void loop() {
  digitalWrite(13, HIGH);
  if (Serial.available()) {
    movimientos = Serial.read();
    delay(10);
    for(int i = 0; i < movimientos.length(); i++){
      if(movimientos[i] == ','){
        coma = 1;
      }
      if(coma){
        temp2 += movimientos[i];
      }
      else{
        temp1 += movimientos[i];
      }
    }
    angulo1 = temp1.toInt();
    angulo2 = temp2.toInt();
    servo_prueba1.write(angulo1);
    servo_prueba2.write(angulo1);

    temp1 = temp2 = "";
    
    while (Serial.available() > 0){Serial.read();} // Rutina de limpieza del buffer del puerto serial
  }
}
