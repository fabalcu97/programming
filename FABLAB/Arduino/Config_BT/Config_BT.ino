
#include <SoftwareSerial.h>
SoftwareSerial BTserialM(13, 12); // RX | TX
SoftwareSerial BTserialE(11, 10); // RX | TX

#define  Alejandro BTserialE
char c = ' ';

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  Alejandro.begin(9600);
  Serial.println("Iniciado");
  digitalWrite(8, HIGH);
}

void loop() {
  
  if (Alejandro.available())
    {  
        c = Alejandro.read();
        Serial.write(c);
    }
 
    // Keep reading from Arduino Serial Monitor and send to HC-05
    if (Serial.available())
    {
        c =  Serial.read();
        Alejandro.write(c);  
    }
}
