// Configuracion del sensor de UltraSonido

#include <NewPing.h>

#define TRIGGER_PIN   12
#define ECHO_PIN      11
#define MAX_DISTANCE  200

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

// Fin configuracion UltraSonido

// Configuracion driver L298N

const int in1 = 10, in2 = 9; // Primer motor
const int in3 = 8, in4 = 7;  // Segundo motor

// Fin configuracion driver L298N

// Auxiliares...

#define ledPin 13
int cm = 0;

// Fin Auxiliares...

void setup() {

  Serial.begin(250000);
  
  pinMode(in1, OUTPUT); pinMode(in2, OUTPUT); // Primer Motor
  pinMode(in3, OUTPUT); pinMode(in4, OUTPUT); // Segundo Motor

  pinMode(ledPin, OUTPUT); // enable output on the led pin
  for (int i = 3; i > 0; i--){
    digitalWrite(ledPin, HIGH);
    delay(1000*i);
    digitalWrite(ledPin, LOW);
    delay(250);
  }
  
}

void loop() {

  delay(50);
  cm = sonar.ping_cm();

  Serial.print("Distancia: ");
  Serial.print(cm); 
  Serial.println("cm");
  
  while (cm < 10){
    avanzar();
    cm = sonar.ping_cm();
  }
  avanzar();
  
}

void avanzar(){
  digitalWrite(in1, HIGH); digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH); digitalWrite(in4, LOW); 
}

void atras(){
  digitalWrite(in1, LOW); digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW); digitalWrite(in4, HIGH); 
}

void derecha(){
  digitalWrite(in1, HIGH); digitalWrite(in2, LOW);
  digitalWrite(in3, LOW); digitalWrite(in4, LOW); 
}

void izquierda(){
  digitalWrite(in1, LOW); digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH); digitalWrite(in4, LOW); 
}




