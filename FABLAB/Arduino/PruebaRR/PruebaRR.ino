// Arduino timer CTC interrupt example
// www.engblaze.com
 
// avr-libc library includes
#include <avr/io.h>
#include <avr/interrupt.h>

bool flag = 0;
void setup()
{    
    Serial.begin(9600);
    pinMode(2, OUTPUT);
    pinMode(8, OUTPUT);
    pinMode(12, OUTPUT);
    pinMode(7, INPUT);
    
    // initialize Timer1
    cli();          // disable global interrupts
    TCCR1A = 0;     // set entire TCCR1A register to 0
    TCCR1B = 0;     // same for TCCR1B
 
    // set compare match register to desired timer count:
    OCR1A = 5624;
    // turn on CTC mode:
    TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler:
    TCCR1B |= (1 << CS10);
    TCCR1B |= (1 << CS12);
    // enable timer compare interrupt:
    TIMSK1 |= (1 << OCIE1A);
    // enable global interrupts:

    //Second timer
    //OCR2A = 15624;
    //TCCR2A = 0;        // set entire TCCR1A register to 0
    //TCCR2B = 0;
 
    //TCCR2B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler:
    //TCCR2B |= (1 << CS10);
    //TCCR2B |= (1 << CS12);
    // enable timer compare interrupt:
    //TIMSK2 |= (1 << OCIE2A);
    // enable global interrupts:
    sei();
}
 
void loop()
{
  flag = digitalRead(7);
  Serial.print(flag);
  if (flag == 1){
    digitalWrite(8, HIGH);
  }
  else{
    digitalWrite(8, LOW);
  }
  delay(250);
}
 
ISR(TIMER1_COMPA_vect)
{
    digitalWrite(2, !digitalRead(2));
}

ISR(TIMER2_COMPA_vect)
{
    digitalWrite(12, !digitalRead(12));
}
