#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int potpin = 0;  // analog pin used to connect the potentiometer
int val;    // variable to read the value from the analog pin
#define servo 9
#define ledPin 13

#define menor 0
#define mayor 180

void setup() {
  myservo.attach(servo);  // attaches the servo on pin 9 to the servo object
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
  //Serial.print(val);
  //Serial.print("\n");
  val = map(val, 0, 1023, menor, mayor);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(val);                  // sets the servo position according to the scaled value
  //Serial.print(val);
  //Serial.print("\n----\n");
  if (val == menor || val == mayor){
    digitalWrite(ledPin, HIGH);
    delay(200);
    digitalWrite(ledPin, LOW);
    delay(200);
  }
  //delay(15);                           // waits for the servo to get there
}
