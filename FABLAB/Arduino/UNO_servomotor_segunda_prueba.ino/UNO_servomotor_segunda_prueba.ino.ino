/*
   PROGRAMA DE PRUEBA DE SERVOS: SERVOS ANALOGICOS
 
   CONEXION:
             PWM: Pin 11 del Arduino UNO
             GND: Conectar los dos GND
             VCC: Conectar los dos 5V
    
   En el siguiente programa probaremos la posicon angular de un servo por medio de escritura de su 
   posicion angular en microsegundos por monitor serial. El angulo inicial es 0 grados o 1000 us.
    
   NOTA: Para el siguiente programa necesitamos la libreria "Servo.h"
    
   Autor: Renato H.
   http://beetlecraft.blogspot.pe/
 
   El siguiente programa es de uso publico, cualquier modificacion o mal uso del mismo que pudiera 
   ocasionar el mal funcionamiento de la plataforma de uso de la misma no es responsabilidad del 
   autor
*/
 
#include <Servo.h> // Libreria de manejo de servos
 
Servo servo_prueba;        // Nombre asignado al servo
const int salida_pwm1 = 11; // Variable para configuracion del pin de salida de senal PWM
int usegundos;             // Variable de almacenamiento del microsegundo ingresado por Monitor Serial
float angulo;              // Variable para calculo del angulo a partir de microsegundos
 
void setup() {
  Serial.begin(9600);                   // Configuracion del puerto serial de comunicacion con la PC
  servo_prueba.attach(salida_pwm1);      // Configuracion del pin de salida de la senal PWM
  servo_prueba.writeMicroseconds(2000); // Posicion angular inicial de 1000 us
  pinMode(13, OUTPUT);
  // Mensaje inicial en el monitor serial
  Serial.println("Escribir la posicion angular en microsegundos de 1000 a 2000: ");
}
 
void loop() {
  digitalWrite(13, HIGH);
  if (Serial.available()) {
   delay(10);               
   usegundos = Serial.parseInt();
   servo_prueba.writeMicroseconds(usegundos);
   
   angulo = 0 + (usegundos - 1000) * 0.18;
    
   // Mensaje de confirmacion del angulo ingresado por Monitor Serial
   Serial.print("Posicion en microsegundos: "); 
   Serial.print(usegundos);
   Serial.print(" us || ");
   Serial.print("Posicion angular: ");
   Serial.print(angulo,0);
   Serial.println(" grados");
    
    while (Serial.available() > 0){Serial.read();} // Rutina de limpieza del buffer
  }
}

