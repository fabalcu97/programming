/*
   PROGRAMA DE COMUNICACION PARA ENCENDER UN LED MEDIANTE BLUETOOTH CON APP INVENTOR

   CONEXION:
             LED:
             LED +: Pin 13 del Arduino UNO
             LED -: Conectar con GND del Arduino UNO

             Modulo bluetooth:
             VCC: Conectar con 3.3V o 5V del Arduino UNO
             GND: Conectar con GND del Arduino UNO
             RX: Conectar con TX del Arduino UNO
             TX: Conectar con RX del Arduino UNO

   En el siguiente programa encenderemos un LED mediante comunicacion bluetooth
   con una aplicacion para android desarrollada en App Inventor.

   Video:
   Applicacion:

   Autor: Fabricio Ballon Cuadros.
   http://beetlecraft.blogspot.pe/

   El siguiente programa es de uso publico, cualquier modificacion o mal uso del mismo que pudiera
   ocasionar el mal funcionamiento de la plataforma de uso de la misma no es responsabilidad del
   autor
*/

#include <Arduino.h>
#include <SoftwareSerial.h>

#define ledPin 10 // pin 13 LED

volatile bool state = false; // Definimos la variable estado como false (apagado)
/*
 * NOTA:
 *      La variable state es declarada como bool pues es el tipo de dato
 *      de menor tamaño, esto es beneficioso porque Arduino UNO tiene una
 *      memoria limitada, ademas el LED solo tiene dos estados, encendido(1) y apagado(0).
*/

SoftwareSerial bluetooth(0, 1); // (rxPin, txPin) Objeto para controlar la comunición bluetooth

void setup() {

    pinMode(ledPin, OUTPUT);  // Permite utiizar el pin 13 como salida

    pinMode(0, INPUT);        // Permite utiizar el pin 0 como entrada
    pinMode(1, OUTPUT);       // Permite utiizar el pin 1 como salida

    bluetooth.begin(9600);     // Iniciamos la comunicación Bluetooth
}

void loop() {

  if( bluetooth.available() ){  // Lee el texto, en este caso el booleano enviado por Bluetooth
    state = bluetooth.read();
  }
  if (state) {    // Si state es 1 o verdadero
      digitalWrite(ledPin, HIGH); // Enciende el LED
  }
  else{    // Si state es 0 o falso
      digitalWrite(ledPin, LOW); // Apaga el LED
  }
  delay(100);
}
