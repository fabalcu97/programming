#define ledPin 13

int frecuencia = 0;

void setup() {
  Serial.begin(115200); // Configuracion del puerto serial de comunicacion
  pinMode(ledPin, OUTPUT);
  
  Serial.println("Frecuencia:");
}
 
void loop() {
  if (Serial.available()) { // Verificaion que el puerto serial recibe datos                                  
   delay(10);                                              
   rotacion_motor(); // Rutina rotacion de motor                        
    while (Serial.available() > 0){Serial.read();} // Rutina de limpieza del buffer del puerto serial
  }
}
 
void rotacion_motor(){
 
  frecuencia = Serial.peek(); 
  Serial.print("Frecuencia a: ");
  Serial.print((frecuencia-48)*1000);
  Serial.print("\n");
  digitalWrite(ledPin, HIGH);
  delay((frecuencia-48)*1000);
  digitalWrite(ledPin, LOW);
  delay(frecuencia>>1);

}
