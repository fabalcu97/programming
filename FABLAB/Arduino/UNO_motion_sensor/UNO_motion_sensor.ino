
#include<Arduino.h>

bool flag = 0;

void setup(){

	pinMode(8, INPUT);
  pinMode(11, OUTPUT);
	Serial.begin(115200);

}

void loop(){

  flag = digitalRead(8);
  if (flag){
      digitalWrite(11, HIGH);
  }
  if (!flag){
      digitalWrite(11, LOW);
  }
  Serial.println( flag );
}
