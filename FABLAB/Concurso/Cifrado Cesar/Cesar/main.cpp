#include <iostream>
#include "Cesar.h"

using namespace std;

int main()
{

    char opt;
    int clave = 0;
    string mensaje = "";

    cout<<"\n       ---------------Cifrado Cesar---------------     \n\n";
    cout<<"             Introduzca la clave que usará: ";
    cin>>clave;

    Cesar A(clave);



    cout<<"\n     ---------------Cifrado Cesar---------------       \n\n";
    cout<<"                 Elija una opción...\n";
    cout<<"                      a) Cifrar\n";
    cout<<"                      b) Descifrar\n";
    cout<<"                      c) Salir\n";

    do{
        cin>>opt;
//        system("clear");
        cout<<"\n     ---------------Cifrado Cesar---------------       \n\n";
        if(opt == 'a'){
            cout<<"         Introduzca el mensaje a cifrar: ";
            cin.ignore();
            getline(cin, mensaje);
            cout<<endl<<"\n         El mensaje cifrado es: \n\n        "<<A.Cifrar(mensaje)<<"     \n\n";

        }
        if(opt == 'b'){
            cout<<"Introduzca el mensaje a descifrar: ";
            cin.ignore();
            getline(cin, mensaje);
            cout<<endl<<"\nEl mensaje descifrado es: \n\n        "<<A.Descifrar(mensaje)<<"     \n\n";

        }
        cout<<"                 Elija una opción...\n";
        cout<<"                      a) Cifrar\n";
        cout<<"                      b) Descifrar\n";
        cout<<"                      c) Salir\n";
    }while(opt != 'c');

}
