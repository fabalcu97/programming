#include "Cesar.h"

Cesar::Cesar(int clave)
{
    key = clave;
    abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
}

string Cesar::Cifrar(string mensaje)
{
    int length = mensaje.size();
    int abc_length = abc.size();
    string result = "";

    for(int i = 0; i < length; i++){
        if ( mensaje[i] == ' ' ){
            result += ' ';
        }
        else{
            result += abc[ ( abc.find(mensaje[i]) + key ) % abc_length ];
        }
    }
    return result;
}

string Cesar::Descifrar(string mensaje)
{
    int length = mensaje.size();
    int abc_length = abc.size();
    string result = "";

    for(int i = 0; i < length; i++){
        if ( mensaje[i] == ' ' ){
            result += ' ';
        }
        else{
            result += abc[ ( abc.find(mensaje[i]) - key ) % abc_length ];
        }
    }
    return result;
}

