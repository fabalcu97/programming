#ifndef CESAR_H
#define CESAR_H

#include <string>

using namespace std;

class Cesar
{
    public:

        Cesar(int);
        string Cifrar(string);
        string Descifrar(string);

    private:

        int key;
        string abc;

};

#endif // CESAR_H
