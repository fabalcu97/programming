import processing.serial.*;

import de.fhpotsdam.unfolding.mapdisplay.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.marker.*;
import de.fhpotsdam.unfolding.tiles.*;
import de.fhpotsdam.unfolding.interactions.*;
import de.fhpotsdam.unfolding.ui.*;
import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.core.*;
import de.fhpotsdam.unfolding.mapdisplay.shaders.*;
import de.fhpotsdam.unfolding.data.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.texture.*;
import de.fhpotsdam.unfolding.events.*;
import de.fhpotsdam.utils.*;
import de.fhpotsdam.unfolding.providers.*;

import org.gwoptics.graphics.graph2D.Graph2D;
import org.gwoptics.graphics.graph2D.LabelPos;
import org.gwoptics.graphics.graph2D.traces.Line2DTrace;
import org.gwoptics.graphics.graph2D.traces.ILine2DEquation;

Graph2D g;

UnfoldingMap Mapa;
Location GPS;
SimplePointMarker GPSMarker;

Serial Puerto;
float LON, LAT;
String mensaje;
boolean flag = true;
boolean flagText = false;
String [] data;
int alto = 0;
int ancho = 0;
int x_axis = 100;
int y_axis = 100;
float x_range = 10;
float y_range = 10;

float xdron = 0;
float ydron = 0;

float xpack = 0;
float ypack = 0;


PGraphics estadisticas;
