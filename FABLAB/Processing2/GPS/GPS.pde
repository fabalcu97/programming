
public void setup() {

  size(displayWidth, displayWidth, P2D);
  if (frame != null) {
    frame.setResizable(true);
  }
  ancho   = 2*(displayWidth/3);
  alto = displayHeight - 70;
  String PuertoArduino = Serial.list()[0];
  Puerto = new Serial(this, PuertoArduino, 9600);

  lectura_datos();

  GPS = new Location(LAT, LON);
  GPSMarker = new SimplePointMarker(GPS);
  Mapa = new UnfoldingMap(this, "mapa", displayWidth-(displayWidth/3), 0, (displayWidth/3), displayHeight/2, true, false, new Google.GoogleMapProvider() );

  Mapa.addMarkers(GPSMarker);
  MapUtils.createDefaultEventDispatcher(this, Mapa);

  g = new Graph2D(this, alto, ancho-250, true); 

  // Definicion de los ejes Y y X
  g.setYAxisMin(0); // Valor minimo del eje Y
  g.setYAxisMax(y_axis); // Valor maximo del eje Y
  g.setYAxisLabel("Latitud"); // Rotulado del eje Y

  g.setXAxisMin(0);  // Valor minimo del eje X
  g.setXAxisMax(x_axis); // Valor maximo del eje X
  g.setXAxisLabel("Longitud"); // Rotulado del eje X

  g.setXAxisTickSpacing(x_range); // Resolucion del espaciamiento del eje X
  g.setYAxisTickSpacing(y_range); // Resolucion del espaciamiento del eje Y

  g.position.x = 70; // Offset del eje X
  g.position.y = 20; // Offset del eje Y
}

public void draw() {
  background(255);
  Mapa.draw();
  g.draw();
  lectura_datos();
  GPS.setLat(LAT);
  GPS.setLon(LON);
  GPSMarker.setLocation(LAT, LON);
  if (flag) {
    Mapa.zoomAndPanTo(GPS, 20);
    flag = false;
  }
  translate(70, 20);
  g.setYAxisMax(y_axis);
  g.setXAxisMax(x_axis);
  posicion1();
  posicion2();  
  translate(-70, -20);
  estadisticas();
}

void lectura_datos() {
  mensaje = Puerto.readStringUntil(10);
  if (mensaje != null) {
    try {
      //print(mensaje);
      data = mensaje.split(":");
      if (data[0].charAt(0) == 'P' && data.length > 3) {
        LAT = Float.parseFloat(data[1]);
        LON = Float.parseFloat(data[2]);
        data[1] = str(LAT);
        data[2] = str(LON);
        //println("LAT: " + LAT + " LON: " + LON);
        flagText = true;
      }
    }
    catch(NumberFormatException e) {
    }
    catch(RuntimeException e) {
    }
  }
}

void estadisticas() {
  int linea = (displayWidth-(displayWidth/3)) + 10;
  int columna = (displayWidth/3) -40;
  rect((displayWidth-(displayWidth/3)), (displayHeight/2), (displayWidth/3), (displayHeight/2));
  fill(0, 0, 0);
  textSize(20);
  try {
    text("Posición:   " + random(3), linea, columna, 0);
    columna += 25;
    text("Altitud:   " + random(3), linea, columna, 0);
    columna += 25;
    text("Velocidad:   " + random(3), linea, columna, 0);
    columna += 25;
    text("Latitud:   " + data[1], linea, columna, 0);
    columna += 25;
    text("Longitud:   " + data[2], linea, columna, 0);
    columna += 25;
    text("Distancia al objetivo:   " + random(3), linea, columna, 0);
  }
  catch(ArrayIndexOutOfBoundsException e) {
  }
  fill(255, 255, 255);
}

void posicion1() {

  fill(255, 0, 0);
  ellipse(150, 150, 20, 20);

  //xdron = map(550 , 0, (displayWidth-(displayWidth/3)), 100, 400);
}

void posicion2() {
  
  fill(0, 0, 255);
  ellipse(0, 661, 20, 20);

  //xdron = map(550 , 0, (displayWidth-(displayWidth/3)), 100, 400);
}

