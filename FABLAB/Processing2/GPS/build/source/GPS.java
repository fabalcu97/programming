import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.serial.*; 
import processing.core.PApplet; 
import de.fhpotsdam.unfolding.UnfoldingMap; 
import de.fhpotsdam.unfolding.geo.Location; 
import de.fhpotsdam.unfolding.utils.MapUtils; 
import de.fhpotsdam.unfolding.marker.SimplePointMarker; 
import de.fhpotsdam.unfolding.providers.Google; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class GPS extends PApplet {










UnfoldingMap Mapa;
Location GPS;
SimplePointMarker GPSMarker;

Serial Puerto;
float LON, LAT;
String mensaje;



PGraphics estadisticas;

public void PG_Est() {
  estadisticas = createGraphics((displayWidth/3), (displayHeight/2), P2D);
  estadisticas.beginDraw();
  estadisticas.background(102);
  estadisticas.stroke(255);
  estadisticas.line(20, 20, 80, 80);
  estadisticas.endDraw();
}

public void setup() {

  size(displayWidth, displayWidth, P2D);
  if(frame != null){
    frame.setResizable(true);
  }

  String PuertoArduino = Serial.list()[0];
  Puerto = new Serial(this, PuertoArduino, 9600);

  delay(1000);

  lectura_datos();

  GPS = new Location(LAT, LON);
  GPSMarker = new SimplePointMarker(GPS);
  Mapa = new UnfoldingMap(this, "mapa", displayWidth-(displayWidth/3), 0, (displayWidth/3), displayHeight/2, true, false, new Google.GoogleTerrainProvider() );
  Mapa.addMarkers(GPSMarker);
  Mapa.zoomAndPanTo(GPS, 15);
  MapUtils.createDefaultEventDispatcher(this, Mapa);

  PG_Est();
}

public void draw() {
  background(0);
  Mapa.draw();
  lectura_datos();
  GPS.setLat(LAT);
  GPS.setLon(LON);
  GPSMarker.setLocation(LAT, LON);
  Mapa.zoomAndPanTo(GPS, 5);
  image(estadisticas, (displayWidth-(displayWidth/3)), (displayHeight/2));
}

public void lectura_datos(){
  mensaje = Puerto.readStringUntil(10);
  if (mensaje != null){
      print(mensaje);
      String [] data = mensaje.split(":");
      if (data[0].charAt(0) == 'P' && data.length > 3){
        LAT = Float.parseFloat(data[1])/1000000;
        LON = Float.parseFloat(data[2])/1000000;
        println("LAT: " + LAT + " LON: " + LON);
      }
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "GPS" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
