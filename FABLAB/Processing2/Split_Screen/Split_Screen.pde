PGraphics pg1, pg2;                             //Creación objectos PGraphics

void setup() {
  
  size(displayWidth, displayHeight);            //Tamaño de la ventana FullScreen
  
  if( frame != null ){                          //Permite que el tamaño de la ventana sea dinámico 
    frame.setResizable(true);
  }
  
  pg1 = createGraphics(width / 2, height);      //Crea una sub-ventana con la mitad del ancho
  pg2 = createGraphics(width / 2, height);      //Crea una sub-ventana con la mitad del ancho

}

void draw() {
  PG_DRAW();                                    //Define los objetos PGraphics
  image(pg1, 0, 0);                             //Grafica los objetos PGraphics
  image(pg2, width/2, 0);

}

void PG_DRAW(){

  pg1.beginDraw();
  pg1.background(111);
  pg1.stroke(000);
  pg1.line(20, 20, mouseX/2, mouseY);
  pg1.endDraw();
  
  pg2.beginDraw();
  pg2.background(00);
  pg2.stroke(000);
  pg2.ellipse(mouseX/2, mouseY, 20, 20);
  pg2.endDraw();
}
