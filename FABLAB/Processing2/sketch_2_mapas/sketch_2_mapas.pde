import de.fhpotsdam.unfolding.mapdisplay.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.marker.*;
import de.fhpotsdam.unfolding.tiles.*;
import de.fhpotsdam.unfolding.interactions.*;
import de.fhpotsdam.unfolding.ui.*;
import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.core.*;
import de.fhpotsdam.unfolding.mapdisplay.shaders.*;
import de.fhpotsdam.unfolding.data.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.texture.*;
import de.fhpotsdam.unfolding.events.*;
import de.fhpotsdam.utils.*;
import de.fhpotsdam.unfolding.providers.*;

import processing.serial.*;

UnfoldingMap Mapa;
public void setup() {
  
  
  //size(displayWidth, displayHeight);
  size(displayWidth, displayWidth, P2D);
  if(frame != null){
    frame.setResizable(true);
  }
  float largo = (displayWidth/3);
  print(largo);
  Mapa = new UnfoldingMap(this, "mapa", displayWidth-(displayWidth/3), 0, (displayWidth/3), displayHeight/2, true, false, new Google.GoogleTerrainProvider() );
  MapUtils.createDefaultEventDispatcher(this, Mapa);

}

public void draw() {
  background(0);
  Mapa.draw();
}
