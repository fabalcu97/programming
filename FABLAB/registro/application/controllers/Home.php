<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->view('home');
	}

	public function submit()
	{
		$form_data = $this->input->post();

		$tmp1 = "";
		$tmp2 = "";

		for ($i=0; $i < 7; $i++) {
			if ($form_data['Entrada'][$i] != ' ') {
				$tmp1 .= $form_data['Entrada'][$i];
			}
			if ($form_data['Salida'][$i] != ' ') {
				$tmp2 .= $form_data['Salida'][$i];
			}
		}

		$form_data['Entrada'] = $tmp1;
		$form_data['Salida'] = $tmp2;

		/*$consult = "SELECT * FROM registro WHERE '".$form_data['f_uso']."'=registro.fecha_uso AND '".$form_data['h_entrada']."' = registro.entrada AND '".$form_data['h_salida']."' = registro.salida AND '".$form_data['Objeto']."' = registro.maquina;";
		$consult = $this->db->query($consult);
		$consult = $consult->result();

		for ($i=0; $i < count($consult); $i++) {
			foreach ($consult[$i] as $key => $value) {
				echo $value;
			}
		}*/

		/*if ( (($form_data['h_entrada'] >= $consult[0]['entrada'] || $form_data['h_entrada'] < $consult[0]['entrada']) && ($form_data['h_salida'] <= $consult[0]['salida'] || $form_data['h_salida'] > $consult[0]['salida']) )  && $consult[0]['maquina'] == $form_data['Objeto'])
		{
			$this->load->view('cruce', $consult->result());
			return;
		}*/

		if ($form_data['Entrada'] > $form_data['Salida']) {
			$data =	array(
			 	'hora' => "Introduzca la hora correcta"
					);
			$this->load->view('home', $data);
			return;
		}

		if ($form_data['Fecha'] < date("d/m/Y") ) {
			$data =	array(
			 	'f_uso' => "Introduzca la fecha correcta"
					);
			$this->load->view('home', $data);
			return;
		}

		$uso = $form_data['Salida']-$form_data['Entrada'];

		$query = "INSERT INTO registro(curso, profesor, encargado, fecha_uso, entrada, salida, tiempo_uso, maquina, material, aprobado, registro) VALUES ('".$form_data['Curso']."', '".$form_data['Profesor']."', '".$form_data['Encargado']."', '".$form_data['Fecha']."', '".$form_data['Entrada']."', '".$form_data['Salida']."', ".$uso.", '".$form_data['Objeto']."', '".$form_data['Material']."', 1, now());";

		$tmp = $this->db->query($query);
		$tmp1 = $this->db->query("SELECT CURRVAL('registro_id_registro_seq') FROM registro LIMIT 1;");
		$tmp1 = $tmp1->row_array();

		if (isset($tmp1['currval']))
		{
			$data['id'] = $tmp1['currval'];
			$data['data'] = $form_data;
		    $this->load->view('registrado', $data);
		}
		else
		{
			echo $query;
			foreach ($this->db->error() as $value) {
				echo $value;
			}
			$this->load->view('Error');
		}

	}

}
