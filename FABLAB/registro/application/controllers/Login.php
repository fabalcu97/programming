<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}

	public function acceso()
	{
		$login = $this->input->post();

		$query = "SELECT * FROM usuario WHERE '".$login['usuario']."'=usuario AND '".$login['contrasenha']."'=contrasenha;";

		$result = $this->db->query($query);
		$result = $result->row_array();

		if (count($result) == 0) {
			$data =	array(
				'error' => 'Introduzca los datos correctamente'
					);
			$this->load->view('login', $data);
			return;
		}


		$logueo = array(
	        'username'  => $result['usuario'],
	        'name'     => $result['nombre'],
	        'logged_in' => TRUE
		);

		$this->session->set_userdata($logueo);
		redirect('http://www.registro.com/index.php/historial');

	}

	public function cerrar()
	{
		$this->session->sess_destroy();
		redirect('http://www.registro.com/index.php/home');
	}
}
