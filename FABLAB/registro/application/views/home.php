<html lang="en">
<head>
	<meta charset="utf-8">
	 <link rel="stylesheet" href="/css/style1.css" type="text/css"/>
	 <link rel="stylesheet" href="/css/jquery-ui.css">
	 <link rel="stylesheet" href="/css/wickedpicker.css">
	 <script src="/js/jquery-1.10.2.js"> </script>
	 <script src="/js/jquery-ui.js"></script>
	 <script type="text/javascript" src="/js/wickedpicker.js"></script>
	<title>Registro de Uso</title>

</head>
<body>

	<div id='login'>
		<a href="http://www.registro.com/index.php/login"> Iniciar Sesión </a>
	</div>

	<h1 style="text-decoration: underline;">Bienvenido al Registro de Uso de FABLAB</h1>

	<div id="body">
		<h2> Por favor complete el siguiente formulario para registrar el uso del laboratorio. </h2>
		<b>
			<form action="http://www.registro.com/index.php/home/submit" method="post">
				<div class="requerimiento">
					<div class="texto">Curso</div>
					<input class="texto" type="text" name="Curso" required="true" placeholder="Curso"/>
				</div>
				<div class="requerimiento">
					<div class="texto">Nombre del profesor</div>
					<input class="texto" type="text" required name="Profesor" placeholder="Nombre del Profesor"/>
				</div>
				<div class="requerimiento">
					<div class="texto">Nombre y Código del encargado del grupo</div>
					<input class="texto" type="text" required name="Encargado" placeholder="Nombre y Código del encargado del grupo(separados por un dos puntos)"/>
				</div>
				<div class="requerimiento">
					<div class="texto">Fecha de Uso
						<?php
						if (isset($f_uso)) {
							echo "<div class='texto' style='color:red; float: right;'> *";
							echo $f_uso;
							echo "</div>";
						}
						?>
					</div>
					<input class="texto" id="datepicker" type="date" required name="Fecha" placeholder="Fecha de Uso"/>
				</div>
				<div class="requerimiento">
					<div class="texto">Hora de Entrada
						<?php
						if (isset($hora)) {
							echo "<div class='texto' style='color:red; float: right;'> *";
							echo $hora;
							echo "</div>";
						}
						?>
					</div>
					<input class="texto timepicker" type="date" required name="Entrada" placeholder="Hora de Entrada">
					</input>
				</div>
				<div class="requerimiento">
					<div class="texto">Hora de Salida</div>
					<input class="texto timepicker" type="date" required name="Salida" placeholder="Hora de Salida"/>
				</div>
				<div class="requerimiento">
					<div class="texto">Máquinas a Usar</div>
					<div class="galeria">
						<input id="checkbox" type="radio" name="Objeto" required value="Impresora 3D"> MakerBot Impresora 3D </input><br>
						<input id="checkbox" type="radio" name="Objeto" required value="Cortadora Laser"> Cortadora Laser </input><br>
						<input id="checkbox" type="radio" name="Objeto" required value="Torno Grande"> Torno emco 1 </input><br>
						<input id="checkbox" type="radio" name="Objeto" required value="Torno Pequeño"> Torno emco 2 </input><br>
						<input id="checkbox" type="radio" name="Objeto" required value="Fresa"> Fresa </input><br>
					</div>
				</div>
				<div class="requerimiento">
					<div class="texto">Material</div>
					<div class="galeria">
						<input id="checkbox" type="radio" name="Material" required value="Propio">
						Material propio</input><br>
						<input id="checkbox" type="radio" name="Material" required value="Laboratorio">
						Material de Laboratorio (previo permiso)</input><br>
					</div>
				</div>
				<br><b><input type="submit" value="Registrar"/></b>
			</form>
		</b>

</div>

<script>
	$(function() {
	  $('#datepicker').datepicker();
	});
</script>
<script>
	$(function() {
	  $('.timepicker').wickedpicker({twentyFour: true});
	});
</script>
</body>
</html>
