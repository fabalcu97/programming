<html lang="en">
<head>
	<meta charset="utf-8">
	 <link rel="stylesheet" href="/css/style1.css" type="text/css"/>
	<title>Registro de Uso</title>

</head>
<body>

	<h1 style="text-decoration: underline;">Iniciar Sesión de FABLAB</h1>

	<div id="body">
			<form action="http://www.registro.com/index.php/login/acceso" method="post">
				<div class="requerimiento">
					<div class="texto">Usuario: </div>
					<input class="texto" type="text" name="usuario" required placeholder="Usuario"></input>
				</div>
				<div class="requerimiento">
					<div class="texto">Contraseña: </div>
					<input class="texto" type="password" name="contrasenha" required placeholder="Contraseña"></input>
				</div>

				<?php
					if (isset($error)) {
						echo "<div style='color: red;' class='texto'> *";
						echo $error;
						echo "</div>";
					}
				?>
				<input type="submit" value="Iniciar Sesión"/>
			</form>
	</div>
</body>
</html>
