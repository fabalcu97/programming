<html lang="en">
<head>
	<meta charset="utf-8">
	 <link rel="stylesheet" href="/css/style1.css" type="text/css"/>
	 <link rel="stylesheet" href="/css/jquery-ui.css">
	 <link rel="stylesheet" href="/css/wickedpicker.css">
	 <script src="/js/jquery-1.10.2.js"> </script>
	 <script src="/js/jquery-ui.js"></script>
	 <script type="text/javascript" src="/js/wickedpicker.js"></script>
	<title>Registro Exitoso</title>

</head>
<body>

	<h1 style="text-decoration: underline;">Registro Exitoso</h1>
	<h4> Recuerda revisar la aprobación con el código de registro.</h4>

	<div id="body">
		<?php
		echo "<div class='requerimiento'><div style='text-decoration: underline;' class='texto'>Código de registro: </div><div class='texto'>";
		echo $id;
		echo "</div></div>";
		?>
		<div class='requerimiento'><div class='texto'> Aprobación: </div><div style='color:yellow;' class='texto'> Pendiente</div></div>
		<?php
		foreach ($data as $key => $value) {
			echo "<div class='requerimiento'><div class='texto'>".$key.": </div><div class='texto'>";
			echo $value;
			echo "</div></div>";
		}
		?>

		<br><br>
	</div>

</div>

</body>
</html>
