<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	 <link rel="stylesheet" href="/css/style2.css" type="text/css"/>
	 <script src="/js/jquery-2.2.3.min.js"> </script>
	 <script type="text/javascript" src="/js/jquery-1.3.2.js"></script>
	 <link rel="stylesheet" type="text/css" href="/css/pagination.css" media="screen"/>
	 <link rel="stylesheet" type="text/css" href="/css/font-awesome.css" media="screen"/>
	 <script src="/js/jquery.paginate.js"> </script>
	<title>Historial de Uso</title>

</head>
<body>
	<div id='login'>
		<a href="http://www.registro.com/index.php/login/cerrar"> Cerrar Sesión </a>
	</div>

	<h1 style="text-decoration: underline;">Historial</h1>

	<h4>
		<div class='row'>
			<div class='cell'>Aprobación</div>
			<div class='cell'>Curso</div>
			<div class='cell'>Profesor</div>
			<div class='cell'>Encargado</div>
			<div class='cell'>F. Uso</div>
			<div class='cell'>H. Salida</div>
			<div class='cell'>H. Entrada</div>
			<div class='cell'>Tiempo de Uso</div>
			<div class='cell'>Máquina</div>
			<div class='cell'>Material</div>
		</h4>
		<div id="container">


		</div>
			<div class="content">
				<div class="demo">
					<button id="aprobar">Aprobar</button>
					<button id="desaprobar">Desaprobar</button>
				</div>
				<div class="demo">
					<div id="demo2">
					</div>
				</div>
			</div>

</body>

	<script type="text/javascript">
		$(document).ready(function()
		{
			$.ajax({
				url: '/index.php/historial/show',
				type: 'GET',
				data: 	{
							limit: 10,
						},
				dataType: 'json',
				success: function(data){
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						if (data[i]['aprobado'] == 0)
						{
							$('#container').append("<div id="+data[i]['id_registro']+" class='row'> <div style='font-size:12px; background-color:red; color:black;' class='cell'><b>DESAPROBADO</b></div><div class='cell'>"+data[i]['curso']+"</div><div class='cell'>"+data[i]['profesor']+"</div><div class='cell'>"+data[i]['encargado']+"</div><div class='cell'>"+data[i]['fecha_uso']+"</div><div class='cell'>"+data[i]['entrada']+"</div><div class='cell'>"+data[i]['salida']+"</div><div class='cell'>"+data[i]['tiempo_uso']+"</div><div class='cell'>"+data[i]['maquina']+"</div><div class='cell'>"+data[i]['material']+"</div>");
						}
						if (data[i]['aprobado'] == 1)
						{
							$('#container').append("<div class='row'><div style='font-size:12px; background-color:yellow; color:black;' class='cell'><b>PENDIENTE<input type='checkbox' onclick=append("+data[i]['id_registro']+") value="+data[i]['id_registro']+"/></b></div><div class='cell'>"+data[i]['curso']+"</div><div class='cell'>"+data[i]['profesor']+"</div><div class='cell'>"+data[i]['encargado']+"</div><div class='cell'>"+data[i]['fecha_uso']+"</div><div class='cell'>"+data[i]['entrada']+"</div><div class='cell'>"+data[i]['salida']+"</div><div class='cell'>"+data[i]['tiempo_uso']+"</div><div class='cell'>"+data[i]['maquina']+"</div><div class='cell'>"+data[i]['material']+"</div>");
						}
						if (data[i]['aprobado'] == 2)
						{
							$('#container').append("<div id="+data[i]['id_registro']+"  class='row'> <div style='font-size:12px; background-color:green; color:black;' class='cell'><b>APROBADO</b></div><div class='cell'>"+data[i]['curso']+"</div><div class='cell'>"+data[i]['profesor']+"</div><div class='cell'>"+data[i]['encargado']+"</div><div class='cell'>"+data[i]['fecha_uso']+"</div><div class='cell'>"+data[i]['entrada']+"</div><div class='cell'>"+data[i]['salida']+"</div><div class='cell'>"+data[i]['tiempo_uso']+"</div><div class='cell'>"+data[i]['maquina']+"</div><div class='cell'>"+data[i]['material']+"</div>");
						}
					}
				},
				error: function(data){
					alert('error');
					console.log(data);
				}
			});
		});
		$(function() {
			$("#demo2").paginate({
				count 		: 50,
				start 		: 1,
				display     : 10,
				border					: false,
				text_color  			: 'white',
				background_color    	: 'transparent',
				text_hover_color  		: 'black',
				background_hover_color	: '#CFCFCF'
			});
		});
	</script>
	<script type="text/javascript">
		var list = [];
		function append(int) {
			list.push(int);
		}
		function change(Limit){
			$('#container').empty();
			$.ajax({
				url: '/index.php/historial/show',
				type: 'GET',
				data: 	{
							limit: parseInt(Limit.innerHTML)*10,
						},
				dataType: 'json',
				success: function(data){
					console.log(data);
					for (var i = 0; i < data.length; i++) {
						if (data[i]['aprobado'] == 0)
						{
							$('#container').append("<div id="+data[i]['id_registro']+" class='row'><div style='font-size:12px; background-color:red; color:black;' class='cell'><b>DESAPROBADO</b></div><div class='cell'>"+data[i]['curso']+"</div><div class='cell'>"+data[i]['profesor']+"</div><div class='cell'>"+data[i]['encargado']+"</div><div class='cell'>"+data[i]['fecha_uso']+"</div><div class='cell'>"+data[i]['entrada']+"</div><div class='cell'>"+data[i]['salida']+"</div><div class='cell'>"+data[i]['tiempo_uso']+"</div><div class='cell'>"+data[i]['maquina']+"</div><div class='cell'>"+data[i]['material']+"</div>");
						}
						if (data[i]['aprobado'] == 1)
						{
							$('#container').append("<div id="+data[i]['id_registro']+" class='row'><div style='font-size:12px; background-color:yellow; color:black;' class='cell'><b>PENDIENTE<input type='checkbox' onclick=append("+data[i]['id_registro']+") value="+data[i]['id_registro']+"/></b></div><div class='cell'>"+data[i]['curso']+"</div><div class='cell'>"+data[i]['profesor']+"</div><div class='cell'>"+data[i]['encargado']+"</div><div class='cell'>"+data[i]['fecha_uso']+"</div><div class='cell'>"+data[i]['entrada']+"</div><div class='cell'>"+data[i]['salida']+"</div><div class='cell'>"+data[i]['tiempo_uso']+"</div><div class='cell'>"+data[i]['maquina']+"</div><div class='cell'>"+data[i]['material']+"</div>");
						}
						if (data[i]['aprobado'] == 2)
						{
							$('#container').append("<div id="+data[i]['id_registro']+"  class='row'> <div style='font-size:12px; background-color:green; color:black;' class='cell'><b>APROBADO</b></div><div class='cell'>"+data[i]['curso']+"</div><div class='cell'>"+data[i]['profesor']+"</div><div class='cell'>"+data[i]['encargado']+"</div><div class='cell'>"+data[i]['fecha_uso']+"</div><div class='cell'>"+data[i]['entrada']+"</div><div class='cell'>"+data[i]['salida']+"</div><div class='cell'>"+data[i]['tiempo_uso']+"</div><div class='cell'>"+data[i]['maquina']+"</div><div class='cell'>"+data[i]['material']+"</div>");
						}
					}
				},
				error: function(data){
					alert('error');
					console.log(data);
				}
			});
		};
	</script>
</html>
