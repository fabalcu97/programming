DROP TABLE registro;

CREATE TABLE registro(
	id_registro SERIAL,
	curso TEXT NOT NULL,
	profesor TEXT NOT NULL,
	encargado TEXT NOT NULL,
	fecha_uso DATE NOT NULL,
	entrada TIME NOT NULL,
	salida TIME NOT NULL,
	tiempo_uso INTEGER NOT NULL,
	maquina TEXT NOT NULL,
	material TEXT NOT NULL,
	aprobado INTEGER NOT NULL,
	registro TIMESTAMP NOT NULL,

	PRIMARY KEY (id_registro)
);

INSERT INTO registro(curso, profesor, encargado, fecha_uso, entrada, salida, tiempo_uso, maquina, material, aprobado, registro) VALUES ('hola', 'hola', 'hola', '12/05/2016', '20:50', '00:10', 4, 'MakerBot', 'Propio', 1, now());

/*//////////////////////*/
DROP TABLE usuario;
CREATE TABLE usuario(
	id_usuario SERIAL,
	nombre TEXT NOT NULL,
	usuario TEXT NOT NULL,
	contrasenha TEXT NOT NULL,

	PRIMARY KEY(id_usuario)
);

INSERT INTO usuario(nombre, usuario, contrasenha) VALUES('Fabricio', 'fabalcu', '123456');
