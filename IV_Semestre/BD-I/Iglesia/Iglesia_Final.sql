CREATE TABLE Usuario(
	idUsuario  SERIAL,
	Nombre text NOT NULL,
	DNI INTEGER NOT NULL,
	Correo_Electronico text,
	usuario text,
	contraseña text NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	Edad INT,
	Sexo VARCHAR(1),

	PRIMARY KEY(idUsuario)
);


CREATE TABLE Jefe_de_grupo(
	idJefe_de_Grupo SERIAL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idJefe_de_Grupo),
	FOREIGN KEY(idJefe_de_Grupo)
		REFERENCES Usuario(idUsuario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Secretario(
	idSecretario INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idSecretario),
	FOREIGN KEY(idSecretario)
		REFERENCES Usuario(idUsuario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Tesorero(
	idTesorero INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idTesorero),
	FOREIGN KEY(idTesorero)
		REFERENCES Usuario(idUsuario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Sacerdote(
	idSacerdote INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idSacerdote),
	FOREIGN KEY(idSacerdote)
		REFERENCES Usuario(idUsuario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Grupo(

	idGrupo  SERIAL,
	Nombre text NOT NULL,
	Finalidad text NOT NULL,
	Fecha_inicio timestamp NOT NULL,
	idJefe_de_Grupo INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	
	PRIMARY KEY(idGrupo),
	FOREIGN KEY(idJefe_de_Grupo)
		REFERENCES Jefe_de_Grupo(idJefe_de_Grupo)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Ingreso_Egresos(
	idIngreso_Egresos  SERIAL,
	Fecha timestamp NOT NULL,
	Tipo text NOT NULL,
	idTesorero INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idIngreso_Egresos),
	FOREIGN KEY(idTesorero)
		REFERENCES Tesorero(idTesorero)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Egresos(
	idEgresos  SERIAL,
	Monto INTEGER NOT NULL,
	Motivo text NOT NULL,
	idIngreso_Egresos INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idEgresos),
	FOREIGN KEY(idIngreso_Egresos)
		REFERENCES Ingreso_Egresos(idIngreso_Egresos)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Ingresos(
	idIngresos  SERIAL,
	Recaudacion INTEGER NOT NULL,
	Descripcion text NOT NULL,
	idIngreso_Egresos INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idIngresos),
	FOREIGN KEY(idIngreso_Egresos)
		REFERENCES Ingreso_Egresos(idIngreso_Egresos)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Presupuesto(
	idPresupuesto  SERIAL,
	Cliente text NOT NULL,
	idSecretario INTEGER NOT NULL,
	idIngreso_Egresos INTEGER NOT NULL,
	Total decimal NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idPresupuesto),
	FOREIGN KEY(idSecretario )
		REFERENCES Secretario(idSecretario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(idIngreso_Egresos)
		REFERENCES Ingreso_Egresos(idIngreso_Egresos)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Inventario(
	idInventario  SERIAL,
	Fecha timestamp NOT NULL,
	Tipo text NOT NULL,
	idTesorero INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idInventario),
	FOREIGN KEY(idTesorero)
		REFERENCES Tesorero(idTesorero)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Objeto(
	idObjeto  SERIAL,
	Nombre text NOT NULL,
	Stock INTEGER NOT NULL,
	Precio decimal NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idObjeto)
);

CREATE TABLE Inventario_has_Objeto(
	
	idInventario INTEGER NOT NULL,
	idObjeto INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idInventario,idObjeto),
	FOREIGN KEY(idInventario)
		REFERENCES Inventario(idInventario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(idObjeto)
		REFERENCES Objeto(idObjeto)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Actividades(
	idActividades  SERIAL,
	Nombre text NOT NULL,
	Tipo text NOT NULL,
	Fecha timestamp NOT NULL,
	Descripcion text NOT NULL,
	Aprobada boolean NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idActividades)
);

CREATE TABLE Actividad_Social(
	idActividad_Social  SERIAL,
	Lugar_de_visita text NOT NULL,
	hora timestamp NOT NULL,
	idActividades INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	
	PRIMARY KEY(idActividad_Social),
	FOREIGN KEY(idActividades)
		REFERENCES Actividades(idActividades)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Actividad_Parroquial(
	idActividad_Parroquial  SERIAL,
	Lugar text NOT NULL,
	hora timestamp NOT NULL,
	idActividades INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	
	PRIMARY KEY(idActividad_Parroquial),
	FOREIGN KEY(idActividades)
		REFERENCES Actividades(idActividades)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Grupo_has_Actividad_Parroquial(
	
	idGrupo INTEGER NOT NULL,
	idJefe_de_Grupo INTEGER NOT NULL,
	idActividad_Parroquial INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idGrupo,idActividad_Parroquial),
	FOREIGN KEY(idGrupo)
		REFERENCES Grupo(idGrupo)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(idJefe_de_Grupo)
		REFERENCES Jefe_de_Grupo(idJefe_de_Grupo)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(idActividad_Parroquial)
		REFERENCES Actividad_Parroquial(idActividad_Parroquial)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Usuario_has_Actividad(
	idUsuario INTEGER NOT NULL,
	idActividades INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idUsuario,idActividades),
	FOREIGN KEY(idUsuario)
		REFERENCES Usuario(idUsuario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(idActividades)
		REFERENCES Actividades(idActividades)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Grupo_has_Usuario(
	idUsuario INTEGER NOT NULL,
	idGrupo INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idUsuario,idGrupo),
	FOREIGN KEY(idUsuario)
		REFERENCES Usuario(idUsuario)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(idGrupo)
		REFERENCES Grupo(idGrupo)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Actividad_Eucaristica(
	idActividad_Eucaristica SERIAL,
	Hora time NOT NULL,
	idActividades INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idActividad_Eucaristica),
	FOREIGN KEY(idActividades)
		REFERENCES Actividades(idActividades)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Actividad_Eucaristica_has_Sacerdote(
	idSacerdote INTEGER NOT NULL,
	idActividad_Eucaristica INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idSacerdote,idActividad_Eucaristica),
	FOREIGN KEY(idSacerdote)
		REFERENCES Sacerdote(idSacerdote)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION,
	FOREIGN KEY(idActividad_Eucaristica)
		REFERENCES Actividad_Eucaristica(idActividad_Eucaristica)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
);

CREATE TABLE Documentos(
	idDocumentos SERIAL,
	Sacramento text NOT NULL,
	Ruta text NOT NULL,
	Fecha timestamp NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idDocumentos)
);

CREATE TABLE Partidas_Bautismo(

	idDocumentos INTEGER NOT NULL,
	idUsuario INTEGER NOT NULL,
	idSacerdote INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idDocumentos),
	FOREIGN KEY (idDocumentos)
		REFERENCES Documentos(idDocumentos)
			ON DELETE No ACTION
			ON UPDATE No ACTION,

	FOREIGN KEY (idUsuario)
		REFERENCES Usuario(idUsuario)
			ON DELETE No ACTION
			ON UPDATE No ACTION,
	
	FOREIGN KEY (idSacerdote)
		REFERENCES Sacerdote(idSacerdote)
			ON DELETE No ACTION
			ON UPDATE No ACTION

);

CREATE TABLE Partidas_Confirmacion(

	idDocumentos INTEGER NOT NULL,
	idUsuario INTEGER NOT NULL,
	idSacerdote INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idDocumentos),
	FOREIGN KEY (idDocumentos)
		REFERENCES Documentos(idDocumentos)
			ON DELETE No ACTION
			ON UPDATE No ACTION,

	FOREIGN KEY (idUsuario)
		REFERENCES Usuario(idUsuario)
			ON DELETE No ACTION
			ON UPDATE No ACTION,
	
	FOREIGN KEY (idSacerdote)
		REFERENCES Sacerdote(idSacerdote)
			ON DELETE No ACTION
			ON UPDATE No ACTION

);
CREATE TABLE Partidas_Matrimonio(

	idDocumentos INTEGER NOT NULL,
	idEsposo INTEGER NOT NULL,
	idEsposa INTEGER NOT NULL,
	idSacerdote INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idDocumentos),
	FOREIGN KEY (idDocumentos)
		REFERENCES Documentos(idDocumentos)
			ON DELETE No ACTION
			ON UPDATE No ACTION,

	FOREIGN KEY (idEsposo)
		REFERENCES Usuario(idUsuario)
			ON DELETE No ACTION
			ON UPDATE No ACTION,

	FOREIGN KEY (idEsposa)
		REFERENCES Usuario(idUsuario)
			ON DELETE No ACTION
			ON UPDATE No ACTION,
	
	FOREIGN KEY (idSacerdote)
		REFERENCES Sacerdote(idSacerdote)
			ON DELETE No ACTION
			ON UPDATE No ACTION

);

CREATE TABLE Certificados_Primera_Comunion(

	idDocumentos INTEGER NOT NULL,
	idUsuario INTEGER NOT NULL,
	idSacerdote INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idDocumentos),
	FOREIGN KEY (idDocumentos)
		REFERENCES Documentos(idDocumentos)
			ON DELETE No ACTION
			ON UPDATE No ACTION,

	FOREIGN KEY (idUsuario)
		REFERENCES Usuario(idUsuario)
			ON DELETE No ACTION
			ON UPDATE No ACTION,
	
	FOREIGN KEY (idSacerdote)
		REFERENCES Sacerdote(idSacerdote)
			ON DELETE No ACTION
			ON UPDATE No ACTION

);

CREATE TABLE Reporte(
	idReporte SERIAL,
	Nombre_de_Reporte text NOT NULL,
	Contenido text NOT NULL,
	Fecha timestamp NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY (idReporte)
);


CREATE TABLE Reporte_diario(

	idReporte INTEGER NOT NULL,
	idSecretario INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idReporte),
	FOREIGN KEY (idReporte)
		REFERENCES Reporte(idReporte)
			ON DELETE No ACTION
			ON UPDATE No ACTION,
	
	FOREIGN KEY (idSecretario)
		REFERENCES Secretario(idSecretario)
			ON DELETE No ACTION
			ON UPDATE No ACTION
);

CREATE TABLE Reporte_financiero(

	idReporte INTEGER NOT NULL,
	idTesorero INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idReporte),
	FOREIGN KEY (idReporte)
		REFERENCES Reporte(idReporte)
			ON DELETE No ACTION
			ON UPDATE No ACTION,
	
	FOREIGN KEY (idTesorero)
		REFERENCES Tesorero(idTesorero)
			ON DELETE No ACTION
			ON UPDATE No ACTION
);

CREATE TABLE Reporte_de_Grupo(

	idReporte INTEGER NOT NULL,
	idJefe_de_Grupo INTEGER NOT NULL,
	Fecha_modificacion_registro timestamp NOT NULL,
	PRIMARY KEY(idReporte),
	FOREIGN KEY (idReporte)
		REFERENCES Reporte(idReporte)
			ON DELETE No ACTION
			ON UPDATE No ACTION,
	
	FOREIGN KEY (idJefe_de_Grupo)
		REFERENCES Jefe_de_Grupo(idJefe_de_Grupo)
			ON DELETE No ACTION
			ON UPDATE No ACTION
);
