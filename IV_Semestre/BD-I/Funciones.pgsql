CREATE OR REPLACE FUNCTION login(uname text, pass text) RETURNS text AS
$BODY$
DECLARE	name TEXT;
BEGIN
	SELECT nombre INTO name FROM usuario WHERE usuario = $1 AND contraseña = $2;
	return name;
END;
$BODY$
LANGUAGE plpgsql VOLATILE