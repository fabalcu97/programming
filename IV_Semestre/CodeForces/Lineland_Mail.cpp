#include <iostream>
#include <vector>
#include <limits.h>

using namespace std;

int abs(int);
int min(int, int);
int max(int, int);

int main()
{

	int total = 0;

	vector<int> lineland;

	cin>>total;

	lineland.resize(total);

	for (int i = 0; i < total; i++)
	{
		cin>>lineland[i];
	}

	for (int i = 0; i < total; i++)
	{
		if ( i == 0 )
		{
			cout<< abs(lineland[i] - lineland[i+1]) <<" "<< abs(lineland[i] - lineland[total-1]) <<endl;
		}
		if ( i == (total - 1) )
		{
			cout<< abs(lineland[i] - lineland[i-1]) <<" "<< abs(lineland[0] - lineland[i]) <<endl;
		}
		if ( (i != 0) && (i != (total - 1)) )
		{
			cout<< min( abs(lineland[i] - lineland[i-1]), abs(lineland[i] - lineland[i+1]) ) <<" "<< max( abs(lineland[i] - lineland[total-1]), abs(lineland[i] - lineland[0]) ) <<endl;
		}
	}

}


int abs(int x)
{
	return x>0?x:x*(-1);
}

int min(int a, int b)
{
	return a>b?b:a;
}

int max(int a, int b)
{
	return a<b?b:a;
}