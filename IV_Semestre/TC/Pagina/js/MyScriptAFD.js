

var Fila = 1,
	Columna = 1,
	RadioFlag = 0,
	init = 0,
	Salida = 0,
	Apt = 0,

	Entradas = [],
    Estados = [],
    Elementos = [],
    Table_Complete = [],
    Estado_Inicial,
    Aceptacion = [],
    A = [];


function createCell(cell, text, style, placeh, number) {
    var div = document.createElement('input');

    div.setAttribute("size", "10");
    div.setAttribute('class', style);
    div.setAttribute('id', Fila+"_"+text);
    div.setAttribute('placeholder', placeh+" "+number);
    cell.appendChild(div);
}

function createState(cell) {
    
    var div = document.createElement('input');

    div.setAttribute("size", "10");
    div.setAttribute('type', 'text');
    div.setAttribute('id', "Estado"+Columna);
    div.setAttribute('placeholder', 'Transición')
    cell.appendChild(div);
}

function createIState(cell, style, type) {
    var div = document.createElement('input');

    div.setAttribute("type", "radio");
    div.setAttribute('id', type + Fila);
    if (type == 'Radbtn') {
        div.setAttribute("onclick", "RadioCheck(this)");
    }
    cell.appendChild(div);
}

function RadioCheck(id) {

	var tmp;

	for (var i = 1; i < Fila && RadioFlag != 1; i++){
		tmp = document.getElementById('Radbtn'+i);
		tmp.checked = false;
	}
	id.checked = true;	
}

function appendState() {
    var tbl = document.getElementById('Mi_Tabla'),
    	colm = tbl.rows[0].insertCell(Columna+1);
    createState(colm);
}
//Agregar Fila
function appendRow() {
    if (init != 0){
    	var tbl = document.getElementById('Mi_Tabla'),
        	row = tbl.insertRow(tbl.rows.length);

    	createIState(row.insertCell(0), "Salida", 'Radbtn');
        for (i = 1; i < tbl.rows[0].cells.length-1; i++) {
            if (i == 1) 
            {
                createCell(row.insertCell(i), i, 'text', 'Estado', Fila);
            }
            else
            {
                createCell(row.insertCell(i), i, 'text', 'Llegada', '');   
            }
        }
        createIState(row.insertCell(tbl.rows[0].cells.length-1), "Salida", 'Aceptacion')
	    Fila++;
	}
	else{
		alert("¡Ingrese el número de columnas porfavor!")
	}
}
//Agregar Columna
function appendColumn() {

    	var tbl = document.getElementById('Mi_Tabla'),
    		Salida = document.getElementById('NumCol').value,
       		i;
    if(init == 0){
	    for (i = 0; i < Salida; i++) {
    		appendState();
    		Columna++;
	    }
	    init = 1;
	    Salida++;
	}
}

// delete table rows with index greater then 0
function deleteRows() {
    var tbl = document.getElementById('Mi_Tabla'),
        lastRow = tbl.rows.length - 1;

    if (lastRow != 0){
        tbl.deleteRow(lastRow);
    }
    Fila--;
}
 
// delete table columns with index greater then 0
function deleteColumns() {
    var tbl = document.getElementById('Mi_Tabla'),
        lastCol = tbl.rows[0].cells.length - 2,
        i;

    if (lastCol >= 2){
        for (i = 0; i < tbl.rows.length; i++) {
            tbl.rows[i].deleteCell(lastCol);
        }
        Columna--;
    }
}

function GenMatrix() {
    var tbl = document.getElementById('Mi_Tabla'),
    	Cols = parseInt(document.getElementById('NumCol').value),
        col = tbl.rows[0].cells.length,
        fil = tbl.rows.length,
        i = 0,
        j = 0,
        tmp,
        tmp1;

   	for (i = 2; i < Cols+2; i++){
   		j = i-1;
   		Entradas[i-2] = document.getElementById("Estado"+j).value;
   	}

	for (i = 1; i < Fila; i++){
		Elementos[i-1] = [];
		for (j = 1; j <= Cols; j++){
			tmp = j+1;
			Elementos[i-1][j-1] = document.getElementById(i+"_"+tmp).value; 
		}
   	}

   	for (i = 1; i < Fila; i++){
   		Estados[i-1] = document.getElementById(i+"_"+1).value; 
   	}

   	for (var i = 0; i < Fila; i++){
    	Table_Complete[i] = [];
    	for (var j = 0; j < Cols+1; j++){
    		Table_Complete[i][j] = 0;
    	}
    }

    for(var i = 0; i < Cols; i++){
    	Table_Complete[0][i+1] = Entradas[i];
    }
    
    for(var i = 1; i < Fila; i++){
    	Table_Complete[i][0] = Estados[i-1];
    }

    for (var i = 1; i < Fila; i++){
    	for (var j = 1; j < Cols+1; j++){
    		Table_Complete[i][j] = Elementos[i-1][j-1];
    	}
    }
    Table_Complete[0][0] = '-';
    for (var i = 1; i < Fila; i++){
		if (document.getElementById('Radbtn'+i).checked == true){
			Estado_Inicial = Estados[i-1]
        }
    }
    for (var i = 0; i < Fila-1; i++){
        if (document.getElementById('Aceptacion'+(i+1)).checked == true){
            Aceptacion[Apt] = Estados[i];
            Apt++;
        }
    }
    valores();

    /*ESTADOS DE TRANSICION*/
    document.write("<table align='center' border='2px solid'");
    for (i=0; i<Table_Complete.length; i++){
        
        document.write("<tr>");
        for (j=0; j<Table_Complete[0].length; j++){
            document.write("<td>");
            document.write(Table_Complete[i][j]);
            document.write("</td>");
        }
        document.write("</tr>");
    }
    document.write("</table>");
    document.write("<br>");

    document.write(Estado_Inicial);
    document.write("<br>");

    for (i=0; i<Aceptacion.length; i++){
        
        document.write(Aceptacion[i]);
    }

    document.write("<br>");
    document.write("<table align='center' border='2px solid'");
    for (i=0; i<A.length; i++){
        
        document.write("<tr>");
        for (j=0; j<A[0].length; j++){
            document.write("<td>");
            document.write(A[i][j]);
            document.write("</td>");
        }
        document.write("</tr>");
    }
    document.write("</table>");
    document.write("<br>");


}

function valores(){
    var tmp;
    for (var i = 0; i < Estados.length; i++) {
        A[i] = [];
        tmp = Estados[i];
        for(var j = 0; j < Elementos.length; j++){
            if (tmp == Elementos[i][j]){
                A[i][i] = Table_Complete[0][j+1];
            }
            else{
                var v = 0;
                for (; v < Estados.length; v++){
                    if (Estados[v] == Elementos[i][j]){
                        break;
                    };
                }
                if (v < Estados.length){
                    A[i][v] = Table_Complete[0][j+1];
                }
            }
        }
    };
};

