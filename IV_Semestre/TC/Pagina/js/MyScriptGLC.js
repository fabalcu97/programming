
var flag = 1;
var f = 0;
var ffc = 0;
var Transiciones = [];
var Apunta = [];
var Estados = [];
var Estado_Inicial;
var RadioFlag = 0;

function SetValue(id){
    id.setAttribute('value', ' ');
}

function createCell(cell, text1, text, style, name) {
    var div = document.createElement('input'), // create DIV element
        txt = document.createTextNode(text); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('class', style);        // set DIV class attribute
    div.setAttribute('id', text1+"_"+text);
    div.setAttribute('value', name);
    div.setAttribute('readonly', 'readonly');
    cell.appendChild(div);           
}

function createAim(cell, text1, text, style) {
    var div = document.createElement('input'), // create DIV element
        txt = document.createTextNode(text); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('class', style);        // set DIV class attribute
    div.setAttribute('placeholder', 'Ingrese Estado');
    div.setAttribute('id', "Aim_"+text1+"_"+text);
    cell.appendChild(div);           
}
function createTransition(cell, text1, text, style) {
    var div = document.createElement('input'), // create DIV element
        txt = document.createTextNode(text); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('class', style);        // set DIV class attribute
    div.setAttribute('placeholder', 'Ingrese cambio');
    div.setAttribute('id', "Transition_"+text1+"_"+text);
    cell.appendChild(div);           
}
//Agregar Fila

function createIState(cell, style) {
    var div = document.createElement('input');

    div.setAttribute("type", "radio");
    div.setAttribute('id', "Radbtn" + f); 
    div.setAttribute("onclick", "RadioCheck(this)");
    cell.appendChild(div);
}

function RadioCheck(id) {

    var tmp;

    for (var i = 0; i < f && RadioFlag != 1; i++){
        tmp = document.getElementById('Radbtn'+i);
        tmp.checked = false;
    }
    id.checked = true;  
}

function createState(cell, text, style) {
    var div = document.createElement('input'), // create DIV element
        txt = document.createTextNode(text); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('class', style);        // set DIV class attribute
    div.setAttribute('id', "Estado_"+text);
    div.setAttribute('placeholder', "Estado "+(text+1));
    cell.appendChild(div);    
}

function appendRow() {
    var tbl = document.getElementById('Transiciones'), // table reference
        Estados = document.getElementById('NumEstados').value,
        j = 0;

    while(Estados > 0){
        row = tbl.insertRow(tbl.rows.length);        // append table row
        createIState(row.insertCell(0), "EI_")
        createState(row.insertCell(1), j, "Estado");
        j++;
        f++;
        Estados--;
    }      
}

//Agregar Columna
function appendColumn() {
    var tbl = document.getElementById('Transiciones'),
        i;

        createCell(tbl.rows[0].insertCell(), i-1, ffc, 'col', 'Transicion');
        createCell(tbl.rows[0].insertCell(), i-1, ffc, 'col', 'Estado');
    for (i = 1; i < tbl.rows.length; i++) {
        createTransition(tbl.rows[i].insertCell(), i-1, ffc, 'sec');
        createAim(tbl.rows[i].insertCell(), i-1, ffc, 'sec');
    }
    ffc++;
}
 
// delete table columns with index greater then 0
function deleteColumns() {
    var tbl = document.getElementById('Transiciones'),
        lastCol = tbl.rows[1].cells.length,
        i;

    if (lastCol > 2){
        for (i = 1; i < tbl.rows.length; i++) {
            tbl.rows[i].deleteCell(lastCol-1);
        }
    }
}

function Convertir(){

    var Columnas = ffc + 1,
        Filas = document.getElementById('NumEstados').value;

    for (i = 0; i < Filas; i++){
        Apunta[i] = [];
        Transiciones[i] = [];
        Estados[i] = 0;
        for (j = 0; j < Columnas-2; j++){
            Apunta[i][j] = 0;
            Transiciones[i][j] = 0;
        }
    }

    for (i = 0; i < Filas; i++){
        Estados[i] = document.getElementById("Estado_"+i).value;
    }

    for (i = 0; i < Filas; i++){
        for (j = 0; j < Columnas-1; j++){
            Apunta[i][j] = document.getElementById("Aim_"+i+"_"+j).value;
            Transiciones[i][j] = document.getElementById("Transition_"+i+"_"+j).value;
        }
    }

    for (var i = 1; i < f; i++){
        if (document.getElementById('Radbtn'+i).checked == true){
            Estado_Inicial = Estados[i-1]
        }
    }

    document.write("Estados->");
    document.write("<table align='center' border='2px solid'");
    for (i=0; i<Estados.length; i++){
        
        document.write("<tr>");
        document.write("<td>");
        document.write(Estados[i]);
        document.write("</td>");
        document.write("</tr>");
    }
    document.write("</table>");

    document.write("Razón->");
    document.write("<table align='center' border='2px solid'");
    for (i=0; i<Transiciones.length; i++){
        
        document.write("<tr>");
        for (j=0; j<Transiciones[0].length; j++){
            document.write("<td>");
            document.write(Transiciones[i][j]);
            document.write("</td>");
        }
        document.write("</tr>");
    }
    document.write("</table>");

    document.write("Apunta->");
    document.write("<table align='center' border='2px solid'");
    for (i=0; i<Apunta.length; i++){
        
        document.write("<tr>");
        for (j=0; j<Apunta[0].length; j++){
            document.write("<td>");
            document.write(Apunta[i][j]);
            document.write("</td>");
        }
        document.write("</tr>");
    }
    document.write("</table>");




}
