// append row to the HTML table

var it = 1;
var State;
var f = 1;
var counter = 0;
 
// create DIV element and append to the table cell
function createCell(cell, text, style) {
    var div = document.createElement('input'), // create DIV element
        txt = document.createTextNode(text); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('class', style);        // set DIV class attribute
    div.setAttribute('id', it+"_"+counter);
    cell.appendChild(div);           
    counter++;
}

function createState(cell, text, style) {
    var div = document.createElement('div'), // create DIV element
        txt = document.createTextNode("Estado "+it); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('id', style);        // set DIV class attribute
    cell.appendChild(div);                   // append DIV to the table cell
}

function createOutput(cell, style) {
    var div = document.createElement('div'), // create DIV element
        txt = document.createTextNode("Salida"); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    div.setAttribute('id', style);        // set DIV class attribute
    cell.appendChild(div);                   // append DIV to the table cell
}

function createIState(cell, style) {
    var div = document.createElement('input'); // create DIV element
    div.setAttribute("type", "checkbox");        // set DIV class attribute
    div.setAttribute('id', "Check_"+f); 
    cell.appendChild(div);                   // append DIV to the table cell
}

function appendState() {
    var tbl = document.getElementById('Mi_Tabla'), // table reference
        colm = tbl.rows[0].insertCell(it+1)
    createState(colm, it, 'Estados');
    it++;
}


function appendOutput() {
        if (flag == 0){
            var tbl = document.getElementById('Mi_Tabla'), // table reference
                colm = tbl.rows[0].insertCell(it)
            createOutput(colm, 'Salida');
            flag = 1;
        }
}

//Agregar Fila
function appendRow() {
    var tbl = document.getElementById('Mi_Tabla'), // table reference
        row = tbl.insertRow(tbl.rows.length);      // append table row

    	createIState(row.insertCell(0), "Salida")
    for (i = 1; i < tbl.rows[0].cells.length; i++) {
        createCell(row.insertCell(i), i, 'row');
    }
    f++;
}
//Agregar Columna
function appendColumn() {
    var tbl = document.getElementById('Mi_Tabla'), // table reference
        i;
    // open loop for each row and append cell
    appendState();    
    for (i = 1; i < tbl.rows.length; i++) {
        createCell(tbl.rows[i].insertCell(), i, 'col');
    }
}

// delete table rows with index greater then 0
function deleteRows() {
    var tbl = document.getElementById('Mi_Tabla'), // table reference
        lastRow = tbl.rows.length - 1,             // set the last row index
        len = tbl.rows[0].cells.length;
    // delete rows with index greater then 0
    if (lastRow != 0){
        tbl.deleteRow(lastRow);
        counter -= len;
    }
}
 
// delete table columns with index greater then 0
function deleteColumns() {
    var tbl = document.getElementById('Mi_Tabla'), // table reference
        lastCol = tbl.rows[0].cells.length - 2,    // set the last column index
        i;

    if (lastCol >= 2){
        for (i = 0; i < tbl.rows.length; i++) {
            tbl.rows[i].deleteCell(lastCol);
        }
        it--;
    }
}