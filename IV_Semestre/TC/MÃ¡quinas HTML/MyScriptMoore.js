

var Fila = 1,
	Columna = 1,
	RadioFlag = 0,
	init = 0,
	Salida = 0,
	
	Entradas = [],
    Estados = [],
    Salidas = [],
    Elementos = [],
    Table_Complete = [],
    Estado_Inicial;


function createCell(cell, text, style) {
    var div = document.createElement('input');

    div.setAttribute("size", "7");
    div.setAttribute('class', style);
    div.setAttribute('id', Fila+"_"+text);
    cell.appendChild(div);
}

function createState(cell) {
    
    var div = document.createElement('input');

    div.setAttribute("size", "7");
    div.setAttribute('class', 'text');
    div.setAttribute('id', "Estado"+Columna);
    cell.appendChild(div);
}

function createIState(cell, style) {
    var div = document.createElement('input');

    div.setAttribute("type", "radio");
    div.setAttribute('id', "Radbtn" + Fila); 
    div.setAttribute("onclick", "RadioCheck(this)");
    cell.appendChild(div);
}

function RadioCheck(id) {

	var tmp;

	for (var i = 1; i < Fila && RadioFlag != 1; i++){
		tmp = document.getElementById('Radbtn'+i);
		tmp.checked = false;
	}
	id.checked = true;	
}

function appendState() {
    var tbl = document.getElementById('Mi_Tabla'),
    	colm = tbl.rows[0].insertCell(Columna+1);
    createState(colm);
}


//Agregar Fila
function appendRow() {
    if (init != 0){
    	var tbl = document.getElementById('Mi_Tabla'),
        	row = tbl.insertRow(tbl.rows.length);

    	createIState(row.insertCell(0), "Salida")
    	for (i = 1; i < tbl.rows[0].cells.length; i++) {
	        createCell(row.insertCell(i), i, 'text');
	    }
	    Fila++;
	}
	else{
		alert("¡Ingrese el número de columnas porfavor!")
	}
}
//Agregar Columna
function appendColumn() {

    	var tbl = document.getElementById('Mi_Tabla'),
    		Salida = document.getElementById('NumCol').value,
       		i;

    if(init == 0){
	    for (i = 0; i < Salida; i++) {
    		appendState();
    		Columna++;
	    }
	    init = 1;
	    Salida++;
	}
}

// delete table rows with index greater then 0
function deleteRows() {
    var tbl = document.getElementById('Mi_Tabla'),
        lastRow = tbl.rows.length - 1;

    if (lastRow != 0){
        tbl.deleteRow(lastRow);
    }
    Fila--;
}
 
// delete table columns with index greater then 0
function deleteColumns() {
    var tbl = document.getElementById('Mi_Tabla'),
        lastCol = tbl.rows[0].cells.length - 2,
        i;

    if (lastCol >= 2){
        for (i = 0; i < tbl.rows.length; i++) {
            tbl.rows[i].deleteCell(lastCol);
        }
        Columna--;
    }
}

function SetValue(){
	var txt = document.getElementById('NumCol');
	txt.setAttribute('value', ' ');
}

function GenMatrix() {
    var tbl = document.getElementById('Mi_Tabla'),
    	Cols = parseInt(document.getElementById('NumCol').value),
        col = tbl.rows[0].cells.length,
        fil = tbl.rows.length,
        i = 0,
        j = 0,
        tmp,
        tmp1;

   	for (i = 2; i < Cols+2; i++){
   		j = i-1;
   		Entradas[i-2] = document.getElementById("Estado"+j).value;
   	}

	for (i = 1; i < Fila; i++){
		Elementos[i-1] = [];
		for (j = 1; j <= Cols; j++){
			tmp = j+1;
			Elementos[i-1][j-1] = document.getElementById(i+"_"+tmp).value; 
		}
   	}

   	for (i = 1; i < Fila; i++){
   		Salidas[i-1] = document.getElementById(i+"_"+(Cols+2)).value; 
   	}

   	for (i = 1; i < Fila; i++){
   		Estados[i-1] = document.getElementById(i+"_"+1).value; 
   	}

   	for (var i = 0; i < Fila; i++){
    	Table_Complete[i] = [];
    	for (var j = 0; j < Cols+2; j++){
    		Table_Complete[i][j] = 0;
    	}
    }

    for(var i = 0; i < Cols; i++){
    	Table_Complete[0][i+1] = Entradas[i];
    }
    
    for(var i = 1; i < Fila; i++){
    	Table_Complete[i][0] = Estados[i-1];
    	Table_Complete[i][Cols+1] = Salidas[i-1];
    }

    for (var i = 1; i < Fila; i++){
    	for (var j = 1; j < Cols+1; j++){
    		Table_Complete[i][j] = Elementos[i-1][j-1];
    	}
    }
    for (var i = 1; i < Fila; i++){
		if (document.getElementById('Radbtn'+i).checked == true){
			Estado_Inicial = Estados[i-1]
		}
	}
}