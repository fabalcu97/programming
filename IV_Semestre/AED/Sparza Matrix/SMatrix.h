#include <iostream>
#include "Node.h"

using namespace std;

template <typename T>

class CSMatrix{
	
	private:

		CSNode<T>** m_col;
		CSNode<T>** m_row;

		int m_x;		//Columns
		int m_y;		//Rows

	public:

		CSMatrix(int row, int col)
		{
			m_x = col;
			m_y = row;
			m_col = new CSNode<T>*[m_x];
			m_row = new CSNode<T>*[m_y];
		}
		
		bool insert(T data, int x, int y)
		{
			CSNode<T>** C = nullptr;
			CSNode<T>** R = nullptr;

			if ( x > m_x || y > m_y)
			{
				return 0;
			}
			find(data, C, R);

			CSNode<T>* tmp = new CSNode<T>(data, x, y);
			
			tmp->set_down((*C));
			tmp->set_right((*R));
			*C = *R = tmp;

			return 1;
		}

		bool find(T data, int x, int y)		//Find normal :3
		{
			CSNode<T>** tmp;
			for (int i = 0; i < m_x; ++i)
			{
				for( tmp = &m_col[i]; (*tmp); tmp = &(*tmp)->m_down )
				{
					if( (*tmp)->is_data(data, x, y) )
					{
						return 1;
					}
				}
								
			}
			return 0;
		}

		bool find(T data, CSNode<T>**& C, CSNode<T>**& R)
		{
			for (int x = 0; x < m_x; ++x)
			{
				for (int y = 0; y < m_y; ++y)
				{
					if( m_col[x] == m_row[y] )
					{
						C = &m_col[x];
						R = &m_row[y];
						return 1;
					}
				}
			}
			return 0;
		}

		void find(T data, int x, int y, CSNode<T>**& C, CSNode<T>**& R)
		{

			for ( C  = m_col; (*C) && !(*C)->is_data(data, x, y); C = &(*C)->m_down);

			for ( R  = m_row; (*R) && !(*R)->is_data(data, x, y); R = &(*R)->m_right);

		}

		bool remove(T data, int x, int y)
		{
			CSNode<T>** C = nullptr;
			CSNode<T>** R = nullptr;

			find(data, x, y, C, R);

			CSNode<T>* tmp = (*C);

			*C = (*C)->get_down();
			*R = (*R)->get_right();

			delete tmp;
			return 1;
		}

		void print()
		{
			for (int x = 0; x < m_x; ++x)
			{
				cout<<" ( ";
				for (int y = 0; y < m_y; ++y)
				{
					if (m_row[x] && m_col[y])
					{
						cout<<m_col[y]->get_data()<<"-";
					}
				}
				cout<<" ) \n";
			}
		}

		T operator()(int x, int y)
		{

		}

		CSNode<T>* operator=(T x)
		{

		}

		T operator= (CSNode<T>* x)
		{
			return x->m_data;
		}

};
