#ifndef LISTA_CIRCULAR_H
#define LISTA_CIRCULAR_H

#include <iostream>
#include <thread>
#include <mutex>
#include "node.h"

using namespace std;

template <typename Tr>
class Lista_Circular{


    public:

        typedef typename Tr::T T;
        typedef typename Tr::I Iterator;

        int ID;
        mutex F;
        Node<T>* Head;
        Node<T>* Tail;
        Iterator m_iterator = begin();

        Lista_Circular(){
            ID = 0;
            Head = Tail = nullptr;
        }


        void Insert(T x, int t){

            Node<T>* tmp = new Node<T>(x, t);
            if (Tail == Head && Head == nullptr){
                Tail = tmp;
                Head = tmp;
                Tail->Next = Head;
                Tail->Prev = Tail;
                m_iterator = begin();
            }
            else{
                tmp->Prev = Tail;
                Tail->Next = tmp;
                tmp->Next = Head;
                Tail = tmp;
                Head->Prev = Tail;
            }
            ID++;
        }

        bool Empty(){
            if (Head == nullptr){
                return 1;
            }
            return 0;
        }
        void Remove(T x){
            Node<T>* tmp = Find(x);
            if (Tail == Head){
                tmp = Head;
                Tail = nullptr;
                Head = nullptr;
                m_iterator.m_it = nullptr;
                delete tmp;
                return;
            }
            if (tmp == Head){
                Head = tmp->Next;
                Head->Prev = Tail;
                Tail->Next = Head;
                m_iterator = begin();
                delete tmp;
                ID--;
                return;
            }
            if (tmp == Tail){
                Tail = tmp->Prev;
                Tail->Next = Head;
                Head->Prev = Tail;
                m_iterator = begin();
                delete tmp;
                ID--;
                return;
            }
            Node<T>* tmp1 = tmp->Prev;
            tmp1->Next = tmp->Next;
            tmp1->Next->Prev= tmp1;
            delete tmp;
            ID--;
        }

        Node<T>* Find(int x){

            Node<T>* tmp = Head;
            while( tmp->valor != x ){

                tmp = tmp->Next;
                if (tmp == Head){
                    return nullptr;
                }
            }
            return tmp;
        }

        Iterator begin(){
           return Iterator(&Head);
        }

        Iterator end(){
            return Iterator(&(Tail->Next));
        }

};

#endif // LISTA_CIRCULAR_H
