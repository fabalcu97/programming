TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11

SOURCES += main.cpp

HEADERS += \
    node.h \
    lista_circular.h \
    Iterator.h

LIBS += -lcurses -lpthread
