
#include "node.h"

/*template<typename T>
class C_Iterator{

    protected:
        Node<T>* m_it;
    public:
        C_Iterator(Node<T>* p = 0){
            m_it = p;
        }
        C_Iterator operator= (C_Iterator<T> x){
            m_it = x.m_it;
            return *this;
        }

        C_Iterator operator== (C_Iterator<T> x){
            return m_it == x.m_it;
        }

        bool operator!= (C_Iterator<T> x){
            return m_it != x.m_it;
        }

        T operator* (){
            return m_it->valor;
        }
};*/


template<typename T>
class C_Iterator_forward{


    public:

        Node<T>** m_it;

        C_Iterator_forward(Node<T>** p = 0){
            m_it = p;
       }

        C_Iterator_forward operator--(int){
            m_it = &((*m_it)->Prev);
            return *this;
        }
        bool operator== (C_Iterator_forward<T> x){
            return m_it == x.m_it;
        }
        C_Iterator_forward operator++(int){
            m_it = &((*m_it)->Next);
            return *this;
        }


        C_Iterator_forward operator= (C_Iterator_forward<T> x){
            m_it = x.m_it;
            return *this;
        }

        bool operator!= (C_Iterator_forward<T> x){
            return (        m_it) != (x.m_it);
        }

        T operator* (){
            return (*m_it)->valor;
        }

};




/*template<typename T>
class C_Iterator_backguards{

    private:
        Node<T>* m_it;

    public:
        C_Iterator_backguards(Node<T>* p = 0){
            m_it = p;
        }

        bool operator== (C_Iterator_backguards<T> x){
            return m_it == x.m_it;
        }

        C_Iterator_backguards operator++(){
            m_it = m_it->Prev;
            return *this;
        }

        C_Iterator_backguards operator= (C_Iterator_backguards<T> x){
            m_it = x.m_it;
            return *this;
        }

        bool operator!= (C_Iterator_backguards<T> x){
            return m_it != x.m_it;
        }

        T operator* (){
            return m_it->valor;
        }
};*/
