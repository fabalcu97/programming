#include "lista_circular.h"

Lista_Circular::Lista_Circular(){
    Head = Tail = nullptr;
}

void Lista_Circular::Insert(int x, int t){

    Node* tmp = new Node(x, t);
    if (Tail == Head && Head == nullptr){
        Tail = tmp;
        Head = tmp;
        Tail->Next = Head;
        Tail->Prev = Tail;
        TT = Head;
    }
    else{
        tmp->Prev = Tail;
        Tail->Next = tmp;
        tmp->Next = Head;
        Tail = tmp;
        Head->Prev = Tail;
    }
    ID++;
}

bool Lista_Circular::Empty(){
    if (Head == nullptr){
        return 1;
    }
    return 0;
}

void Lista_Circular::Remove(int x){
    Node* tmp = Find(x);
    if (Tail == Head){
        tmp = Head;
        Tail = nullptr;
        Head = nullptr;
        TT = nullptr;
        delete tmp;
        return;
    }
    if (tmp == Head){
        Head = tmp->Next;
        Head->Prev = Tail;
        Tail->Next = Head;
        TT = Head;
        delete tmp;
        ID--;
        return;
    }
    if (tmp == Tail){
        Tail = tmp->Prev;
        Tail->Next = Head;
        Head->Prev = Tail;
        TT = Head;
        delete tmp;
        ID--;
        return;
    }
    Node* tmp1 = tmp->Prev;
    tmp1->Next = tmp->Next;
    tmp1->Next->Prev= tmp1;
    delete tmp;
    ID--;
}

Node* Lista_Circular::Find(int x){
    Node* tmp = Head;

    while( tmp->valor != x ){

        tmp = tmp->Next;
        if (tmp == Head){
            return nullptr;
        }
    }
    return tmp;
}
