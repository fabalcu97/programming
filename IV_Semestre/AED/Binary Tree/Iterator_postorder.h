#ifndef ITERATOR_POSOTRDER_H
#define ITERATOR_POSOTRDER_H

/**
 *	0->Visita izquierda
 *	1->Visita Nodo
 *	2->Visita Derecha
 *	3->Sube->(opcional)
**/
	
#include <tuple>
#include <stack>

template <typename T>
class CIterator_p{
	
	public:
		stack< pair<CNode<T>*, int> > m_pila;

		CIterator_p(stack< pair<CNode<T>*, int> > Pila = 0){
			m_pila = Pila;
		}

		void operator =(CIterator_p<T> x){
			m_pila = x.m_pila;
		}
		
		bool operator !=(CIterator_p<T> x){
			return m_pila != x.m_pila;
		}

		bool operator ==(CIterator_p<T> x){
			return m_pila == x.m_pila;
		}

		T operator *(){
			return m_pila.top().first->m_data;
		}

		CIterator_p<T> operator ++(int){

			pair< CNode<T>*, int >* tmp(&m_pila.top());
			pair< CNode<T>*, int > tmp1;
			m_pila.pop();
			while( tmp->second != 2 && !m_pila.empty())
			{
				if (tmp->second == 0)
				{
					tmp->second++;
					if (tmp->first->m_child[0])
					{
						tmp1.first = tmp->first->m_child[0];
						tmp1.second = 0;
						m_pila.push(tmp1);
					}
				}

				/*else if (tmp->second == 1)
				{
					tmp->second++;
					return *this;
				}*/

				else if (tmp->second == 1)
				{
					tmp->second++;
					if (tmp->first->m_child[1])
					{
						tmp1.first = tmp->first->m_child[1];
						tmp1.second = 0;
						m_pila.push(tmp1);
					}
				}
				tmp = &m_pila.top();
			}
			tmp->second++;
			return *this;
		}
		
};


#endif // ITERATOR_POSTORDER_H