
template <typename T>
struct Comparator{
    virtual bool compare(T, T) = 0;
};

template <typename T>
struct Comp_Less: Comparator<T>{
    inline bool compare(T a, T b){
        return a < b;
    }
};

template <typename T>
struct Comp_Greater: Comparator<T>{
    inline bool compare(T a, T b){
        return a > b;
    }
};
