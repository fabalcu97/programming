#ifndef RBNODE_H
#define RBNODE_H

template <typename T>
struct CRBNode{

	int m_color;

	T m_data;

	CRBNode* m_right;
	CRBNode* m_left;
	CRBNode* m_parent;


	CRBNode(T data)
	{
		m_data = data;
		m_color = 1;
		m_right = m_left = m_parent = nullptr;
	}

};


#endif // RBNODE_H