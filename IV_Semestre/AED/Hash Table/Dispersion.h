#ifndef DISPERSION_H_
#define DISPERSION_H_

#include <math.h>

using namespace std;

template<typename T>
class CDispersion_Modulo{

	public:

		int operator()(T x, int total)
		{
			return mod(x) % total;
		}

		int mod(int x)
		{
			return x;
		}

		int mod(string x)
		{
			int i = 0,
				cnt = 0;
			while( x[i] != 0 )
			{
				cnt += x[i];
			}
			return cnt;
		}

};

template<typename T>
class CDispersion_CRC{

	unsigned reverse(unsigned x) {
      x = ((x & 0x55555555) <<  1) | ((x >>  1) & 0x55555555);
      x = ((x & 0x33333333) <<  2) | ((x >>  2) & 0x33333333);
      x = ((x & 0x0F0F0F0F) <<  4) | ((x >>  4) & 0x0F0F0F0F);
      x = (x << 24) | ((x & 0xFF00) << 8) | ((x >> 8) & 0xFF00) | (x >> 24);
	   return x;
	}

	public:

		int operator()(T x, int total)
		{
			return crc(x) % total;
		}

		unsigned int crc(string message) {
		   int i, j;
		   unsigned long long int byte, crc;

		   i = 0;
		   crc = 0xFFFFFFFF;
		   while (message[i] != 0) {
		      byte = message[i];
		      for (j = 0; j <= 7; j++)
		      {
		         if ((int)(crc ^ byte) < 0)
		              crc = (crc << 1) ^ 0x1EDC6F41 /*0x04C11DB7*/;
		         else crc = crc << 1;
		         byte = byte << 1;
		      }
		      i = i + 1;
		   }
		   return (crc);
		}


		unsigned int crc(int id) {

		   unsigned int   crc = 0xFFFFFFFF;

		   for (int j = 0; j <= 7; j++) {
		      if ((int)(crc ^ id) < 0){
		         crc = (crc << 1) ^ 0x1EDC6F41 /*0x04C11DB7*/;      //Estándar que maximiza la detección de errores y minimiza las colisiones
		                                             //IEEE 802.3 --> además es difícil encontrar uno perfecto
		      }                                      //x27 + x26 + x23 + x22 + x16 + x12 + x11 + x10 + x8 + x7 + x5 + x4 + x2 + x + 1
		      else{
		         crc = crc << 1;
		      }
		      id <<= 1;
		   }
		   return (crc);
		}
};

template <typename T>
class CDispersion_FNV{

	unsigned reverse(unsigned x) {
      x = ((x & 0x55555555) <<  1) | ((x >>  1) & 0x55555555);
      x = ((x & 0x33333333) <<  2) | ((x >>  2) & 0x33333333);
      x = ((x & 0x0F0F0F0F) <<  4) | ((x >>  4) & 0x0F0F0F0F);
      x = (x << 24) | ((x & 0xFF00) << 8) | ((x >> 8) & 0xFF00) | (x >> 24);
	   return x;
	}

	public:

		int operator()(T x, int total){
			return FNC(x) % total;
		}

		unsigned int FNC(string id)
		{
		   	unsigned long int hash = 2166136261;
		   	int i = 0;
		   	char tmp;

		   	while(id[i] != 0)
		   	{
		   		tmp = id[i];
		   		tmp = reverse(tmp);
			   	for(int i = 0; i < 7; i++)
			   	{
			    	hash = hash ^ 1099511628211;
			      	hash = hash * (int)tmp;
			   	}
			   	i++;
			}
		   	return (hash);
		}

		unsigned int FNC(int id)
		{
		   unsigned long int hash = 2166136261;

		   for(int i = 0; i < 7; i++)
		   {
		      hash = hash ^ 1099511628211;
		      hash = hash * id;
		   }
		   return (hash);
		}
};

#endif /* DISPERSION_H_ */
