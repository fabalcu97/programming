#ifndef HASH_TABLE_H_
#define HASH_TABLE_H_

#include<iostream>

using namespace std;

template<typename Tr>
class CHash_Table{

	typedef typename Tr::T T;
	typedef typename Tr::Dispersion Disp;
	typedef typename Tr::Adaptor E;

	public:
		E* m_ht;
		Disp m_disp;
		int m_size;

		CHash_Table(int size){
			m_size = size;
			m_ht = new E[m_size];
		}

		bool Insert(T x){
			int id = m_disp(x, m_size);
			return m_ht[id].insert(x);
		}

		bool Remove(T x){
			int id = m_disp(x) % m_size;
			/*cout<<"\n Id: "<<id<<endl;*/
			return m_ht[id].remove(x);
		}

		bool Find(T x){
			int id = m_disp(x) % m_size;
			return m_ht[id].find(x);
		}

		void print(){
			int cnt = 0;
			for (int i = 0; i < m_size; i++){
				if (m_ht[i].size() == 0)
				{
					cnt++;
				}
				m_ht[i].print();
				cout<<endl;
			}
		cout<<"Vacios: "<<cnt<<endl;
		}

		int vacios(){
			int cnt = 0;
			for (int i = 0; i < m_size; i++){
				if (m_ht[i].size() == 0)
				{
					cnt++;
				}
			}
		return cnt;
		}

};



#endif /* HASH_TABLE_H_ */
