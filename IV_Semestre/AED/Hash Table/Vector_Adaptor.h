#ifndef VECTOR_ADAPTOR_H_
#define VECTOR_ADAPTOR_H_

#include <iostream>
#include <vector>

using namespace std;

template <typename T>
struct CVectorAdaptor{

	vector<T> m_vector;
	int i = 0;

	bool find(T x){
		for(i = 0; i < m_vector.size(); i++){
			if (m_vector[i] == x){
				return 1;
			}
		}	
		return 0;
	}

	bool insert(T x){
		if ( find(x) ){
			return 0;
		}
		m_vector.push_back(x);
		return 1;
	}

	bool remove(T x){
		typename vector<T>::iterator it = m_vector.begin();
		if ( find(x) ){
			m_vector.erase(it + i);
			return 1;
		}
		return 0;
	}

	void print(){

		for (int j = 0; j < m_vector.size(); j++){
			cout<<m_vector[j]<<"-";
		}
	}

	int size(){
		return m_vector.size();
	}

};



#endif /* VECTOR_ADAPTOR_H_ */
