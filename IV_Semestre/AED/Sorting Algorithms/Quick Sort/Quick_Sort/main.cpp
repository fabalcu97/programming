#include <iostream>
#include <vector>
#include <stack>
#include "Quick.h"

using namespace std;

void QuickSort(vector<int>*);
int Pivot_Sort(vector<int>*, int, int);
void iterativeQsort(int[], int);
int partition(int[], int p, int start, int end);



int main(){

    srand(time(NULL));

    vector<int> array;
    vector<int> result;

    int n;
    double x;

    int elementos[6] = {10, 100, 1000, 10000, 100000, 10000000};

    int NT = 250*2;

    for(int i = 0; i < 6; i++){

        n = elementos[i];

        for (int i = 0; i < n; i++){
            array.push_back(rand()%n);
        }
        cout<<"--------------------\n";

        clock_t start = clock();

        QuickSort(&array);

        x = (clock() - start) / (double)(CLOCKS_PER_SEC);

        orden(&array);

        if(flag){
            cout<<"Ordenado";
        }

        cout<<endl<<"Tiempo: "<<x<<"\nHilos: "<<NT<<"\nCantidad Elementos: "<<n<<endl<<"--------------------\n";
        print(array);
        array.clear();
    }

}




void QuickSort(vector<int>* _array){

    vector<int> pila;
    pila.push_back(0);
    pila.push_back(_array->size()-1);

    while ( !pila.empty() ) {

        int fin = pila.back();
        pila.pop_back();                        ///Elimina de la pila
        int ini = pila.back();                  ///Copia a la pila
        pila.pop_back();
//        cout<<"De "<<ini<<" hasta "<<fin<<endl;

        int tmp = Pivot_Sort(_array, ini, fin);     ///Ordena el vector[ini;fin] respecto al pivote

        if(tmp-1 > ini){
            pila.push_back(ini);                    ///Agrega lado izquierdo
            pila.push_back(tmp-1);
        }
        if(tmp+1 < fin){
            pila.push_back(tmp+1);                    ///Agrega lado derecho
            pila.push_back(fin);
        }

//        print(_array);
//        getchar();
//        cout<<"-----------------"<<endl;

    }
}

int Pivot_Sort(vector<int>* _array, int _start, int _end){

    int x = _array->at(_end);
    int i = (_start - 1);
//    cout<<"i: "<<i<<"\nx: "<<x<<endl;

    for (int j = _start; j <= _end - 1; j++){

        if (_array->at(j) <= x){                            ///Solo considera menores pues al hacer swap los mayores
            i++;                                            ///Pasan a la derecha del pivote
//            cout<<"compara: "<<_array->at(j)<<"\nj: "<<j<<"\ni: "<<i<<endl;
            swap(*(_array->begin()+i), *(_array->begin()+j));
        }
//        print(_array);
    }

    swap(*(_array->begin()+i+1), *(_array->begin()+_end));
    return (i + 1);                                         ///Siguiente a ordenar
}





