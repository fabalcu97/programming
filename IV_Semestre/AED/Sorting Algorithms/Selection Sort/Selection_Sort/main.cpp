#include <iostream>
#include <algorithm>
#include <thread>
#include <mutex>
#include "Selection.h"

using namespace std;



int main(){

    srand(time(NULL));

    vector<int> array;
    vector<int> result;

    int n;
    double x;

    int elementos[6] = {10, 100, 1000, 10000, 100000, 10000000};

    int NT = 250*2;

    for(int i = 0; i < 6; i++){

        n = elementos[i];

        for (int i = 0; i < n; i++){
            array.push_back(rand() % n);
        }
        cout<<"--------------------\n";

        clock_t start = clock();

        Selection_Sort(&array);

        x = (clock() - start) / (double)(CLOCKS_PER_SEC);

        orden(&array);

        if(flag){
            cout<<"Ordenado";
        }

        cout<<endl<<"Tiempo: "<<x<<"\nHilos: "<<NT<<"\nCantidad Elementos: "<<n<<endl<<"--------------------\n";
        print(array);
        array.clear();
    }


}

