#ifndef MERGE_H
#define MERGE_H

#include <vector>
#include <thread>

using namespace std;

template <typename T>
void print(vector<T> a){

    int n = a.size();

    for (int i = 0; i < n; i++){
        cout << a[i] << "-";
    }
    cout<<endl;

}

template <typename T>
void print(vector<T>* _array){
    int n = _array->size();
    for (int i = 0; i < n; i++){
        cout<<*(_array->begin()+i)<<"-";
    }
    cout<<endl;
}

bool flag = false;

void verify(vector<int>* _array){
    int n = _array->size();
    for (int i = 1; i < n; i++){
        if(_array->at(i-1) > _array->at(i)){
            flag = false;
            return;
        }
    }
    flag = true;
}


void orden(vector<int>* _array){

    int n = _array->size();

    vector<int> _array1(_array->begin(), _array->begin() + (n/4));

    vector<int> _array2(_array->begin() + ((n/4)+1), _array->begin() + n/2);

    vector<int> _array3(_array->begin() + ((n/2)+1), _array->begin() + 3*(n/4));

    vector<int> _array4(_array->begin() + (3*(n/4)+1), _array->end());


    thread T1(verify, &_array1);
    thread T2(verify, &_array2);
    thread T3(verify, &_array3);
    thread T4(verify, &_array4);

    T1.join();
    T2.join();
    T3.join();
    T4.join();

}
#endif // MERGE_H
