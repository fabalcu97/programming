#ifndef BUBBLE_SORT_H
#define BUBBLE_SORT_H

#include <vector>
#include <mutex>

using namespace std;

mutex F;

template <typename T>
void print(vector<T> a){

    int n = a.size();
    for (int i = 0; i < n; i++){
        cout << a[i] << "-";
    }
    cout<<endl;  

}

template <typename T>
void print(vector<T>* a){
    F.lock();
    int n = a->size();
    for (int i = 0; i < n; i++){
        cout << a->at(i) << "-";
    }
    cout<<endl;
    F.unlock();
}

template <typename T>
vector<T> Bubble_Sort(vector<T> _array){

    int n = _array.size();
    bool swapped = true;

    while (swapped){
        swapped = false;
        for (int j = 1; j < n; j++){
            if (_array[j-1] > _array[j]){

                swap(_array[j-1], _array[j]);
                swapped = true;
            }
        }
    }
    return _array;
}




/*void Bubble_even(vector<int>* _array, bool* swapped){
    int n = _array->size();
    for(int i = 0; i < (n/2); i++){
        if ( *(_array->begin()+(2*i)) > *(_array->begin()+(2*i+1))){

            F.lock();
            swap(*(_array->begin()+(2*i)), *(_array->begin()+(2*i+1)));
            *swapped = true;
            F.unlock();
        }
    }
}

void Bubble_odd(vector<int>* _array, bool* swapped){
    int n = _array->size();
    for(int i = 0; i < (n/2); i++){
        if ( *(_array->begin()+(2*i+1)) > *(_array->begin()+(2*i+2))){

            F.lock();
            swap(*(_array->begin()+(2*i+1)), *(_array->begin()+(2*i+2)));
            *swapped = true;
            F.unlock();
        }
    }
}*/

int NT;
vector<thread*> threads;
bool swapped = true;

template <typename T>
void Bubble_PSort(vector<T>* _array){

    int n = _array->size();

    while (swapped){

        swapped = false;

        for (int j = 1; j < n; j++){

            F.lock();

            if (_array->at(j-1) > _array->at(j)){
                swap(_array->at(j-1), _array->at(j));
                swapped = true;
            }


            if (j == n/2  && threads.size() < NT){
                threads.push_back(new thread(Bubble_PSort<T>, _array));
            }

            F.unlock();
        }

    }

}


template <typename T>
void Bubble_PSort1(vector<T>* _array, int i){

    int n = _array->size();
    T tmp;

    for (int j = 0; j < n-i-1; j++){
        F.lock();
        if (_array->at(j) > _array->at(j+1)){
            swap(_array->at(j+1), _array->at(j));
        }
        if (j == 2){
            threads.push_back(new thread(Bubble_PSort1<T>, _array, i+1));
        }
        F.unlock();
    }

}


bool flag = false;

void verify(vector<int>* _array){

    int n = _array->size();

    for (int i = 1; i < n; i++){

        if(_array->at(i-1) > _array->at(i)){
            flag = false;
            return;
        }
    }
    flag = true;
}


void orden(vector<int>* _array){

    int n = _array->size();

    vector<int> _array1(_array->begin(), _array->begin() + (n/4));

    vector<int> _array2(_array->begin() + ((n/4)), _array->begin() + n/2);

    vector<int> _array3(_array->begin() + ((n/2)), _array->begin() + 3*(n/4));

    vector<int> _array4(_array->begin() + (3*(n/4)), _array->end());


    thread T1(verify, &_array1);
    thread T2(verify, &_array2);
    thread T3(verify, &_array3);
    thread T4(verify, &_array4);

    T1.join();
    T2.join();
    T3.join();
    T4.join();

}


#endif // BUBBLE_SORT_H













