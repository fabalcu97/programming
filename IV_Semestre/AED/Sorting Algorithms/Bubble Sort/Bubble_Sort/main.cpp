#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <thread>
#include "Bubble_sort.h"

using namespace std;

int main(){

    srand(time(NULL));

    vector<int> array;

    int n;
    double x;

    int elementos[6] = {10, 100, 1000, 10000, 100000, 10000000};

    NT = 10;

    for(int i = 0; i < 6; i++){

        n = elementos[i];

        for (int i = 0; i < n; i++){
            array.push_back(rand()%n);
        }
        cout<<"--------------------\n";

        clock_t start = clock();

        Bubble_PSort(&array);

        x = (clock() - start) / (double)(CLOCKS_PER_SEC);

        orden(&array);

        if(flag){
            cout<<"Ordenado";
        }

        print(array);

        cout<<endl<<"Tiempo: "<<x<<"\nHilos: "<<NT<<"\nCantidad Elementos: "<<n<<endl<<"--------------------\n";

        array.clear();
        swapped = true;
//        getchar();
    }
}
