#ifndef HEAP_H
#define HEAP_H

#include<vector>
#include<thread>

using namespace std;

template <typename T>
int techo(T a){
    return a+1;
}

template <typename T>
int maximo(T a, T b){
    if (a > b){
        return a;
    }
    return b;
}

template <typename T>
int piso(T a){
    return a;
}

template <typename T>
void print(vector<T> a){

    int n = a.size();

    for (int i = 0; i < n; i++){
        cout << a[i] << "-";
    }
    cout<<endl;

}

template <typename T>
void print(vector<T>* _array){
    int n = _array->size();
    for (int i = 0; i < n; i++){
        cout<<*(_array->begin()+i)<<"-";
    }
    cout<<endl;
}




/**
*
* padre = techo(pos/2)-1
* hijo izquierdo = (n*2)+1
* hijo derecho   = (n*2)+2
*
**/


void Verify(vector<int>* _array){

    int     n = _array->size(),
            tmp1, tmp2, tmp3, iz, der;

    for(int i = 0 ; i < n;){               ///recorre de atras hacia adelante (hijo -> padre)

        iz = ((i*2)+1);
        der = ((i*2)+2);

        if (der >= n){
            break;
        }

        tmp1 = _array->at(iz);
        tmp2 = _array->at(der);

        tmp3 = maximo(_array->at(iz), _array->at(der));

        if((_array->at(i) < _array->at(iz)) && (tmp1 == tmp3)){       ///Si el padre es menor hace un cambio

            swap(_array->at(i), _array->at(iz));
            i = iz;                                                    ///Salta alg sgte. hijo, el padre que ahora es hijo
        }
        else if((_array->at(i) < _array->at(der)) && (tmp2 == tmp3)){       ///Si el hijo es mayor hace un cambio

            swap(_array->at(i), _array->at(der));       ///repite mientras haya cambios pues no sería heap
            i = der;
        }
        else{
            break;
        }


//       print(_array);
    }
}



void BinaryHeap(vector<int>* _array){

    int     n = _array->size(),
            tmp;

    bool swapped = true;

    while(swapped){

        swapped = false;

        for(int i = (n-1) ; i >= 0; i--){                   ///recorre de atras hacia adelante (hijo -> padre)
            tmp = (i/2)-1;
            if(i&1){
                tmp = techo(i/2)-1;
            }
            if (tmp == -1){                                 ///Termina si recorrió todos los elementos
                break;
            }
            else if(_array->at(i) > _array->at(tmp)){       ///Si el hijo es mayor hace un cambio
                swap(_array->at(i), _array->at(tmp));
                swapped = true;                             ///repite mientras haya cambios pues no sería heap

            }
        }
    }

}




void Heap_Sort(vector<int>* _array, vector<int>* _result){

//    clock_t start = clock();

    BinaryHeap(_array);

//    cout<<((double)clock() - start) / CLOCKS_PER_SEC;

    while (!_array->empty()){

//        clock_t start1 = clock();

        _result->insert(_result->begin(), _array->at(0));           ///Inserta el mayor

        swap(_array->at(0), _array->at(_array->size()-1));          ///Hace cambio entre el mayor y el ultimo

        _array->erase( _array->begin() + _array->size()-1 );        ///elimina el elemento que ya insertó(mayor)

//        cout<<((double)clock() - start1) / CLOCKS_PER_SEC<<endl;

        Verify(_array);                                         ///Reordena el Heap

//        it++;
//        if (it%1000 == 0)
//        cout<<it<<endl;

    }


}


bool flag = false;

void verify(vector<int>* _array){
    int n = _array->size();
    for (int i = 1; i < n; i++){
        if(_array->at(i-1) > _array->at(i)){
            flag = false;
            return;
        }
    }
    flag = true;
}


void orden(vector<int>* _array){

    int n = _array->size();

    vector<int> _array1(_array->begin(), _array->begin() + (n/4));

    vector<int> _array2(_array->begin() + ((n/4)+1), _array->begin() + n/2);

    vector<int> _array3(_array->begin() + ((n/2)+1), _array->begin() + 3*(n/4));

    vector<int> _array4(_array->begin() + (3*(n/4)+1), _array->end());


    thread T1(verify, &_array1);
    thread T2(verify, &_array2);
    thread T3(verify, &_array3);
    thread T4(verify, &_array4);

    T1.join();
    T2.join();
    T3.join();
    T4.join();

}



#endif // HEAP_H
