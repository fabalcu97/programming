#ifndef INSERTION_H
#define INSERTION_H

#include <vector>

using namespace std;

template <typename T>
void print(vector<T> _array){
    int n = _array.size();
    for (int i = 0; i < n; i++){
        cout<<_array[i]<<"-";
    }
    cout<<endl;
}


template <typename T>
void Insertion_Sort(vector<T>* _array){

    int n = _array->size(),
        j = 0,
        x = 0;

    for (int i = 1; i < n; i++){
        x = _array->at(i);
        j = i;
        while(j > 0 &&  ( _array->at(j-1) > x ) ){      ///Va de atras hacia adelante duplicando
            _array->at(j) = _array->at(j-1);            ///Cuando no hay cambios inserta el valor en la posición
            j--;
        }
        _array->at(j) = x;
    }
}


bool flag = false;

void verify(vector<int>* _array){
    int n = _array->size();
    for (int i = 1; i < n; i++){
        if(_array->at(i-1) > _array->at(i)){
            flag = false;
            return;
        }
    }
    flag = true;
}


void orden(vector<int>* _array){

    int n = _array->size();

    vector<int> _array1(_array->begin(), _array->begin() + (n/4));

    vector<int> _array2(_array->begin() + ((n/4)+1), _array->begin() + n/2);

    vector<int> _array3(_array->begin() + ((n/2)+1), _array->begin() + 3*(n/4));

    vector<int> _array4(_array->begin() + (3*(n/4)+1), _array->end());


    thread T1(verify, &_array1);
    thread T2(verify, &_array2);
    thread T3(verify, &_array3);
    thread T4(verify, &_array4);

    T1.join();
    T2.join();
    T3.join();
    T4.join();

}

template <typename T>
void Insertion_PSort(vector<T>* _array){

    int n = _array->size(),
        j = 0,
        x = 0;


    for (int i = 1; i < n; i++){
        x = _array->at(i);
        j = i;
        while(j > 0 &&  ( _array->at(j-1) > x ) ){
            _array->at(j) = _array->at(j-1);
            j--;
        }
        _array->at(j) = x;
    }
}




#endif // INSERTION_H
