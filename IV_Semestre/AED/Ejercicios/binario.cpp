/*Copyright 2015 <Copyright Lab. G>*/

#include <iostream>
#include <vector>
#include <algorithm>

std::vector<int> get_bin(int x);
int unos(std::vector<int> x);
int abs(int x);

int main() {
    char ch;
    int numero = 0;
    int num_tmp = 0;
    std::cin >> numero;
    num_tmp = abs(numero);

    std::vector<int> tmp = get_bin(numero);
    std::vector<int> tmp1;

    for (int i = num_tmp-1; i > 0; i--) {
        tmp1 = get_bin(i);
        if (unos(tmp) == unos(tmp1)) {
            if (numero < 0) {
                    std::cout << std::endl << i*(-1) << std::endl << std::endl;
            } else {
                    std::cout << std::endl << i << std::endl << std::endl;
            }
            break;
        }
    }

    std::cout << "Press enter to continue..." << std::endl;
    std::cin.ignore();
    std::cin.get();

    return 0;
}

std::vector<int> get_bin(int x) {
    int tmp = abs(x);
    std::cout << "Número: " << tmp << ": ";
    std::vector<int> res;

    while ( tmp > 0 ) {
        res.push_back(tmp % 2);
        tmp = tmp / 2;
    }
    for (std::vector<int>::iterator i = res.begin(); i != res.end(); ++i) {
        std::cout << *i << "-";
    }
    std::cout << std::endl;

    return res;
}

int unos(std::vector<int> x) {
    int tam = x.size();
    int unos = 0;

    for (int i = 0; i < tam; i++) {
        if (x[i] == 1) {
                unos++;
        }
    }
    return unos;
}

int abs(int x) {
        return (x) > (0) ? x : x*(-1);
}
