#include <iostream>
#include <stdlib.h>
using namespace std;

int abs(int);
int min(int, int);

int main()
{

    int cols,
        rows,
        n,
        ones = 0,
        ans,
        UC,
        UR,
        ans1 = 0,
        a = 0,
        b = 0,
        z = 0;

    cout<<"Columnas: ";
    cin>> cols;
    cout<<"Filas: ";
    cin>> rows;

    int M [rows+1][cols+1];

    for(int j=0;j<=cols;j++)
    {
        M[0][j]=0;
    }

    cout<<"Ingrese la matriz: "<<endl;

    for(int i=1;i<=rows;i++)
    {
        M[i][0]=0;
        for(int j=1;j<=cols;j++)
        {
            cin>>n;
            M[i][j]=n;
            if(n==1)
            {
                M[i][0]+=1;                     //cantidad de unos por fila
                M[0][j]+=1;                     // Llenado de cantidad de unos por columna
                ones+=1;                        // cantidad total de unos
            }
        }
    }


    ans=min(cols*rows-ones,ones);        // Qué es más fácil? cambiar todo a ceros o a unos

    if( cols%rows == 0 || rows%cols == 0 )
    {
        if( rows%cols == 0 )               //cant de unos que debe tener cada fila y cada columna
        {
            UC = rows/cols;
            UR = 1;
            if(ones%rows<=rows-ones%rows)
            {
                UR=ones/rows;
                UC=UR*UC;
            }
            else
            {
                UR=ones/rows +1;
                UC=UR*UC;
            }
        }
        if(cols%rows==0)
        {
            UR=cols/rows;
            UC=1;
            if(ones%cols<=cols-ones%cols)       //Rango de quitar o agregar unos al mayor multiplo o menor multiplo cercano al total de unos
            {
                UC=ones/cols;
                UR=UR*UC;
            }
            else
            {
                UC=ones/cols +1;
                UR=UR*UC;
            }
        }

        for(int i=1;i<=rows;i++)
        {
            for(int j=1;j<=cols;j++)
            {
                z = M[i][j];
                a = M[i][0];            // cantidad de rows que hay en la fila
                b = M[0][j];            // cantidad de rows que hay en la columna
                a = UR-a;               // numero de cambios (positivo = agregar unos - negativo= quitar unos - cero = desplazar )
                b = UC-b;               

                if( z == 0 ) // 0->1 Cambiar cero por uno
                {
                    if( a > 0 && b > 0 )
                    {
                        ans1++;        //agregamos un cambio
                        M[i][0]++;      // agregamos un uno en la fila
                        M[0][j]++;      //agregamos un uno en la columna
                    }
                }
                else        // 1->0    Cambiar cero por uno
                {
                    if(a<0 && b<0)
                    {
                        ans1++;        //agregamos un cambio
                        M[i][0]--;       // agregamos un uno en la fila
                        M[0][j]--;       //agregamos un uno en la columna
                    }
                }
            }
        }

        for(int i=1;i<=rows;i++)            // recorremos todos los acumulados en caso falten cambios cuando el numero de unos
        {                                   // es igual al número que corresponde, es decir solo deben desplazarse
            ans1+=abs(UR-M[i][0]);      //filas
        }
        for(int j=1;j<=cols;j++)
        {
            ans1+=abs(UC-M[0][j]);      //columnas
        }
        ans=min(ans,ans1);           // minimo de cambios de todo a cero o todo a uno y cambios normales
    }

    cout<< "Se necesitan "<<ans<<" cambios"<<endl;

    return 0;
}

int abs(int x)
{
    if(x<0)
    {
        return (-1)*x;
    }
    return x;
}

int min(int x, int y)
{
    if(x<y)
    {
        return x;
    }
    return y;
}
