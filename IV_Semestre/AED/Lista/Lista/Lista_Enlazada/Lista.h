#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include "node.h"

using namespace std;



template <typename T>
class CIterator{

    public:
        Node<T>* m_p;
        int id;

        CIterator(Node<T>* p = 0){
            m_p = p;
        }
        void operator =(CIterator<T> I){
            m_p = I.m_p;
        }

};

template<typename T>

class Lista{
    public:

        Node<T>* Head;
        CIterator<T> m_iterator;

        Lista(){

            Head = nullptr;

        }

        bool Find(T x, Node<T>**& p){

            for(p = &Head; (*p) && x > (*p)-> valor; p = &(*p)->Next);
            return *p && x==(*p)->valor;

        }

        void Print(){

            Node<T>* p;
            for(p = Head; p; p = p->Next){
                cout<<p->valor<<"-";
            }

        }

        bool Insert(T x){

            Node<T>** p;
            if(Find(x, p)){
                return 0;
            }
            Node<T>* tmp = new Node<T>(x);
            tmp->Next = *p;
            *p = tmp;
            return 1;
        }


        bool Remove(T x){

            Node<T>** p;
            if(!Find(x, p)){
                return 0;
            }
            Node<T>* tmp = *p;
            *p = (*p)->Next;
            delete tmp;
            return 1;
        }

        CIterator<T> Begin(){
            CIterator<T> m_i(Head);
            return m_i;
        }
        CIterator<T> End(){
            CIterator<T> m_i;
            return m_i;

        }

};




#endif // LISTA_H
