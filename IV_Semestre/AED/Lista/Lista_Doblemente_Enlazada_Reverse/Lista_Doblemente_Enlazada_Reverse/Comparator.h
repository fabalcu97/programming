#ifndef COMPARATOR_H
#define COMPARATOR_H

template <typename T>
struct Comparator{
    virtual bool cmp(T, T) = 0;
};

template <typename T>
struct Comp_Less: Comparator<T>{
    inline bool cmp(T a, T b){
        return a < b;
    }
};

template <typename T>
struct Comp_Greater: Comparator<T>{
    inline bool cmp(T a, T b){
        return a > b;
    }
};

#endif // COMPARATOR_H
