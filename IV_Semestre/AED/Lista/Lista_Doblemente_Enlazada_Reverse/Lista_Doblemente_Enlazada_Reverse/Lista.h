#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;

template<typename Tr>
class Lista{
    public:

        typedef typename Tr::T T;
        typedef typename Tr::Ht H;
        typedef typename Tr::I It;

        Node<T>* Head;

        It m_Iterator;
        H Compare;

        Lista(){
            Head = nullptr;
        }

        bool Find(T x, Node<T>**& p){
            for(p = &Head; (*p) && Compare.cmp( x, (*p)-> valor ); p = &(*p)->Next);
            return *p && x==(*p)->valor;
        }

        void Print(){

            for(m_Iterator = Begin(); m_Iterator != End(); m_Iterator++){
                cout<< *m_Iterator <<"-";
            }
        }

        bool Insert(T x){

            Node<T>** p;
            if(Find(x, p)){
                return 0;
            }
            Node<T>* tmp = new Node<T>(x);
            tmp->Next = *p;
            *p = tmp;
            return 1;
        }


        bool Remove(T x){
            Node<T>** p;
            if(!Find(x, p)){
                return 0;
            }
            Node<T>* tmp = *p;
            *p = (*p)->Next;
            delete tmp;
            return 1;
        }

        C_Iterator<T> Begin(){
            C_Iterator<T> m_i(Head);
            return m_i;
        }

        C_Iterator<T> End(){
            C_Iterator<T> m_i;
            return m_i;

        }

};

#endif // LISTA_H
