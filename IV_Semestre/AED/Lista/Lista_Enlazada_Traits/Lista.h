#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include "node.h"

using namespace std;

template<typename Tr>

class Lista{
    public:

        typedef typename Tr::T T;
        typedef typename Tr::H H;

        Node<T>* Head;

        H Comp;

        Lista(){

            Head = nullptr;

        }

        bool Find(T x, Node<T>**& p){
            for(p = &Head; (*p) && Comp.cmp(x, (*p)-> valor); p = &(*p)->Next);
            return *p && x==(*p)->valor;
        }

        void Print(){

            Node<T>* p;
            for(p = Head; p; p = p->Next){
                cout<<p->valor<<"-";
            }

        }

        bool Insert(T x){

            Node<T>** p;
            if(Find(x, p)){
                return 0;
            }
            Node<T>* tmp = new Node<T>(x);
            tmp->Next = *p;
            *p = tmp;
            return 1;
        }


        bool Remove(T x){

            Node<T>** p;
            if(!Find(x, p)){
                return 0;
            }
            Node<T>* tmp = *p;
            *p = (*p)->Next;
            delete tmp;
            return 1;
        }

};




#endif // LISTA_H
