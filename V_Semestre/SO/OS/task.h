
typedef void(*pFunction)(void *);

struct Task{
	Task(pFunction, int, char*);
	void def_next(Task*);

	char* t_name;
	int t_priority;
	int t_state;
	pFunction t_function;
	Task* t_next;
	//Task* prev;
};
