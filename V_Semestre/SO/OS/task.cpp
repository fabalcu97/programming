
#include "task.h"

#define BLK 0
#define RNN 1
#define	WTN 2

Task::Task(pFunction foo, int priority, char* name)
{
	t_name = name;
	t_priority = priority;
	t_state = RNN;
}

void Task::def_next(Task* next)
{
	t_next = next;
}
