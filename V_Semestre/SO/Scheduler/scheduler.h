#include <iostream>
#include <vector>

#include <unistd.h>
#include "variables.h"
#include "memoria.cpp"
#include "cpu.h"

#define FLAG 1

using namespace std;

class scheduler{

	private:

		vector<bool*> m_CPU;
		vector<memoria> m_Memorias;
		vector<int*> m_fproc;

	public:
		scheduler();
		void exec();


};
