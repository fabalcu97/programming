#include <iostream>
#include <vector>
#include "variables.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

using namespace std;

class cpu{

	private:
		bool* m_flag;		//Procesador ocupado/libre
		key_t m_cpuMEM;

	public:
		cpu(key_t, key_t);
		bool get_state();
		void sum(int, int);
		void rest(int, int);
		void fib(int);
		void sle(int);
		void operacion();

		/*vector< vector<int> > multMatrix(vector< vector<int> >, vector< vector<int> >);
		vector< vector<int> > TransMatrix(vector< vector<int> >, vector< vector<int> >);
		void printMatrix(vector<vector<int> >);

		vector<int> OrdElementos(vector<int>);

		vector< vector<int> > Invertir(vector< vector<int> >);*/

};
