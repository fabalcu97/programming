#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXSIZE     27

int main()
{
    int shmid, flag;
    key_t key1, key2;
    int *shm;
    bool *flg;

    key1 = 5678;
    key2 = 5679;

    shmid = shmget(key1, MAXSIZE, 0666);
    flag = shmget(key2, 1, 0666);

    shm = shmat(shmid, NULL, 0);
    flg = shmat(flag, NULL, 0);

    while(1){
        if( !(*flg) ){
            printf("Leo: %d\n", *shm);
            *flg = 1;
            sleep(2);
        }
    }

    exit(0);
}
