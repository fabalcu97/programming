#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include  <sys/types.h>

void parse(char*, char**);

bool *flag;
bool *turno;

int main()
{

    int serv, cli, procs;
    int idf, idt, ids, idfs;

    key_t keyf, keyt, keys, keyfs;

    keyt = 1235;
    keyf = 1236;
    keys = 1237;
    keyfs = 1238;

    printf("Introduzca el número de procesos: ");
    scanf("%d", &procs);

    serv = (procs / 2) + 1;
    cli = procs - serv;

    idt = shmget(keyt, procs*sizeof(bool), IPC_CREAT | 0666);
    idf = shmget(keyf, procs*sizeof(bool), IPC_CREAT | 0666);
    ids = shmget(keys, serv*sizeof(int), IPC_CREAT | 0666);
    idfs = shmget(keyfs, serv*sizeof(bool), IPC_CREAT | 0666);

    for(int i = 0; i < procs; ++i){
        char *args[] = {"./Servidor", serv, procs, i, (char *) 0 };
        execv("./Servidor", args);
    }
    return 0;
}


void  parse(char *line, char **argv)
{
     while (*line != '\0') {       /* if not the end of line ....... */
        while (*line == ' ' || *line == '\t' || *line == '\n'){
             *line++ = '\0';     /* replace white spaces with 0    */
        }
        *argv++ = line;          /* save the argument position     */
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n'){
            line++;             /* skip the argument until ...    */
        }
     }
     *argv = '\0';                 /* mark the end of argument list  */
}



