
int base = 70;
int alto = 200;

float rotacion = 0;

// Spring simulation constants
float M = 0.8;   // Mass
float K = 0.2;   // Spring constant
float D = 0.92;  // Damping
float R = 150;   // Rest position

// Spring simulation variables
float ps = R;    // Position
float vs = 0.0;  // Velocity
float as = 0;    // Acceleration
float f = 0;     // Force

void setup() {

  size(600, 500); 

  rectMode(CORNERS);
}
  

void draw() {
  
  background(123, 213, 93);

  fill(255);
  translate(width/2, height/2);
  rotate(radians(rotacion));
  rect( 0, 0, base, alto );
  actualizacion();
  //delay(100);
  
  if (keyPressed) {
    if ( key == CODED ) {
      if ( keyCode == UP && rotacion > 0) {
        rotacion -= 1;
      }
      if ( keyCode == DOWN && rotacion < 90) {
        rotacion += 1;
      }
      if ( keyCode == LEFT && alto > 50) {
        alto -= 1;
      }
      if ( keyCode == RIGHT && alto < 200) {
        alto += 1;
      }
    }
    println("Rotacion: "+rotacion+"°.");
    println("Altura: "+alto+".");
    println("Base: "+base+".");
  }
}

void actualizacion() {
}