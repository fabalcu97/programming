 //<>//
float x_axis = 0;
float y_axis = 0;
float x_0 = 0;
float y_0 = 0;

float speed = 100;   // m/s
float ang = radians(90);  // 85°
float G = 9.8;
float time = 0;
float speed_y = 0;
float speed_x = 0;

volatile boolean flag = true;
volatile boolean lanz_flag = false;


float Tv = t_vuelo(speed * sin(ang));
float max_x = x(speed*cos(ang), Tv);
float max_y = max_Y(speed*sin(ang));

void settings() {

  //size(700, 600);
  //size(parseInt(max_x), parseInt(max_y));
  size(displayWidth, displayHeight);
}

void draw() {
  background(0, 0, 0);

  line(20, 300, displayWidth-50, 300);
  stroke(255);
  line(20, 20, 20, 300);
  stroke(255);

  ellipse(x_axis + 20, 300 - y_axis, 10, 10);
  fill(255, 0, 0);

  if (keyPressed) {
    if (key == ' ') {
      lanzamiento();
    }
  }
}

void lanzamiento() {

  if ( x_axis >= max_x || y_axis >= max_y) {
    return;
  }

  x_axis = x((speed*(cos(ang))), time);
  y_axis = y1(ang, x_axis, speed);

  time += 0.1;
  speed_y = y_speed(speed*sin(ang), time);
}

float x(float vel, float time) {
  return x_0 + vel * time;
}

float y(float vel, float time) {
  return y_0 + vel * time - ( G * sq(time) ) / 2;
}

float y1(float ang, float pos_x, float speed) {
  return (tan(ang)) * pos_x - ( G * sq(pos_x) ) / ( 2 * sq(speed * (cos(ang)) ) );
}

float y_speed(float vel, float time) {
  return vel - G*time;
}

float t_vuelo(float vel) {
  return (2 * vel)/G;
}

float max_Y(float vel) {
  return (sq(vel))/(2*G);
}

int conversion() {
  return 0;
}