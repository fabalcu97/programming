public color yellow = color(255, 255, 0);
public color black = color(0, 0, 0);
public color red = color(255, 0, 0);
public color green = color(0, 255, 0);
public color blue = color(0, 0, 255);
public color white = color(255, 255, 255);

float e = 0;
float scl = 1;
float rd1 = 200;
float rd2 = 20;

float x = 100;
float y = 100;

circle circ1 = new circle(red);
circle circ2 = new circle(blue);

dot dot1 = new dot(red, 10, 600, 700);
dot dot2 = new dot(blue, 10, 600, 700);
