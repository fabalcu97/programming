
class circle{

    private float mFakeDiameter = 20;
    private float mRealDiameter = 20;
    private float mx = width/2;
    private float my = height/2;
    private color mColor = white;

    public circle(color Color){
        mColor = Color;
    }

    public circle(color Color, float Diameter){
        mColor = Color;
        mFakeDiameter = Diameter;
        mRealDiameter = Diameter;
    }

    public void draw(){
        ellipse(mx, my, mFakeDiameter, mFakeDiameter);
    }

    public void draw(float x, float y){
        mx = x;
        my = y;
        ellipse(mx, my, mFakeDiameter, mFakeDiameter);
    }

    public float distance(circle Circle1){
        float x2 = Circle1.get_x();
        float y2 = Circle1.get_y();
        stroke(black);
        line(mx, my, x2, y2);
        return sqrt( sq(x2 - mx) + sq(y2 - my) );
    }

    public float get_x(){
        return mx;
    }

    public float get_y(){
        return my;
    }

    public float get_RealDiameter(){
        return mRealDiameter;
    }

    public float get_FakeDiameter(){
        return mFakeDiameter;
    }

    public void set_x(float x){
        mx = x;
    }

    public void set_y(float y){
        my = y;
    }

    public void set_RealDiameter(float Diameter){
        mRealDiameter = Diameter;
    }

    public void set_FakeDiameter(float Diameter){
        mFakeDiameter = Diameter;
    }

    public void scale(){
        if ( mFakeDiameter >= 500 ){
            mFakeDiameter = 100;
        }
        if ( mRealDiameter > 10 && mFakeDiameter <= 0 ){
            mFakeDiameter = 100;
        }
    }
}
