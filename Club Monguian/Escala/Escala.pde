
void setup(){

	size(700, 600);
	println("W: ", width );
	println("H: ", height );

}

void draw(){

	background(white);

	dot1.scaled_draw(x, y, 2);
	dot2.draw(20, 20);
	println("-------");
	println("x: ", x);
	println("y: ", y);
	println("D: ", dot1.distance(dot2) );
	println("FX: ", dot1.get_FakeX() );
	println("FY: ", dot1.get_FakeY() );
	println("W: ", width );
	println("H: ", height );

	if ( keyPressed ) {
		if ( key == 'w' && y > 0 ){
			y -= 10;
			dot1.set_FakeY( dot1.get_FakeY() - 10 );
		}
		if ( key == 's' ){
			y += 10;
			dot1.set_FakeY( dot1.get_FakeY() + 10 );
		}
		if ( key == 'd' ){
			x += 10;
			dot1.set_FakeX( dot1.get_FakeX() + 10 );
		}
		if ( key == 'a' && x > 0){
			x -= 10;
			dot1.set_FakeX( dot1.get_FakeX() - 10 );
		}
	}

    /*circ1.scale();
    circ2.scale();
    circ1.draw(100, 100);
    circ2.draw(300, 300);

    e = 0;

    println("Distance: " + circ1.distance(circ2) );
*/
}

/*void mouseWheel(MouseEvent event) {

    e = event.getCount();

    if ( e < 0 ){
        circ1.set_RealDiameter(circ1.get_RealDiameter() + 5);
        circ1.set_FakeDiameter(circ1.get_FakeDiameter() + 5);

        circ2.set_RealDiameter(circ2.get_RealDiameter() + 5);
        circ2.set_FakeDiameter(circ2.get_FakeDiameter() + 5);
    }
    if ( e > 0 && circ1.get_RealDiameter() > 0 ){
        circ1.set_RealDiameter(circ1.get_RealDiameter() - 5);
        circ1.set_FakeDiameter(circ1.get_FakeDiameter() - 5);

        circ2.set_RealDiameter(circ2.get_RealDiameter() - 5);
        circ2.set_FakeDiameter(circ2.get_FakeDiameter() - 5);
    }

    e = 0;

}
*/
