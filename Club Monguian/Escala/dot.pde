
class dot{

	private float mInitX = 0;
	private float mInitY = 0;
	private float mHeight = 0;
	private float mWidth = 0;

	private float mX = 0;
	private float mY = 0;

	private float mFakeX = 0;
	private float mFakeY = 0;

	private float mDiameter = 0;
	private float mDst = 0;
	private color mColor = white;

	//Constructor to set default values
	public dot(color Color, float Diameter, float Fheight, float Fwidth){
		mColor = Color;
		mDiameter = Diameter;
		mHeight = Fheight;
		mWidth = Fwidth;
	}

	//Constructor with a defined canvas
	public dot(color Color, float InitX, float InitY, float Fheight, float Fwidth, float Diameter){
		mColor = Color;
		mInitY = InitY;
		mInitX = InitX;
		mHeight = Fheight;
		mWidth = Fwidth;
		mDiameter = Diameter;
	}

	public void draw(){
		fill(mColor);
		ellipse(mX, mY, mDiameter, mDiameter);
	}

	public void scaled_draw(float x, float y, float scale){
		mX = x;
		mY = y;
		scale(scale);
		fill(mColor);
		ellipse(mFakeX, mFakeY, mDiameter, mDiameter);
	}

	public void draw(float x, float y){
		mX = x;
		mY = y;
		mFakeX = x;
		mFakeY = y;
		fill(mColor);
		ellipse(mX, mY, mDiameter, mDiameter);
	}

	public float distance(dot Dot){
		stroke(black);
		float x2 = Dot.get_FakeX();
		float y2 = Dot.get_FakeY();
		mDst = sqrt( sq(x2 - mFakeX) + sq(y2 - mFakeY) );
		line(mFakeX, mFakeY, x2, y2);

		float x1 = Dot.get_X();
		float y1 = Dot.get_Y();
		return sqrt( sq(x1 - mX) + sq(y1 - mY) );
	}

	private void scale(float scale){

		if ( mFakeY > mHeight ){
			mFakeY = mFakeY / scale;
		}
		if ( mFakeX > mWidth ){
			mFakeX = mFakeX / scale;
		}
		if ( mDst < scale * 10 ){
			mFakeY = mFakeY * scale;
			mFakeX = mFakeX * scale;
		}
	}

	//get methods
	public float get_X(){
		return mX;
	}

	public float get_Y(){
		return mY;
	}

	public float get_FakeX(){
		return mFakeX;
	}

	public float get_FakeY(){
		return mFakeY;
	}

	//Set methods
	public void set_FakeX(float x){
		if ( x >= 0 ){
			mFakeX = x;
		}

	}

	public void set_FakeY(float y){
		if ( y >= 0 ){
			mFakeY = y;
		}
	}

};
