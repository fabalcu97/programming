import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Escala extends PApplet {


public void setup(){

	
	println("W: ", width );
	println("H: ", height );

}

public void draw(){

	background(white);

	dot1.scaled_draw(x, y, 2);
	dot2.draw(20, 20);
	println("-------");
	println("x: ", x);
	println("y: ", y);
	println("D: ", dot1.distance(dot2) );
	println("FX: ", dot1.get_FakeX() );
	println("FY: ", dot1.get_FakeY() );
	println("W: ", width );
	println("H: ", height );

	if ( keyPressed ) {
		if ( key == 'w' && y > 0 ){
			y -= 10;
			dot1.set_FakeY( dot1.get_FakeY() - 10 );
		}
		if ( key == 's' ){
			y += 10;
			dot1.set_FakeY( dot1.get_FakeY() + 10 );
		}
		if ( key == 'd' ){
			x += 10;
			dot1.set_FakeX( dot1.get_FakeX() + 10 );
		}
		if ( key == 'a' && x > 0){
			x -= 10;
			dot1.set_FakeX( dot1.get_FakeX() - 10 );
		}
	}

    /*circ1.scale();
    circ2.scale();
    circ1.draw(100, 100);
    circ2.draw(300, 300);

    e = 0;

    println("Distance: " + circ1.distance(circ2) );
*/
}

/*void mouseWheel(MouseEvent event) {

    e = event.getCount();

    if ( e < 0 ){
        circ1.set_RealDiameter(circ1.get_RealDiameter() + 5);
        circ1.set_FakeDiameter(circ1.get_FakeDiameter() + 5);

        circ2.set_RealDiameter(circ2.get_RealDiameter() + 5);
        circ2.set_FakeDiameter(circ2.get_FakeDiameter() + 5);
    }
    if ( e > 0 && circ1.get_RealDiameter() > 0 ){
        circ1.set_RealDiameter(circ1.get_RealDiameter() - 5);
        circ1.set_FakeDiameter(circ1.get_FakeDiameter() - 5);

        circ2.set_RealDiameter(circ2.get_RealDiameter() - 5);
        circ2.set_FakeDiameter(circ2.get_FakeDiameter() - 5);
    }

    e = 0;

}
*/

class circle{

    private float mFakeDiameter = 20;
    private float mRealDiameter = 20;
    private float mx = width/2;
    private float my = height/2;
    private int mColor = white;

    public circle(int Color){
        mColor = Color;
    }

    public circle(int Color, float Diameter){
        mColor = Color;
        mFakeDiameter = Diameter;
        mRealDiameter = Diameter;
    }

    public void draw(){
        ellipse(mx, my, mFakeDiameter, mFakeDiameter);
    }

    public void draw(float x, float y){
        mx = x;
        my = y;
        ellipse(mx, my, mFakeDiameter, mFakeDiameter);
    }

    public float distance(circle Circle1){
        float x2 = Circle1.get_x();
        float y2 = Circle1.get_y();
        stroke(black);
        line(mx, my, x2, y2);
        return sqrt( sq(x2 - mx) + sq(y2 - my) );
    }

    public float get_x(){
        return mx;
    }

    public float get_y(){
        return my;
    }

    public float get_RealDiameter(){
        return mRealDiameter;
    }

    public float get_FakeDiameter(){
        return mFakeDiameter;
    }

    public void set_x(float x){
        mx = x;
    }

    public void set_y(float y){
        my = y;
    }

    public void set_RealDiameter(float Diameter){
        mRealDiameter = Diameter;
    }

    public void set_FakeDiameter(float Diameter){
        mFakeDiameter = Diameter;
    }

    public void scale(){
        if ( mFakeDiameter >= 500 ){
            mFakeDiameter = 100;
        }
        if ( mRealDiameter > 10 && mFakeDiameter <= 0 ){
            mFakeDiameter = 100;
        }
    }
}

class dot{

	private float mInitX = 0;
	private float mInitY = 0;
	private float mHeight = 0;
	private float mWidth = 0;

	private float mX = 0;
	private float mY = 0;

	private float mFakeX = 0;
	private float mFakeY = 0;

	private float mDiameter = 0;
	private float mDst = 0;
	private int mColor = white;

	//Constructor to set default values
	public dot(int Color, float Diameter, float Fheight, float Fwidth){
		mColor = Color;
		mDiameter = Diameter;
		mHeight = Fheight;
		mWidth = Fwidth;
	}

	//Constructor with a defined canvas
	public dot(int Color, float InitX, float InitY, float Fheight, float Fwidth, float Diameter){
		mColor = Color;
		mInitY = InitY;
		mInitX = InitX;
		mHeight = Fheight;
		mWidth = Fwidth;
		mDiameter = Diameter;
	}

	public void draw(){
		fill(mColor);
		ellipse(mX, mY, mDiameter, mDiameter);
	}

	public void scaled_draw(float x, float y, float scale){
		mX = x;
		mY = y;
		scale(scale);
		fill(mColor);
		ellipse(mFakeX, mFakeY, mDiameter, mDiameter);
	}

	public void draw(float x, float y){
		mX = x;
		mY = y;
		mFakeX = x;
		mFakeY = y;
		fill(mColor);
		ellipse(mX, mY, mDiameter, mDiameter);
	}

	public float distance(dot Dot){
		stroke(black);
		float x2 = Dot.get_FakeX();
		float y2 = Dot.get_FakeY();
		mDst = sqrt( sq(x2 - mFakeX) + sq(y2 - mFakeY) );
		line(mFakeX, mFakeY, x2, y2);

		float x1 = Dot.get_X();
		float y1 = Dot.get_Y();
		return sqrt( sq(x1 - mX) + sq(y1 - mY) );
	}

	private void scale(float scale){

		if ( mFakeY > mHeight ){
			mFakeY = mFakeY / scale;
		}
		if ( mFakeX > mWidth ){
			mFakeX = mFakeX / scale;
		}
		if ( mDst < scale * 10 ){
			mFakeY = mFakeY * scale;
			mFakeX = mFakeX * scale;
		}
	}

	//get methods
	public float get_X(){
		return mX;
	}

	public float get_Y(){
		return mY;
	}

	public float get_FakeX(){
		return mFakeX;
	}

	public float get_FakeY(){
		return mFakeY;
	}

	//Set methods
	public void set_FakeX(float x){
		if ( x >= 0 ){
			mFakeX = x;
		}

	}

	public void set_FakeY(float y){
		if ( y >= 0 ){
			mFakeY = y;
		}
	}

};
public int yellow = color(255, 255, 0);
public int black = color(0, 0, 0);
public int red = color(255, 0, 0);
public int green = color(0, 255, 0);
public int blue = color(0, 0, 255);
public int white = color(255, 255, 255);

float e = 0;
float scl = 1;
float rd1 = 200;
float rd2 = 20;

float x = 100;
float y = 100;

circle circ1 = new circle(red);
circle circ2 = new circle(blue);

dot dot1 = new dot(red, 10, 600, 700);
dot dot2 = new dot(blue, 10, 600, 700);
    public void settings() { 	size(700, 600); }
    static public void main(String[] passedArgs) {
        String[] appletArgs = new String[] { "Escala" };
        if (passedArgs != null) {
          PApplet.main(concat(appletArgs, passedArgs));
        } else {
          PApplet.main(appletArgs);
        }
    }
}
